<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_b0ab92c2845626e4888d0cdbb6db45ede593a1ffa5205523e70c3a80d813f792 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@FOSUser/layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fefc390f02859600a4744a4c8f6d3d17b55ba92624a196d695efc09276edfb25 = $this->env->getExtension("native_profiler");
        $__internal_fefc390f02859600a4744a4c8f6d3d17b55ba92624a196d695efc09276edfb25->enter($__internal_fefc390f02859600a4744a4c8f6d3d17b55ba92624a196d695efc09276edfb25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fefc390f02859600a4744a4c8f6d3d17b55ba92624a196d695efc09276edfb25->leave($__internal_fefc390f02859600a4744a4c8f6d3d17b55ba92624a196d695efc09276edfb25_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6b0576c72cd8875b23ed26b2bafb25b3cbfd9318a40713ce101889e1267575bc = $this->env->getExtension("native_profiler");
        $__internal_6b0576c72cd8875b23ed26b2bafb25b3cbfd9318a40713ce101889e1267575bc->enter($__internal_6b0576c72cd8875b23ed26b2bafb25b3cbfd9318a40713ce101889e1267575bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_6b0576c72cd8875b23ed26b2bafb25b3cbfd9318a40713ce101889e1267575bc->leave($__internal_6b0576c72cd8875b23ed26b2bafb25b3cbfd9318a40713ce101889e1267575bc_prof);

    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_72816928ef8df9f31583b50ca44bb8ae03e9b1c5e995ca5ae28bb10517e9d3e6 = $this->env->getExtension("native_profiler");
        $__internal_72816928ef8df9f31583b50ca44bb8ae03e9b1c5e995ca5ae28bb10517e9d3e6->enter($__internal_72816928ef8df9f31583b50ca44bb8ae03e9b1c5e995ca5ae28bb10517e9d3e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 8
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 27
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 29
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    ";
        
        $__internal_72816928ef8df9f31583b50ca44bb8ae03e9b1c5e995ca5ae28bb10517e9d3e6->leave($__internal_72816928ef8df9f31583b50ca44bb8ae03e9b1c5e995ca5ae28bb10517e9d3e6_prof);

    }

    // line 27
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_7d317a72eef07d51845bf4cfcae7a282ebeb4249b6612b173ad2f0b4e50cdb6d = $this->env->getExtension("native_profiler");
        $__internal_7d317a72eef07d51845bf4cfcae7a282ebeb4249b6612b173ad2f0b4e50cdb6d->enter($__internal_7d317a72eef07d51845bf4cfcae7a282ebeb4249b6612b173ad2f0b4e50cdb6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 28
        echo "                    ";
        
        $__internal_7d317a72eef07d51845bf4cfcae7a282ebeb4249b6612b173ad2f0b4e50cdb6d->leave($__internal_7d317a72eef07d51845bf4cfcae7a282ebeb4249b6612b173ad2f0b4e50cdb6d_prof);

    }

    // line 35
    public function block_title($context, array $blocks = array())
    {
        $__internal_ace7c72b3590d69eff95313e35528afd4050c7b590354bb483d6ef8f9d2c3ff4 = $this->env->getExtension("native_profiler");
        $__internal_ace7c72b3590d69eff95313e35528afd4050c7b590354bb483d6ef8f9d2c3ff4->enter($__internal_ace7c72b3590d69eff95313e35528afd4050c7b590354bb483d6ef8f9d2c3ff4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_ace7c72b3590d69eff95313e35528afd4050c7b590354bb483d6ef8f9d2c3ff4->leave($__internal_ace7c72b3590d69eff95313e35528afd4050c7b590354bb483d6ef8f9d2c3ff4_prof);

    }

    // line 37
    public function block_content($context, array $blocks = array())
    {
        $__internal_8084e75ff4ed28691dbab4220d7e2b43aa4a7d24854e965320db97975f91cf25 = $this->env->getExtension("native_profiler");
        $__internal_8084e75ff4ed28691dbab4220d7e2b43aa4a7d24854e965320db97975f91cf25->enter($__internal_8084e75ff4ed28691dbab4220d7e2b43aa4a7d24854e965320db97975f91cf25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 38
        echo "
    ";
        // line 39
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 40
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 41
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 42
                    echo "                <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo " pagination-centered\">
                    <h2><center> ";
                    // line 43
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo " </center></h2>
                </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "    ";
        }
        // line 48
        echo "
    <div>
        ";
        // line 50
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 52
        echo "    </div>
";
        
        $__internal_8084e75ff4ed28691dbab4220d7e2b43aa4a7d24854e965320db97975f91cf25->leave($__internal_8084e75ff4ed28691dbab4220d7e2b43aa4a7d24854e965320db97975f91cf25_prof);

    }

    // line 50
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_081c54644912335ca674d4aa0c5b8aad9a258200d292f5e4ae48baac2b6d357f = $this->env->getExtension("native_profiler");
        $__internal_081c54644912335ca674d4aa0c5b8aad9a258200d292f5e4ae48baac2b6d357f->enter($__internal_081c54644912335ca674d4aa0c5b8aad9a258200d292f5e4ae48baac2b6d357f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 51
        echo "        ";
        
        $__internal_081c54644912335ca674d4aa0c5b8aad9a258200d292f5e4ae48baac2b6d357f->leave($__internal_081c54644912335ca674d4aa0c5b8aad9a258200d292f5e4ae48baac2b6d357f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 51,  181 => 50,  173 => 52,  171 => 50,  167 => 48,  164 => 47,  158 => 46,  149 => 43,  144 => 42,  139 => 41,  134 => 40,  132 => 39,  129 => 38,  123 => 37,  111 => 35,  104 => 28,  98 => 27,  87 => 29,  85 => 27,  74 => 19,  61 => 8,  55 => 7,  45 => 4,  39 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                     {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/*     {% endblock %}*/
/* */
/* {% block title %} {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     {% if app.request.hasPreviousSession %}*/
/*         {% for type, messages in app.session.flashbag.all() %}*/
/*             {% for message in messages %}*/
/*                 <div class="flash-{{ type }} pagination-centered">*/
/*                     <h2><center> {{ message }} </center></h2>*/
/*                 </div>*/
/*             {% endfor %}*/
/*         {% endfor %}*/
/*     {% endif %}*/
/* */
/*     <div>*/
/*         {% block fos_user_content %}*/
/*         {% endblock fos_user_content %}*/
/*     </div>*/
/* {% endblock %}*/
