<?php

/* Mentor/layout.html.twig */
class __TwigTemplate_56de3c647f4691c4e7ff95bcaf4005e548219d71b02d779d617b70b8587b215f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "Mentor/layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e015c0b370bdb1e512eacaa185809b0cc8cbe0835accbdfab9f6e95567350979 = $this->env->getExtension("native_profiler");
        $__internal_e015c0b370bdb1e512eacaa185809b0cc8cbe0835accbdfab9f6e95567350979->enter($__internal_e015c0b370bdb1e512eacaa185809b0cc8cbe0835accbdfab9f6e95567350979_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Mentor/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e015c0b370bdb1e512eacaa185809b0cc8cbe0835accbdfab9f6e95567350979->leave($__internal_e015c0b370bdb1e512eacaa185809b0cc8cbe0835accbdfab9f6e95567350979_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_4c7fe7a054c76e3105e343a761a7311486d084fbe324e2973d860dbff5a86bb6 = $this->env->getExtension("native_profiler");
        $__internal_4c7fe7a054c76e3105e343a761a7311486d084fbe324e2973d860dbff5a86bb6->enter($__internal_4c7fe7a054c76e3105e343a761a7311486d084fbe324e2973d860dbff5a86bb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_4c7fe7a054c76e3105e343a761a7311486d084fbe324e2973d860dbff5a86bb6->leave($__internal_4c7fe7a054c76e3105e343a761a7311486d084fbe324e2973d860dbff5a86bb6_prof);

    }

    // line 6
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_f5a97665d9ed5cc97462d2b5f97361070af6488adf0a41fc531df952f9aa6268 = $this->env->getExtension("native_profiler");
        $__internal_f5a97665d9ed5cc97462d2b5f97361070af6488adf0a41fc531df952f9aa6268->enter($__internal_f5a97665d9ed5cc97462d2b5f97361070af6488adf0a41fc531df952f9aa6268_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 7
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 26
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 28
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

";
        
        $__internal_f5a97665d9ed5cc97462d2b5f97361070af6488adf0a41fc531df952f9aa6268->leave($__internal_f5a97665d9ed5cc97462d2b5f97361070af6488adf0a41fc531df952f9aa6268_prof);

    }

    // line 26
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_5292d6f62d9162c4b7ece42bcd6c2b2390e349f2033b1618f387a84623b4c225 = $this->env->getExtension("native_profiler");
        $__internal_5292d6f62d9162c4b7ece42bcd6c2b2390e349f2033b1618f387a84623b4c225->enter($__internal_5292d6f62d9162c4b7ece42bcd6c2b2390e349f2033b1618f387a84623b4c225_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 27
        echo "                    ";
        
        $__internal_5292d6f62d9162c4b7ece42bcd6c2b2390e349f2033b1618f387a84623b4c225->leave($__internal_5292d6f62d9162c4b7ece42bcd6c2b2390e349f2033b1618f387a84623b4c225_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_22ecb3e486d031e279c4d91fc54df2fd4875ba579beae458b3a78b7cab7cde2b = $this->env->getExtension("native_profiler");
        $__internal_22ecb3e486d031e279c4d91fc54df2fd4875ba579beae458b3a78b7cab7cde2b->enter($__internal_22ecb3e486d031e279c4d91fc54df2fd4875ba579beae458b3a78b7cab7cde2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>

";
        
        $__internal_22ecb3e486d031e279c4d91fc54df2fd4875ba579beae458b3a78b7cab7cde2b->leave($__internal_22ecb3e486d031e279c4d91fc54df2fd4875ba579beae458b3a78b7cab7cde2b_prof);

    }

    public function getTemplateName()
    {
        return "Mentor/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 39,  121 => 38,  116 => 37,  110 => 36,  103 => 27,  97 => 26,  85 => 28,  83 => 26,  72 => 18,  59 => 7,  53 => 6,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                     {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('bundles/fosjsrouting/js/router.js') }}"></script>*/
/*     <script src="{{ path('fos_js_routing_js', { callback: 'fos.Router.setData' }) }}"></script>*/
/* */
/* {% endblock %}*/
