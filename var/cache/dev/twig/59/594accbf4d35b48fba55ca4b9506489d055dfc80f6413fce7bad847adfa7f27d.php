<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_a6e4e2d5504c47922455efe5a9c7ae4cfc0051d09b08ee98afc322ed3dff5b58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_837c1110fd5e1104a65b7ce300c33d56bfe4a10c6e744fa109112dd5ae1387c0 = $this->env->getExtension("native_profiler");
        $__internal_837c1110fd5e1104a65b7ce300c33d56bfe4a10c6e744fa109112dd5ae1387c0->enter($__internal_837c1110fd5e1104a65b7ce300c33d56bfe4a10c6e744fa109112dd5ae1387c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_837c1110fd5e1104a65b7ce300c33d56bfe4a10c6e744fa109112dd5ae1387c0->leave($__internal_837c1110fd5e1104a65b7ce300c33d56bfe4a10c6e744fa109112dd5ae1387c0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
