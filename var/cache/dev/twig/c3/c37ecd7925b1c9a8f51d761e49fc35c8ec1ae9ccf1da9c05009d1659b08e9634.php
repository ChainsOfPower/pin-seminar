<?php

/* :Home:layout.html.twig */
class __TwigTemplate_a01ce7186d34e60fddeb41e20d633f2812847a44ae28f92147d57308056984fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Home:layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e67bd52766a4ab5280275360b8d08f621b484fccd78e7f95bf579e0db38921d6 = $this->env->getExtension("native_profiler");
        $__internal_e67bd52766a4ab5280275360b8d08f621b484fccd78e7f95bf579e0db38921d6->enter($__internal_e67bd52766a4ab5280275360b8d08f621b484fccd78e7f95bf579e0db38921d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Home:layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e67bd52766a4ab5280275360b8d08f621b484fccd78e7f95bf579e0db38921d6->leave($__internal_e67bd52766a4ab5280275360b8d08f621b484fccd78e7f95bf579e0db38921d6_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b02e7288109798bab242f2094e390e3064c7730c350eb82abdbd99c8db0bae34 = $this->env->getExtension("native_profiler");
        $__internal_b02e7288109798bab242f2094e390e3064c7730c350eb82abdbd99c8db0bae34->enter($__internal_b02e7288109798bab242f2094e390e3064c7730c350eb82abdbd99c8db0bae34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_b02e7288109798bab242f2094e390e3064c7730c350eb82abdbd99c8db0bae34->leave($__internal_b02e7288109798bab242f2094e390e3064c7730c350eb82abdbd99c8db0bae34_prof);

    }

    // line 6
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_0cb9bf2b37e5f6414d4f7b703e283e92036563cb544376cb50b65b9d1f255b8b = $this->env->getExtension("native_profiler");
        $__internal_0cb9bf2b37e5f6414d4f7b703e283e92036563cb544376cb50b65b9d1f255b8b->enter($__internal_0cb9bf2b37e5f6414d4f7b703e283e92036563cb544376cb50b65b9d1f255b8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 7
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                   ";
        // line 26
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 28
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

";
        
        $__internal_0cb9bf2b37e5f6414d4f7b703e283e92036563cb544376cb50b65b9d1f255b8b->leave($__internal_0cb9bf2b37e5f6414d4f7b703e283e92036563cb544376cb50b65b9d1f255b8b_prof);

    }

    // line 26
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_3e469f4ff398837bef63b76e383ef8760185e3a787796728e89d5abff7207127 = $this->env->getExtension("native_profiler");
        $__internal_3e469f4ff398837bef63b76e383ef8760185e3a787796728e89d5abff7207127->enter($__internal_3e469f4ff398837bef63b76e383ef8760185e3a787796728e89d5abff7207127_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 27
        echo "                    ";
        
        $__internal_3e469f4ff398837bef63b76e383ef8760185e3a787796728e89d5abff7207127->leave($__internal_3e469f4ff398837bef63b76e383ef8760185e3a787796728e89d5abff7207127_prof);

    }

    public function getTemplateName()
    {
        return ":Home:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 27,  96 => 26,  84 => 28,  82 => 26,  71 => 18,  58 => 7,  52 => 6,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                    {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
