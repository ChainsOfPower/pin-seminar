<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_4da8b99103fa3a8b870ee33afdd875746e15d386a211245b83cbd879dcce641f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_349e385a76919280db1df110891781aefbdd1071dacb0b4c93e9fd56590511ba = $this->env->getExtension("native_profiler");
        $__internal_349e385a76919280db1df110891781aefbdd1071dacb0b4c93e9fd56590511ba->enter($__internal_349e385a76919280db1df110891781aefbdd1071dacb0b4c93e9fd56590511ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_349e385a76919280db1df110891781aefbdd1071dacb0b4c93e9fd56590511ba->leave($__internal_349e385a76919280db1df110891781aefbdd1071dacb0b4c93e9fd56590511ba_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
