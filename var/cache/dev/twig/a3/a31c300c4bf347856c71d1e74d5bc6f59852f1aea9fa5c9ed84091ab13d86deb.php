<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_470d7ff6f85d46e8eea8c2d08aef61e38ccba60776d1a7ea89af3d3d78fabba0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31d5b7d338fa2ee0de882c2fa9da5687962b49c856b1d0d3c1e54316c276a7bd = $this->env->getExtension("native_profiler");
        $__internal_31d5b7d338fa2ee0de882c2fa9da5687962b49c856b1d0d3c1e54316c276a7bd->enter($__internal_31d5b7d338fa2ee0de882c2fa9da5687962b49c856b1d0d3c1e54316c276a7bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_31d5b7d338fa2ee0de882c2fa9da5687962b49c856b1d0d3c1e54316c276a7bd->leave($__internal_31d5b7d338fa2ee0de882c2fa9da5687962b49c856b1d0d3c1e54316c276a7bd_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_e482e36569fa1b38cd0e45911976b6b460539a534175b6a8ba4ed4c015df9b7d = $this->env->getExtension("native_profiler");
        $__internal_e482e36569fa1b38cd0e45911976b6b460539a534175b6a8ba4ed4c015df9b7d->enter($__internal_e482e36569fa1b38cd0e45911976b6b460539a534175b6a8ba4ed4c015df9b7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
";
        
        $__internal_e482e36569fa1b38cd0e45911976b6b460539a534175b6a8ba4ed4c015df9b7d->leave($__internal_e482e36569fa1b38cd0e45911976b6b460539a534175b6a8ba4ed4c015df9b7d_prof);

    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        $__internal_1913a9a8100f628c962bfefcfded23f53cf33645a8a130edb8cfcbf245a9b2f0 = $this->env->getExtension("native_profiler");
        $__internal_1913a9a8100f628c962bfefcfded23f53cf33645a8a130edb8cfcbf245a9b2f0->enter($__internal_1913a9a8100f628c962bfefcfded23f53cf33645a8a130edb8cfcbf245a9b2f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 11
        echo "    Logiraj se
";
        
        $__internal_1913a9a8100f628c962bfefcfded23f53cf33645a8a130edb8cfcbf245a9b2f0->leave($__internal_1913a9a8100f628c962bfefcfded23f53cf33645a8a130edb8cfcbf245a9b2f0_prof);

    }

    // line 14
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_21064968752fd8cdb666d8ef5099255e0d0cd289af51a9bf6534e4a13f7cb624 = $this->env->getExtension("native_profiler");
        $__internal_21064968752fd8cdb666d8ef5099255e0d0cd289af51a9bf6534e4a13f7cb624->enter($__internal_21064968752fd8cdb666d8ef5099255e0d0cd289af51a9bf6534e4a13f7cb624_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 15
        echo "    <li class=\"active\"><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_21064968752fd8cdb666d8ef5099255e0d0cd289af51a9bf6534e4a13f7cb624->leave($__internal_21064968752fd8cdb666d8ef5099255e0d0cd289af51a9bf6534e4a13f7cb624_prof);

    }

    // line 20
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_de428322ea7a78a70e198dfaad41b3461276d9fb9b0aed02b49bfdb1fa6f6ed4 = $this->env->getExtension("native_profiler");
        $__internal_de428322ea7a78a70e198dfaad41b3461276d9fb9b0aed02b49bfdb1fa6f6ed4->enter($__internal_de428322ea7a78a70e198dfaad41b3461276d9fb9b0aed02b49bfdb1fa6f6ed4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 21
        echo "

    <div class=\"container\">

        <h2><center>Logiraj se</center></h2>
        <form action=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\" class=\"form-signin\">
            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />

            <label for=\"username\">E-mail:</label>
            <input type=\"email\" id=\"username\" name=\"_username\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" class=\"form-control\" />

            <label for=\"password\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\"/>
            ";
        // line 34
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 35
            echo "                <div class=\"text-danger\">Pogrešni podaci</div>
            ";
        }
        // line 37
        echo "
            <div class=\"checkbox\">
                <label>
                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />Zapamti me
                </label>
            </div>

            <input class=\"btn btn-primary btn-block\" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"Logiraj se\" />

            <a href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
        echo "\">Zaboravljena lozinka?</a>
        </form>



    </div>

";
        
        $__internal_de428322ea7a78a70e198dfaad41b3461276d9fb9b0aed02b49bfdb1fa6f6ed4->leave($__internal_de428322ea7a78a70e198dfaad41b3461276d9fb9b0aed02b49bfdb1fa6f6ed4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 46,  125 => 37,  121 => 35,  119 => 34,  114 => 32,  109 => 30,  103 => 27,  99 => 26,  92 => 21,  86 => 20,  77 => 15,  71 => 14,  63 => 11,  57 => 10,  48 => 7,  43 => 6,  37 => 5,  11 => 1,);
    }
}
/* {% extends '@FOSUser/layout.html.twig' %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     Logiraj se*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li class="active"><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* */
/* {% block fos_user_content %}*/
/* */
/* */
/*     <div class="container">*/
/* */
/*         <h2><center>Logiraj se</center></h2>*/
/*         <form action="{{ path("fos_user_security_check") }}" method="post" class="form-signin">*/
/*             <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/* */
/*             <label for="username">E-mail:</label>*/
/*             <input type="email" id="username" name="_username" value="{{ last_username }}" required="required" class="form-control" />*/
/* */
/*             <label for="password">{{ 'security.login.password'|trans }}</label>*/
/*             <input type="password" id="password" name="_password" required="required" class="form-control"/>*/
/*             {% if error %}*/
/*                 <div class="text-danger">Pogrešni podaci</div>*/
/*             {% endif %}*/
/* */
/*             <div class="checkbox">*/
/*                 <label>*/
/*                     <input type="checkbox" id="remember_me" name="_remember_me" value="on" />Zapamti me*/
/*                 </label>*/
/*             </div>*/
/* */
/*             <input class="btn btn-primary btn-block" type="submit" id="_submit" name="_submit" value="Logiraj se" />*/
/* */
/*             <a href="{{ path('fos_user_resetting_request') }}">Zaboravljena lozinka?</a>*/
/*         </form>*/
/* */
/* */
/* */
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
