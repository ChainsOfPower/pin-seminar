<?php

/* Student/redovni_home.html.twig */
class __TwigTemplate_21c9e21ba076148373dc2635647956a860ff85f2ab66027ade9746179abe1236 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Student/layout.html.twig", "Student/redovni_home.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Student/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b23e96a49cc4ab30e1d062b253b4252c8e0068cc30b07ffb4345834c5ef5bb4 = $this->env->getExtension("native_profiler");
        $__internal_6b23e96a49cc4ab30e1d062b253b4252c8e0068cc30b07ffb4345834c5ef5bb4->enter($__internal_6b23e96a49cc4ab30e1d062b253b4252c8e0068cc30b07ffb4345834c5ef5bb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Student/redovni_home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6b23e96a49cc4ab30e1d062b253b4252c8e0068cc30b07ffb4345834c5ef5bb4->leave($__internal_6b23e96a49cc4ab30e1d062b253b4252c8e0068cc30b07ffb4345834c5ef5bb4_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_98ffe29f4a62a13a33855c0421503641fb4e1d76142eaf9bab4bdb739c64b989 = $this->env->getExtension("native_profiler");
        $__internal_98ffe29f4a62a13a33855c0421503641fb4e1d76142eaf9bab4bdb739c64b989->enter($__internal_98ffe29f4a62a13a33855c0421503641fb4e1d76142eaf9bab4bdb739c64b989_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/table_student.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_98ffe29f4a62a13a33855c0421503641fb4e1d76142eaf9bab4bdb739c64b989->leave($__internal_98ffe29f4a62a13a33855c0421503641fb4e1d76142eaf9bab4bdb739c64b989_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_701de3fa665d6a06b411773d435a234fb0a0c5a6ac8f764a7cc5fdc242ba7eb4 = $this->env->getExtension("native_profiler");
        $__internal_701de3fa665d6a06b411773d435a234fb0a0c5a6ac8f764a7cc5fdc242ba7eb4->enter($__internal_701de3fa665d6a06b411773d435a234fb0a0c5a6ac8f764a7cc5fdc242ba7eb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    Upisni list
";
        
        $__internal_701de3fa665d6a06b411773d435a234fb0a0c5a6ac8f764a7cc5fdc242ba7eb4->leave($__internal_701de3fa665d6a06b411773d435a234fb0a0c5a6ac8f764a7cc5fdc242ba7eb4_prof);

    }

    // line 11
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_f7691e6efa6b27a85571e2932ca3a2f6570dab4c090514d87a8c546856b15e62 = $this->env->getExtension("native_profiler");
        $__internal_f7691e6efa6b27a85571e2932ca3a2f6570dab4c090514d87a8c546856b15e62->enter($__internal_f7691e6efa6b27a85571e2932ca3a2f6570dab4c090514d87a8c546856b15e62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 12
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("ROLE_STUDENT")) {
            // line 13
            echo "        <li class=\"active\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("student_homepage");
            echo "\">Upisni list</a></li>
        <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
        <li><a href=\"/logout\">Odjavi se</a></li>
    ";
        }
        // line 17
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("ROLE_MENTOR")) {
            // line 18
            echo "        <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("mentor_studenti");
            echo "\">Studenti</a></li>
        <li><a href=\"";
            // line 19
            echo $this->env->getExtension('routing')->getPath("subject_index");
            echo "\">Predmeti</a></li>
        <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
        <li><a href=\"/logout\">Odjavi se</a></li>
    ";
        }
        
        $__internal_f7691e6efa6b27a85571e2932ca3a2f6570dab4c090514d87a8c546856b15e62->leave($__internal_f7691e6efa6b27a85571e2932ca3a2f6570dab4c090514d87a8c546856b15e62_prof);

    }

    // line 26
    public function block_content($context, array $blocks = array())
    {
        $__internal_8bf32485fe41d412b48e3a3c601a1c51dd4e413065a86503906f93cc7ea406cb = $this->env->getExtension("native_profiler");
        $__internal_8bf32485fe41d412b48e3a3c601a1c51dd4e413065a86503906f93cc7ea406cb->enter($__internal_8bf32485fe41d412b48e3a3c601a1c51dd4e413065a86503906f93cc7ea406cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 27
        echo "    <div style=\"display: none\" id=\"token\">";
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "</div>
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-6\">
                <h2>Predmeti:</h2>
                <div class=\"sadrzaj\">
                    <ul class=\"list-group\" id=\"neupisani_predmeti_studenta\">
                        ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["neupisani"]) ? $context["neupisani"] : $this->getContext($context, "neupisani")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 35
            echo "                            <li class=\"list-group-item\"><a href=\"javascript:void(0)\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\"><i
                                            class=\"fa fa-plus-square fa-lg\"
                                            aria-hidden=\"true\"></i></a> ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                    </ul>
                </div>


            </div>

            <div class=\"col-md-6\" id=\"upisani_predmeti_studenta\">

                <h2>Upisi (";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "username", array()), "html", null, true);
        echo ")</h2>
                <span style=\"display: none\" id=\"korisnik\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "id", array()), "html", null, true);
        echo "</span>
                <h3>Semestar 1:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"prviSemestar\">

                    ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prviSemestar"]) ? $context["prviSemestar"] : $this->getContext($context, "prviSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 55
            echo "                        <tr>
                            <td>
                                ";
            // line 57
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 58
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 62
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 64
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "                    </tbody>
                </table>

                <h3>Semestar 2:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"drugiSemestar\">

                    ";
        // line 76
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["drugiSemestar"]) ? $context["drugiSemestar"] : $this->getContext($context, "drugiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 77
            echo "                        <tr>
                            <td>
                                ";
            // line 79
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 80
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 84
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 86
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "                    </tbody>
                </table>

                <h3>Semestar 3:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"treciSemestar\">

                    ";
        // line 98
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["treciSemestar"]) ? $context["treciSemestar"] : $this->getContext($context, "treciSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 99
            echo "                        <tr>
                            <td>
                                ";
            // line 101
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 102
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 106
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 108
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 113
        echo "                    </tbody>
                </table>

                <h3>Semestar 4:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"cetvrtiSemestar\">

                    ";
        // line 120
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cetvrtiSemestar"]) ? $context["cetvrtiSemestar"] : $this->getContext($context, "cetvrtiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 121
            echo "                        <tr>
                            <td>
                                ";
            // line 123
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 124
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 128
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 130
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 135
        echo "                    </tbody>
                </table>

                <h3>Semestar 5:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"petiSemestar\">

                    ";
        // line 142
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["petiSemestar"]) ? $context["petiSemestar"] : $this->getContext($context, "petiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 143
            echo "                        <tr>
                            <td>
                                ";
            // line 145
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 146
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 150
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 152
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 157
        echo "                    </tbody>
                </table>

                <h3>Semestar 6:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"sestiSemestar\">

                    ";
        // line 164
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sestiSemestar"]) ? $context["sestiSemestar"] : $this->getContext($context, "sestiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 165
            echo "                        <tr>
                            <td>
                                ";
            // line 167
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 168
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 172
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 174
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 175
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 179
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
";
        
        $__internal_8bf32485fe41d412b48e3a3c601a1c51dd4e413065a86503906f93cc7ea406cb->leave($__internal_8bf32485fe41d412b48e3a3c601a1c51dd4e413065a86503906f93cc7ea406cb_prof);

    }

    // line 186
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5e63dd1f91ace31c663a7435d2b26e5cb8705da07c2e3a3725084c4972e73808 = $this->env->getExtension("native_profiler");
        $__internal_5e63dd1f91ace31c663a7435d2b26e5cb8705da07c2e3a3725084c4972e73808->enter($__internal_5e63dd1f91ace31c663a7435d2b26e5cb8705da07c2e3a3725084c4972e73808_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 187
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 188
        if ($this->env->getExtension('security')->isGranted("ROLE_STUDENT")) {
            // line 189
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/student.js"), "html", null, true);
            echo "\"></script>
    ";
        }
        // line 191
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("ROLE_MENTOR")) {
            // line 192
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/mentor_student.js"), "html", null, true);
            echo "\"></script>
    ";
        }
        
        $__internal_5e63dd1f91ace31c663a7435d2b26e5cb8705da07c2e3a3725084c4972e73808->leave($__internal_5e63dd1f91ace31c663a7435d2b26e5cb8705da07c2e3a3725084c4972e73808_prof);

    }

    public function getTemplateName()
    {
        return "Student/redovni_home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  468 => 192,  465 => 191,  459 => 189,  457 => 188,  452 => 187,  446 => 186,  434 => 179,  424 => 175,  419 => 174,  415 => 172,  407 => 168,  405 => 167,  401 => 165,  397 => 164,  388 => 157,  378 => 153,  373 => 152,  369 => 150,  361 => 146,  359 => 145,  355 => 143,  351 => 142,  342 => 135,  332 => 131,  327 => 130,  323 => 128,  315 => 124,  313 => 123,  309 => 121,  305 => 120,  296 => 113,  286 => 109,  281 => 108,  277 => 106,  269 => 102,  267 => 101,  263 => 99,  259 => 98,  250 => 91,  240 => 87,  235 => 86,  231 => 84,  223 => 80,  221 => 79,  217 => 77,  213 => 76,  204 => 69,  194 => 65,  189 => 64,  185 => 62,  177 => 58,  175 => 57,  171 => 55,  167 => 54,  159 => 49,  155 => 48,  145 => 40,  136 => 37,  130 => 35,  126 => 34,  115 => 27,  109 => 26,  97 => 19,  92 => 18,  89 => 17,  81 => 13,  78 => 12,  72 => 11,  64 => 8,  58 => 7,  49 => 4,  44 => 3,  38 => 2,  11 => 1,);
    }
}
/* {% extends 'Student/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/table_student.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     Upisni list*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     {% if is_granted('ROLE_STUDENT') %}*/
/*         <li class="active"><a href="{{ path('student_homepage') }}">Upisni list</a></li>*/
/*         <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*         <li><a href="/logout">Odjavi se</a></li>*/
/*     {% endif %}*/
/*     {% if is_granted('ROLE_MENTOR') %}*/
/*         <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*         <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*         <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*         <li><a href="/logout">Odjavi se</a></li>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
/* */
/* {% block content %}*/
/*     <div style="display: none" id="token">{{ token }}</div>*/
/*     <div class="container-fluid">*/
/*         <div class="row">*/
/*             <div class="col-md-6">*/
/*                 <h2>Predmeti:</h2>*/
/*                 <div class="sadrzaj">*/
/*                     <ul class="list-group" id="neupisani_predmeti_studenta">*/
/*                         {% for predmet in neupisani %}*/
/*                             <li class="list-group-item"><a href="javascript:void(0)" id="{{ predmet.id }}"><i*/
/*                                             class="fa fa-plus-square fa-lg"*/
/*                                             aria-hidden="true"></i></a> {{ predmet.ime }}*/
/*                             </li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
/* */
/* */
/*             </div>*/
/* */
/*             <div class="col-md-6" id="upisani_predmeti_studenta">*/
/* */
/*                 <h2>Upisi ({{ student.username }})</h2>*/
/*                 <span style="display: none" id="korisnik">{{ student.id }}</span>*/
/*                 <h3>Semestar 1:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="prviSemestar">*/
/* */
/*                     {% for predmet in prviSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 2:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="drugiSemestar">*/
/* */
/*                     {% for predmet in drugiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 3:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="treciSemestar">*/
/* */
/*                     {% for predmet in treciSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 4:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="cetvrtiSemestar">*/
/* */
/*                     {% for predmet in cetvrtiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 5:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="petiSemestar">*/
/* */
/*                     {% for predmet in petiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 6:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="sestiSemestar">*/
/* */
/*                     {% for predmet in sestiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {% if is_granted('ROLE_STUDENT') %}*/
/*         <script src="{{ asset('js/student.js') }}"></script>*/
/*     {% endif %}*/
/*     {% if is_granted('ROLE_MENTOR') %}*/
/*         <script src="{{ asset('js/mentor_student.js') }}"></script>*/
/*     {% endif %}*/
/* {% endblock %}*/
