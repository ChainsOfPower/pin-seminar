<?php

/* @FOSUser/Registration/email.txt.twig */
class __TwigTemplate_95429e08a458b07a10305010717562bc3a47fff73ea2b0a1ade2e8ea0c4a86ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a8176088f53e220e9e7a84b8cabab1005dcbe1b1fe7fbe7a8306782c36cede30 = $this->env->getExtension("native_profiler");
        $__internal_a8176088f53e220e9e7a84b8cabab1005dcbe1b1fe7fbe7a8306782c36cede30->enter($__internal_a8176088f53e220e9e7a84b8cabab1005dcbe1b1fe7fbe7a8306782c36cede30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_a8176088f53e220e9e7a84b8cabab1005dcbe1b1fe7fbe7a8306782c36cede30->leave($__internal_a8176088f53e220e9e7a84b8cabab1005dcbe1b1fe7fbe7a8306782c36cede30_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_6abe771f99a0b007dce884ee65576bd2045a88a3a725d1375c0148cb4d7f2c90 = $this->env->getExtension("native_profiler");
        $__internal_6abe771f99a0b007dce884ee65576bd2045a88a3a725d1375c0148cb4d7f2c90->enter($__internal_6abe771f99a0b007dce884ee65576bd2045a88a3a725d1375c0148cb4d7f2c90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_6abe771f99a0b007dce884ee65576bd2045a88a3a725d1375c0148cb4d7f2c90->leave($__internal_6abe771f99a0b007dce884ee65576bd2045a88a3a725d1375c0148cb4d7f2c90_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_63868c609b8b1aaa7abc84da3c2c9feb6570530d627dd2501529e19207970c21 = $this->env->getExtension("native_profiler");
        $__internal_63868c609b8b1aaa7abc84da3c2c9feb6570530d627dd2501529e19207970c21->enter($__internal_63868c609b8b1aaa7abc84da3c2c9feb6570530d627dd2501529e19207970c21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_63868c609b8b1aaa7abc84da3c2c9feb6570530d627dd2501529e19207970c21->leave($__internal_63868c609b8b1aaa7abc84da3c2c9feb6570530d627dd2501529e19207970c21_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_4ff5a94de263a4e18997fc9f2f77b2ff47c8cc58b395374b6c04a04d72c3bd49 = $this->env->getExtension("native_profiler");
        $__internal_4ff5a94de263a4e18997fc9f2f77b2ff47c8cc58b395374b6c04a04d72c3bd49->enter($__internal_4ff5a94de263a4e18997fc9f2f77b2ff47c8cc58b395374b6c04a04d72c3bd49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_4ff5a94de263a4e18997fc9f2f77b2ff47c8cc58b395374b6c04a04d72c3bd49->leave($__internal_4ff5a94de263a4e18997fc9f2f77b2ff47c8cc58b395374b6c04a04d72c3bd49_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
