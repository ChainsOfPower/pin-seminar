<?php

/* @FOSUser/Resetting/checkEmail.html.twig */
class __TwigTemplate_5005539111d6b89a1b3958ec44d733fc18c6c87bba4a506493787a989c990cc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Resetting/checkEmail.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc9261aba5cebc7a65cbdbea2729badd30e9d7d3584392670a62303400092e91 = $this->env->getExtension("native_profiler");
        $__internal_dc9261aba5cebc7a65cbdbea2729badd30e9d7d3584392670a62303400092e91->enter($__internal_dc9261aba5cebc7a65cbdbea2729badd30e9d7d3584392670a62303400092e91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dc9261aba5cebc7a65cbdbea2729badd30e9d7d3584392670a62303400092e91->leave($__internal_dc9261aba5cebc7a65cbdbea2729badd30e9d7d3584392670a62303400092e91_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_994e0871aa163e2f34bb8e6d7a8b2ae7f7e18bb821c4a1d2d7da5c40545b3350 = $this->env->getExtension("native_profiler");
        $__internal_994e0871aa163e2f34bb8e6d7a8b2ae7f7e18bb821c4a1d2d7da5c40545b3350->enter($__internal_994e0871aa163e2f34bb8e6d7a8b2ae7f7e18bb821c4a1d2d7da5c40545b3350_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_994e0871aa163e2f34bb8e6d7a8b2ae7f7e18bb821c4a1d2d7da5c40545b3350->leave($__internal_994e0871aa163e2f34bb8e6d7a8b2ae7f7e18bb821c4a1d2d7da5c40545b3350_prof);

    }

    // line 7
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_cd9510584b8d7d04c7a0c582d8a1b8bab90b3098f49f23b1518b1340f2a2619b = $this->env->getExtension("native_profiler");
        $__internal_cd9510584b8d7d04c7a0c582d8a1b8bab90b3098f49f23b1518b1340f2a2619b->enter($__internal_cd9510584b8d7d04c7a0c582d8a1b8bab90b3098f49f23b1518b1340f2a2619b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 8
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_cd9510584b8d7d04c7a0c582d8a1b8bab90b3098f49f23b1518b1340f2a2619b->leave($__internal_cd9510584b8d7d04c7a0c582d8a1b8bab90b3098f49f23b1518b1340f2a2619b_prof);

    }

    // line 14
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_df3cc03360704ac084ad04edfe014f3ad29f559125295a6ccd0ea750cb2b05ec = $this->env->getExtension("native_profiler");
        $__internal_df3cc03360704ac084ad04edfe014f3ad29f559125295a6ccd0ea750cb2b05ec->enter($__internal_df3cc03360704ac084ad04edfe014f3ad29f559125295a6ccd0ea750cb2b05ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 15
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\" style=\"background-color: rgba(255,255,255,0.8)\">
                <p>
                    ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email"))), "FOSUserBundle"), "html", null, true);
        echo "
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_df3cc03360704ac084ad04edfe014f3ad29f559125295a6ccd0ea750cb2b05ec->leave($__internal_df3cc03360704ac084ad04edfe014f3ad29f559125295a6ccd0ea750cb2b05ec_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 19,  73 => 15,  67 => 14,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{  parent() }}*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3" style="background-color: rgba(255,255,255,0.8)">*/
/*                 <p>*/
/*                     {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/*                 </p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
/* */
/* */
/* */
/* */
/* */
/* */
