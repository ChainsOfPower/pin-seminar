<?php

/* @FOSUser/Profile/edit.html.twig */
class __TwigTemplate_006962ce2b19c78db73617a3a51aa90888a8acd4da1ff97aeaf792960916de59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Profile/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25d2428921877fe66efcfa337926a84d3eec4609095c219622fe465fda65ebcc = $this->env->getExtension("native_profiler");
        $__internal_25d2428921877fe66efcfa337926a84d3eec4609095c219622fe465fda65ebcc->enter($__internal_25d2428921877fe66efcfa337926a84d3eec4609095c219622fe465fda65ebcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_25d2428921877fe66efcfa337926a84d3eec4609095c219622fe465fda65ebcc->leave($__internal_25d2428921877fe66efcfa337926a84d3eec4609095c219622fe465fda65ebcc_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5e43b3da176c8a152c31e9e176a9f0d6695db3b56dc0a77b555726aed4cabb9b = $this->env->getExtension("native_profiler");
        $__internal_5e43b3da176c8a152c31e9e176a9f0d6695db3b56dc0a77b555726aed4cabb9b->enter($__internal_5e43b3da176c8a152c31e9e176a9f0d6695db3b56dc0a77b555726aed4cabb9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "@FOSUser/Profile/edit.html.twig", 4)->display($context);
        
        $__internal_5e43b3da176c8a152c31e9e176a9f0d6695db3b56dc0a77b555726aed4cabb9b->leave($__internal_5e43b3da176c8a152c31e9e176a9f0d6695db3b56dc0a77b555726aed4cabb9b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
