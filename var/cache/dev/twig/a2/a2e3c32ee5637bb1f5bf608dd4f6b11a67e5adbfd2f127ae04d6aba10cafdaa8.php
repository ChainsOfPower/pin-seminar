<?php

/* :Subject:index.html.twig */
class __TwigTemplate_6a9ad479d4c0241acefa70208b3874c6673012073c0b1a6e8b3bfa54792b6b55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Subject/layout.html.twig", ":Subject:index.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Subject/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_845bbf7c36d38a93b9f4a10a749406c38882c46db8bcd15ea9c84fd6c824dfda = $this->env->getExtension("native_profiler");
        $__internal_845bbf7c36d38a93b9f4a10a749406c38882c46db8bcd15ea9c84fd6c824dfda->enter($__internal_845bbf7c36d38a93b9f4a10a749406c38882c46db8bcd15ea9c84fd6c824dfda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Subject:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_845bbf7c36d38a93b9f4a10a749406c38882c46db8bcd15ea9c84fd6c824dfda->leave($__internal_845bbf7c36d38a93b9f4a10a749406c38882c46db8bcd15ea9c84fd6c824dfda_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_27f90bb422e5ac62c93089db132d432be4297b70d2d75ab78fc2ef4289b6262a = $this->env->getExtension("native_profiler");
        $__internal_27f90bb422e5ac62c93089db132d432be4297b70d2d75ab78fc2ef4289b6262a->enter($__internal_27f90bb422e5ac62c93089db132d432be4297b70d2d75ab78fc2ef4289b6262a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/table_student.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_27f90bb422e5ac62c93089db132d432be4297b70d2d75ab78fc2ef4289b6262a->leave($__internal_27f90bb422e5ac62c93089db132d432be4297b70d2d75ab78fc2ef4289b6262a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_6b1b9cf256fff4218c307a3292dcadc214d269ca5303d541bb50f67932f536d0 = $this->env->getExtension("native_profiler");
        $__internal_6b1b9cf256fff4218c307a3292dcadc214d269ca5303d541bb50f67932f536d0->enter($__internal_6b1b9cf256fff4218c307a3292dcadc214d269ca5303d541bb50f67932f536d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    Popis predmeta
";
        
        $__internal_6b1b9cf256fff4218c307a3292dcadc214d269ca5303d541bb50f67932f536d0->leave($__internal_6b1b9cf256fff4218c307a3292dcadc214d269ca5303d541bb50f67932f536d0_prof);

    }

    // line 10
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_b71a592434e0daee545f0b535bce64b6984bd29bbccda2e6e4662ecf94745ca2 = $this->env->getExtension("native_profiler");
        $__internal_b71a592434e0daee545f0b535bce64b6984bd29bbccda2e6e4662ecf94745ca2->enter($__internal_b71a592434e0daee545f0b535bce64b6984bd29bbccda2e6e4662ecf94745ca2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 11
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li class=\"active\"><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_b71a592434e0daee545f0b535bce64b6984bd29bbccda2e6e4662ecf94745ca2->leave($__internal_b71a592434e0daee545f0b535bce64b6984bd29bbccda2e6e4662ecf94745ca2_prof);

    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        $__internal_2132c1e1b0f140d25eb436b641290e1e95c90e73bf1517a4cf41610d40bc68e5 = $this->env->getExtension("native_profiler");
        $__internal_2132c1e1b0f140d25eb436b641290e1e95c90e73bf1517a4cf41610d40bc68e5->enter($__internal_2132c1e1b0f140d25eb436b641290e1e95c90e73bf1517a4cf41610d40bc68e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 18
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\">
                <table class=\"table\">
                    <thead>
                    <tr>
                        <th><a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("subject_new");
        echo "\">Novi predmet</a></th>
                    </tr>
                    </thead>
                    <tbody id=\"lista_predmeta\">
                    ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["subjects"]) ? $context["subjects"] : $this->getContext($context, "subjects")));
        foreach ($context['_seq'] as $context["_key"] => $context["subject"]) {
            // line 29
            echo "                        <tr>
                            <td> ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["subject"], "ime", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($context["subject"], "kod", array()), "html", null, true);
            echo ") <a
                                        href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("subject_show", array("id" => $this->getAttribute($context["subject"], "id", array()))), "html", null, true);
            echo "\">Detalji</a> <a
                                        href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("subject_edit", array("id" => $this->getAttribute($context["subject"], "id", array()))), "html", null, true);
            echo "\">Uredi</a></td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subject'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
";
        
        $__internal_2132c1e1b0f140d25eb436b641290e1e95c90e73bf1517a4cf41610d40bc68e5->leave($__internal_2132c1e1b0f140d25eb436b641290e1e95c90e73bf1517a4cf41610d40bc68e5_prof);

    }

    // line 42
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ed2730adfb5e3613c4b3f5e68d50edfb49af995f0347382b75980cfce11cf994 = $this->env->getExtension("native_profiler");
        $__internal_ed2730adfb5e3613c4b3f5e68d50edfb49af995f0347382b75980cfce11cf994->enter($__internal_ed2730adfb5e3613c4b3f5e68d50edfb49af995f0347382b75980cfce11cf994_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 43
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/predmeti.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_ed2730adfb5e3613c4b3f5e68d50edfb49af995f0347382b75980cfce11cf994->leave($__internal_ed2730adfb5e3613c4b3f5e68d50edfb49af995f0347382b75980cfce11cf994_prof);

    }

    public function getTemplateName()
    {
        return ":Subject:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 44,  159 => 43,  153 => 42,  141 => 35,  132 => 32,  128 => 31,  122 => 30,  119 => 29,  115 => 28,  108 => 24,  100 => 18,  94 => 17,  83 => 12,  78 => 11,  72 => 10,  64 => 8,  58 => 7,  49 => 4,  44 => 3,  38 => 2,  11 => 1,);
    }
}
/* {% extends 'Subject/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/table_student.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     Popis predmeta*/
/* {% endblock %}*/
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li class="active"><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3">*/
/*                 <table class="table">*/
/*                     <thead>*/
/*                     <tr>*/
/*                         <th><a href="{{ path('subject_new') }}">Novi predmet</a></th>*/
/*                     </tr>*/
/*                     </thead>*/
/*                     <tbody id="lista_predmeta">*/
/*                     {% for subject in subjects %}*/
/*                         <tr>*/
/*                             <td> {{ subject.ime }} ({{ subject.kod }}) <a*/
/*                                         href="{{ path('subject_show', {'id': subject.id}) }}">Detalji</a> <a*/
/*                                         href="{{ path('subject_edit', {'id': subject.id}) }}">Uredi</a></td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('js/predmeti.js') }}"></script>*/
/* {% endblock %}*/
