<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_4ea3dca634bd5759b5bffc9b1ecb664cc381f811bfd6e1b0b85fac91bd421ca8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5e02d5558de1daa12fbb37ed6127b855028310e006c19c949287fd44f744a1dc = $this->env->getExtension("native_profiler");
        $__internal_5e02d5558de1daa12fbb37ed6127b855028310e006c19c949287fd44f744a1dc->enter($__internal_5e02d5558de1daa12fbb37ed6127b855028310e006c19c949287fd44f744a1dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5e02d5558de1daa12fbb37ed6127b855028310e006c19c949287fd44f744a1dc->leave($__internal_5e02d5558de1daa12fbb37ed6127b855028310e006c19c949287fd44f744a1dc_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_27d39e625a712fc78334d6969d369152d4af115a4409b5d12af5aeb195c32f98 = $this->env->getExtension("native_profiler");
        $__internal_27d39e625a712fc78334d6969d369152d4af115a4409b5d12af5aeb195c32f98->enter($__internal_27d39e625a712fc78334d6969d369152d4af115a4409b5d12af5aeb195c32f98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_27d39e625a712fc78334d6969d369152d4af115a4409b5d12af5aeb195c32f98->leave($__internal_27d39e625a712fc78334d6969d369152d4af115a4409b5d12af5aeb195c32f98_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
