<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_657eb89f8e13304527f265b0ffa002ba2c360e0bc504c21d5b57cf9aedfec5ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df16bf1f13cbb685eeaebaf4afe4baa45f0fe0b7cbba90d93ca7fbd363c19a4d = $this->env->getExtension("native_profiler");
        $__internal_df16bf1f13cbb685eeaebaf4afe4baa45f0fe0b7cbba90d93ca7fbd363c19a4d->enter($__internal_df16bf1f13cbb685eeaebaf4afe4baa45f0fe0b7cbba90d93ca7fbd363c19a4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_df16bf1f13cbb685eeaebaf4afe4baa45f0fe0b7cbba90d93ca7fbd363c19a4d->leave($__internal_df16bf1f13cbb685eeaebaf4afe4baa45f0fe0b7cbba90d93ca7fbd363c19a4d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
