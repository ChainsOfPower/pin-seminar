<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_47f6a65416f92a9b51ec1942045ae1c98538512f82e8be43f929d542769b2f96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6e3fd22d65ea157751c0b72d2d9910089641ac18a89c4d1ee8359914fa5a958 = $this->env->getExtension("native_profiler");
        $__internal_c6e3fd22d65ea157751c0b72d2d9910089641ac18a89c4d1ee8359914fa5a958->enter($__internal_c6e3fd22d65ea157751c0b72d2d9910089641ac18a89c4d1ee8359914fa5a958_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c6e3fd22d65ea157751c0b72d2d9910089641ac18a89c4d1ee8359914fa5a958->leave($__internal_c6e3fd22d65ea157751c0b72d2d9910089641ac18a89c4d1ee8359914fa5a958_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_4d74f883be04050d86fb263d28c085e513f1161a45ed05b9ac577efb7070b5af = $this->env->getExtension("native_profiler");
        $__internal_4d74f883be04050d86fb263d28c085e513f1161a45ed05b9ac577efb7070b5af->enter($__internal_4d74f883be04050d86fb263d28c085e513f1161a45ed05b9ac577efb7070b5af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_4d74f883be04050d86fb263d28c085e513f1161a45ed05b9ac577efb7070b5af->leave($__internal_4d74f883be04050d86fb263d28c085e513f1161a45ed05b9ac577efb7070b5af_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_b31de71829d0c2ac10720ece0936a0b42a6e2847ae10551a1d80df73d006fb1b = $this->env->getExtension("native_profiler");
        $__internal_b31de71829d0c2ac10720ece0936a0b42a6e2847ae10551a1d80df73d006fb1b->enter($__internal_b31de71829d0c2ac10720ece0936a0b42a6e2847ae10551a1d80df73d006fb1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_b31de71829d0c2ac10720ece0936a0b42a6e2847ae10551a1d80df73d006fb1b->leave($__internal_b31de71829d0c2ac10720ece0936a0b42a6e2847ae10551a1d80df73d006fb1b_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_f53b7b645140a1f0231e4022d79b580cd76b83c71634e7177a2a821ed61fdf32 = $this->env->getExtension("native_profiler");
        $__internal_f53b7b645140a1f0231e4022d79b580cd76b83c71634e7177a2a821ed61fdf32->enter($__internal_f53b7b645140a1f0231e4022d79b580cd76b83c71634e7177a2a821ed61fdf32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_f53b7b645140a1f0231e4022d79b580cd76b83c71634e7177a2a821ed61fdf32->leave($__internal_f53b7b645140a1f0231e4022d79b580cd76b83c71634e7177a2a821ed61fdf32_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
