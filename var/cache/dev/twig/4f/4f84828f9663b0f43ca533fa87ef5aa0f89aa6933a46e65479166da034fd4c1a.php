<?php

/* :Subject:edit.html.twig */
class __TwigTemplate_41755686685a0d3f7ec7d26434cc6290610faf9067319716f3a6317e6361664e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate(":Subject:layout.html.twig", ":Subject:edit.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return ":Subject:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ea3dfc4ccc37614b6a35ce2d3b462b7d37f953f8ee2d6fd5bf15f5a5192f53a = $this->env->getExtension("native_profiler");
        $__internal_0ea3dfc4ccc37614b6a35ce2d3b462b7d37f953f8ee2d6fd5bf15f5a5192f53a->enter($__internal_0ea3dfc4ccc37614b6a35ce2d3b462b7d37f953f8ee2d6fd5bf15f5a5192f53a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Subject:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0ea3dfc4ccc37614b6a35ce2d3b462b7d37f953f8ee2d6fd5bf15f5a5192f53a->leave($__internal_0ea3dfc4ccc37614b6a35ce2d3b462b7d37f953f8ee2d6fd5bf15f5a5192f53a_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_aab136e75e03278f01903623853041e776dc104c1a50dd6f89cd016626218237 = $this->env->getExtension("native_profiler");
        $__internal_aab136e75e03278f01903623853041e776dc104c1a50dd6f89cd016626218237->enter($__internal_aab136e75e03278f01903623853041e776dc104c1a50dd6f89cd016626218237_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_aab136e75e03278f01903623853041e776dc104c1a50dd6f89cd016626218237->leave($__internal_aab136e75e03278f01903623853041e776dc104c1a50dd6f89cd016626218237_prof);

    }

    // line 8
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_c961a083c36a67db06b9ba5aa2bb58bc03081ce903be589cfbb5fdd8696e9729 = $this->env->getExtension("native_profiler");
        $__internal_c961a083c36a67db06b9ba5aa2bb58bc03081ce903be589cfbb5fdd8696e9729->enter($__internal_c961a083c36a67db06b9ba5aa2bb58bc03081ce903be589cfbb5fdd8696e9729_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 9
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_c961a083c36a67db06b9ba5aa2bb58bc03081ce903be589cfbb5fdd8696e9729->leave($__internal_c961a083c36a67db06b9ba5aa2bb58bc03081ce903be589cfbb5fdd8696e9729_prof);

    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        $__internal_263510369d6da2a7996fbd1abd691371e677b71da8e83d3de13a3311e6b86a17 = $this->env->getExtension("native_profiler");
        $__internal_263510369d6da2a7996fbd1abd691371e677b71da8e83d3de13a3311e6b86a17->enter($__internal_263510369d6da2a7996fbd1abd691371e677b71da8e83d3de13a3311e6b86a17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "    ";
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 17
        echo "    <h2>
        <center>Uredi predmet</center>
    </h2>

    ";
        // line 21
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
    ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'widget');
        echo "
    <div>
        <input class=\"btn btn-primary btn-block\" type=\"submit\" value=\"Uredi\"/>
    </div>

    ";
        // line 27
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "


    ";
        // line 30
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
    <input class=\"btn btn-primary btn-block\" type=\"submit\" value=\"Obriši\">
    ";
        // line 32
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
";
        
        $__internal_263510369d6da2a7996fbd1abd691371e677b71da8e83d3de13a3311e6b86a17->leave($__internal_263510369d6da2a7996fbd1abd691371e677b71da8e83d3de13a3311e6b86a17_prof);

    }

    public function getTemplateName()
    {
        return ":Subject:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 32,  111 => 30,  105 => 27,  97 => 22,  93 => 21,  87 => 17,  84 => 16,  78 => 15,  67 => 10,  62 => 9,  56 => 8,  47 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends  ':Subject:layout.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     {% form_theme edit_form 'bootstrap_3_layout.html.twig' %}*/
/*     <h2>*/
/*         <center>Uredi predmet</center>*/
/*     </h2>*/
/* */
/*     {{ form_start(edit_form) }}*/
/*     {{ form_widget(edit_form) }}*/
/*     <div>*/
/*         <input class="btn btn-primary btn-block" type="submit" value="Uredi"/>*/
/*     </div>*/
/* */
/*     {{ form_end(edit_form) }}*/
/* */
/* */
/*     {{ form_start(delete_form) }}*/
/*     <input class="btn btn-primary btn-block" type="submit" value="Obriši">*/
/*     {{ form_end(delete_form) }}*/
/* {% endblock %}*/
