<?php

/* :Student:layout.html.twig */
class __TwigTemplate_5e2ea5a7c16fca3fe3c339180784f3b9c10a788fadc21fddcd5565a87848a346 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Student:layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5528eea2a7c41b768f3920ba697d9ee1c3dee8917c14f3f22fc414123de4d061 = $this->env->getExtension("native_profiler");
        $__internal_5528eea2a7c41b768f3920ba697d9ee1c3dee8917c14f3f22fc414123de4d061->enter($__internal_5528eea2a7c41b768f3920ba697d9ee1c3dee8917c14f3f22fc414123de4d061_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Student:layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5528eea2a7c41b768f3920ba697d9ee1c3dee8917c14f3f22fc414123de4d061->leave($__internal_5528eea2a7c41b768f3920ba697d9ee1c3dee8917c14f3f22fc414123de4d061_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8a2537d74027646a1fe31a6e81875e1d7241308218de0d3c845875b9f421ec29 = $this->env->getExtension("native_profiler");
        $__internal_8a2537d74027646a1fe31a6e81875e1d7241308218de0d3c845875b9f421ec29->enter($__internal_8a2537d74027646a1fe31a6e81875e1d7241308218de0d3c845875b9f421ec29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_8a2537d74027646a1fe31a6e81875e1d7241308218de0d3c845875b9f421ec29->leave($__internal_8a2537d74027646a1fe31a6e81875e1d7241308218de0d3c845875b9f421ec29_prof);

    }

    // line 6
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_d649dbc3aa602b2c32f68d1655d750f631dcdce1136edf57ec29f5b99983a087 = $this->env->getExtension("native_profiler");
        $__internal_d649dbc3aa602b2c32f68d1655d750f631dcdce1136edf57ec29f5b99983a087->enter($__internal_d649dbc3aa602b2c32f68d1655d750f631dcdce1136edf57ec29f5b99983a087_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 7
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 26
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 28
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

";
        
        $__internal_d649dbc3aa602b2c32f68d1655d750f631dcdce1136edf57ec29f5b99983a087->leave($__internal_d649dbc3aa602b2c32f68d1655d750f631dcdce1136edf57ec29f5b99983a087_prof);

    }

    // line 26
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_6450bf88c364c3d886d237223f0b8d3b1d842461d9644a45bdb171e5b7bcdec8 = $this->env->getExtension("native_profiler");
        $__internal_6450bf88c364c3d886d237223f0b8d3b1d842461d9644a45bdb171e5b7bcdec8->enter($__internal_6450bf88c364c3d886d237223f0b8d3b1d842461d9644a45bdb171e5b7bcdec8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 27
        echo "                    ";
        
        $__internal_6450bf88c364c3d886d237223f0b8d3b1d842461d9644a45bdb171e5b7bcdec8->leave($__internal_6450bf88c364c3d886d237223f0b8d3b1d842461d9644a45bdb171e5b7bcdec8_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_fa687932670c42906e5232bf581d44f99d672d24643aef401a0fe2fe319e81a2 = $this->env->getExtension("native_profiler");
        $__internal_fa687932670c42906e5232bf581d44f99d672d24643aef401a0fe2fe319e81a2->enter($__internal_fa687932670c42906e5232bf581d44f99d672d24643aef401a0fe2fe319e81a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>

";
        
        $__internal_fa687932670c42906e5232bf581d44f99d672d24643aef401a0fe2fe319e81a2->leave($__internal_fa687932670c42906e5232bf581d44f99d672d24643aef401a0fe2fe319e81a2_prof);

    }

    public function getTemplateName()
    {
        return ":Student:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 39,  120 => 38,  116 => 37,  110 => 36,  103 => 27,  97 => 26,  85 => 28,  83 => 26,  72 => 18,  59 => 7,  53 => 6,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                     {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/* {{ parent() }}*/
/*     <script src="{{ asset('bundles/fosjsrouting/js/router.js') }}"></script>*/
/*     <script src="{{ path('fos_js_routing_js', { callback: 'fos.Router.setData' }) }}"></script>*/
/* */
/* {% endblock %}*/
