<?php

/* Mentor/mentor_studenti.html.twig */
class __TwigTemplate_569e6d45f538ae627df8516cda533463e82a11d0a050e30f26416017f5bb9718 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Student/layout.html.twig", "Mentor/mentor_studenti.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Student/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec995cc346a32cee417f75f969b82fe705a4b0f70d9e795796a9ee6b1cef953b = $this->env->getExtension("native_profiler");
        $__internal_ec995cc346a32cee417f75f969b82fe705a4b0f70d9e795796a9ee6b1cef953b->enter($__internal_ec995cc346a32cee417f75f969b82fe705a4b0f70d9e795796a9ee6b1cef953b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Mentor/mentor_studenti.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ec995cc346a32cee417f75f969b82fe705a4b0f70d9e795796a9ee6b1cef953b->leave($__internal_ec995cc346a32cee417f75f969b82fe705a4b0f70d9e795796a9ee6b1cef953b_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_e58cb908c8d4b5b8f9ffbeaa406ace85fcdc7d03d577cdcae8d03e56a7714df5 = $this->env->getExtension("native_profiler");
        $__internal_e58cb908c8d4b5b8f9ffbeaa406ace85fcdc7d03d577cdcae8d03e56a7714df5->enter($__internal_e58cb908c8d4b5b8f9ffbeaa406ace85fcdc7d03d577cdcae8d03e56a7714df5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/table_student.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_e58cb908c8d4b5b8f9ffbeaa406ace85fcdc7d03d577cdcae8d03e56a7714df5->leave($__internal_e58cb908c8d4b5b8f9ffbeaa406ace85fcdc7d03d577cdcae8d03e56a7714df5_prof);

    }

    // line 6
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_9e62c5f7a9e78df1e0d4e714d870b68364aa75390085616aca305c38a3b78c8a = $this->env->getExtension("native_profiler");
        $__internal_9e62c5f7a9e78df1e0d4e714d870b68364aa75390085616aca305c38a3b78c8a->enter($__internal_9e62c5f7a9e78df1e0d4e714d870b68364aa75390085616aca305c38a3b78c8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 7
        echo "    <li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_9e62c5f7a9e78df1e0d4e714d870b68364aa75390085616aca305c38a3b78c8a->leave($__internal_9e62c5f7a9e78df1e0d4e714d870b68364aa75390085616aca305c38a3b78c8a_prof);

    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        $__internal_7e106fa9f6bec0cbb5a9c294cf881f67b820137042f6598d971831f06b874e80 = $this->env->getExtension("native_profiler");
        $__internal_7e106fa9f6bec0cbb5a9c294cf881f67b820137042f6598d971831f06b874e80->enter($__internal_7e106fa9f6bec0cbb5a9c294cf881f67b820137042f6598d971831f06b874e80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 15
        echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-8 col-md-offset-2\">

                <table class=\"table\">
                    <thead>
                    <tr>
                        <th>Student</th>
                    </tr>
                    </thead>
                    <tbody id=\"lista_studenata\">
                    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["studenti"]) ? $context["studenti"] : $this->getContext($context, "studenti")));
        foreach ($context['_seq'] as $context["_key"] => $context["student"]) {
            // line 27
            echo "                        <tr>
                            <td><a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mentor_studentList", array("id" => $this->getAttribute($context["student"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["student"], "username", array()), "html", null, true);
            echo "</a></td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['student'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
";
        
        $__internal_7e106fa9f6bec0cbb5a9c294cf881f67b820137042f6598d971831f06b874e80->leave($__internal_7e106fa9f6bec0cbb5a9c294cf881f67b820137042f6598d971831f06b874e80_prof);

    }

    // line 39
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7fdb61bcdc28ca651ba320263576dd974359a4fe43289328de23e596a16566f3 = $this->env->getExtension("native_profiler");
        $__internal_7fdb61bcdc28ca651ba320263576dd974359a4fe43289328de23e596a16566f3->enter($__internal_7fdb61bcdc28ca651ba320263576dd974359a4fe43289328de23e596a16566f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 40
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/mentor_studenti.js"), "html", null, true);
        echo "\" ></script>
";
        
        $__internal_7fdb61bcdc28ca651ba320263576dd974359a4fe43289328de23e596a16566f3->leave($__internal_7fdb61bcdc28ca651ba320263576dd974359a4fe43289328de23e596a16566f3_prof);

    }

    public function getTemplateName()
    {
        return "Mentor/mentor_studenti.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 41,  134 => 40,  128 => 39,  116 => 31,  105 => 28,  102 => 27,  98 => 26,  85 => 15,  79 => 14,  68 => 8,  63 => 7,  57 => 6,  48 => 4,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'Student/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/table_student.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* {% block navBarLinks %}*/
/*     <li class="active"><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* */
/* {% block content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row">*/
/*             <div class="col-md-8 col-md-offset-2">*/
/* */
/*                 <table class="table">*/
/*                     <thead>*/
/*                     <tr>*/
/*                         <th>Student</th>*/
/*                     </tr>*/
/*                     </thead>*/
/*                     <tbody id="lista_studenata">*/
/*                     {% for student in studenti %}*/
/*                         <tr>*/
/*                             <td><a href="{{ path('mentor_studentList', {'id': student.id}) }}">{{ student.username }}</a></td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('js/mentor_studenti.js') }}" ></script>*/
/* {% endblock %}*/
