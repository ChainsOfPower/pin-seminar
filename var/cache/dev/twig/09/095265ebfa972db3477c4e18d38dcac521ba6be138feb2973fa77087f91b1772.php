<?php

/* @Twig/Exception/error.txt.twig */
class __TwigTemplate_44ec6fdade201cfe3e78023025e8ae7d8a0823e168bc787c33237ad719d6546a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af5a08f6c16f8b11477e0d3458f19fa1c5ed46fc0f5d180e15c63f3c3bba5806 = $this->env->getExtension("native_profiler");
        $__internal_af5a08f6c16f8b11477e0d3458f19fa1c5ed46fc0f5d180e15c63f3c3bba5806->enter($__internal_af5a08f6c16f8b11477e0d3458f19fa1c5ed46fc0f5d180e15c63f3c3bba5806_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_af5a08f6c16f8b11477e0d3458f19fa1c5ed46fc0f5d180e15c63f3c3bba5806->leave($__internal_af5a08f6c16f8b11477e0d3458f19fa1c5ed46fc0f5d180e15c63f3c3bba5806_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
