<?php

/* @FOSUser/Resetting/request_content.html.twig */
class __TwigTemplate_5d504502c5735679027d89963f985943f8a26bf514bfcee4aa42d9753e972256 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e58c2960fdede074aab86da3275df867a9dfd84d9524442629a593ea77e9350a = $this->env->getExtension("native_profiler");
        $__internal_e58c2960fdede074aab86da3275df867a9dfd84d9524442629a593ea77e9350a->enter($__internal_e58c2960fdede074aab86da3275df867a9dfd84d9524442629a593ea77e9350a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request_content.html.twig"));

        // line 2
        echo "
<div class=\"container\">
    <form action=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"fos_user_resetting_request\">
        <div>
            ";
        // line 6
        if (array_key_exists("invalid_username", $context)) {
            // line 7
            echo "                <center><p class=\"text-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.invalid_username", array("%username%" => (isset($context["invalid_username"]) ? $context["invalid_username"] : $this->getContext($context, "invalid_username"))), "FOSUserBundle"), "html", null, true);
            echo "</p></center>
            ";
        }
        // line 9
        echo "            <label for=\"username\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" class=\"form-control\" />
        </div>
        <div>
            <input type=\"submit\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"btn btn-primary btn-block\" />
        </div>
    </form>
</div>

";
        
        $__internal_e58c2960fdede074aab86da3275df867a9dfd84d9524442629a593ea77e9350a->leave($__internal_e58c2960fdede074aab86da3275df867a9dfd84d9524442629a593ea77e9350a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 13,  39 => 9,  33 => 7,  31 => 6,  26 => 4,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="container">*/
/*     <form action="{{ path('fos_user_resetting_send_email') }}" method="POST" class="fos_user_resetting_request">*/
/*         <div>*/
/*             {% if invalid_username is defined %}*/
/*                 <center><p class="text-danger">{{ 'resetting.request.invalid_username'|trans({'%username%': invalid_username}) }}</p></center>*/
/*             {% endif %}*/
/*             <label for="username">{{ 'resetting.request.username'|trans }}</label>*/
/*             <input type="text" id="username" name="username" required="required" class="form-control" />*/
/*         </div>*/
/*         <div>*/
/*             <input type="submit" value="{{ 'resetting.request.submit'|trans }}" class="btn btn-primary btn-block" />*/
/*         </div>*/
/*     </form>*/
/* </div>*/
/* */
/* */
