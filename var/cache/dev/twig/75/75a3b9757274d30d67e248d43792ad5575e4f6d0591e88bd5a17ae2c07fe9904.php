<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_8c14e934cd301c404ac36348548232dc02c9698873d9c2aa7fdfb500d29abb8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec73a996e85f3fad1efb3bd3a0169518a9ff8a8e0c88317a8b38dc55b7d9c412 = $this->env->getExtension("native_profiler");
        $__internal_ec73a996e85f3fad1efb3bd3a0169518a9ff8a8e0c88317a8b38dc55b7d9c412->enter($__internal_ec73a996e85f3fad1efb3bd3a0169518a9ff8a8e0c88317a8b38dc55b7d9c412_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ec73a996e85f3fad1efb3bd3a0169518a9ff8a8e0c88317a8b38dc55b7d9c412->leave($__internal_ec73a996e85f3fad1efb3bd3a0169518a9ff8a8e0c88317a8b38dc55b7d9c412_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_dfc050efd54a1930212faebb9756c1b0cab08e0743347f229b4a91758eb6f6f8 = $this->env->getExtension("native_profiler");
        $__internal_dfc050efd54a1930212faebb9756c1b0cab08e0743347f229b4a91758eb6f6f8->enter($__internal_dfc050efd54a1930212faebb9756c1b0cab08e0743347f229b4a91758eb6f6f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_dfc050efd54a1930212faebb9756c1b0cab08e0743347f229b4a91758eb6f6f8->leave($__internal_dfc050efd54a1930212faebb9756c1b0cab08e0743347f229b4a91758eb6f6f8_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_b2ab0c82afdbdf0c71eb3b1a0635519aa3d7721441be2e3db36233769cf26668 = $this->env->getExtension("native_profiler");
        $__internal_b2ab0c82afdbdf0c71eb3b1a0635519aa3d7721441be2e3db36233769cf26668->enter($__internal_b2ab0c82afdbdf0c71eb3b1a0635519aa3d7721441be2e3db36233769cf26668_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_b2ab0c82afdbdf0c71eb3b1a0635519aa3d7721441be2e3db36233769cf26668->leave($__internal_b2ab0c82afdbdf0c71eb3b1a0635519aa3d7721441be2e3db36233769cf26668_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5a48b601e571e9afa509f0c606a4eba73a65fbafcc440f4bb57a7072254519ab = $this->env->getExtension("native_profiler");
        $__internal_5a48b601e571e9afa509f0c606a4eba73a65fbafcc440f4bb57a7072254519ab->enter($__internal_5a48b601e571e9afa509f0c606a4eba73a65fbafcc440f4bb57a7072254519ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_5a48b601e571e9afa509f0c606a4eba73a65fbafcc440f4bb57a7072254519ab->leave($__internal_5a48b601e571e9afa509f0c606a4eba73a65fbafcc440f4bb57a7072254519ab_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
