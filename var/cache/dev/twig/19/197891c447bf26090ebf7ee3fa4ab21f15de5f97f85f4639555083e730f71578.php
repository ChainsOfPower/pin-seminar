<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_1fe390c0cb24a59631eeee1fac3938ca2976b0ae7766b18d503fb10c608cbb12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f40cfa732aaca2455100ef434d50541d4643263520df22a1f1bed053b7452dca = $this->env->getExtension("native_profiler");
        $__internal_f40cfa732aaca2455100ef434d50541d4643263520df22a1f1bed053b7452dca->enter($__internal_f40cfa732aaca2455100ef434d50541d4643263520df22a1f1bed053b7452dca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_f40cfa732aaca2455100ef434d50541d4643263520df22a1f1bed053b7452dca->leave($__internal_f40cfa732aaca2455100ef434d50541d4643263520df22a1f1bed053b7452dca_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
