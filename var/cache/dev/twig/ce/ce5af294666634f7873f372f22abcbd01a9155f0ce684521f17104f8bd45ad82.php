<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_3780d20a062f99e37df037413c0601b7b325f320fcde23b467db1ba6804e1000 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eeb5f9b0165f45686a8ebfb14eb409293040c88a04c9214abce66cc0dea4b510 = $this->env->getExtension("native_profiler");
        $__internal_eeb5f9b0165f45686a8ebfb14eb409293040c88a04c9214abce66cc0dea4b510->enter($__internal_eeb5f9b0165f45686a8ebfb14eb409293040c88a04c9214abce66cc0dea4b510_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_eeb5f9b0165f45686a8ebfb14eb409293040c88a04c9214abce66cc0dea4b510->leave($__internal_eeb5f9b0165f45686a8ebfb14eb409293040c88a04c9214abce66cc0dea4b510_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
