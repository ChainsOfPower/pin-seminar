<?php

/* :Mentor:mentor_studenti.html.twig */
class __TwigTemplate_4aa9f5154bd6d54a63d1791ab6cd7a14312dc6900478d761f0341db7a32e290f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Student/layout.html.twig", ":Mentor:mentor_studenti.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Student/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9920b1eacf618b509e1f5ad7a7ed9d228c9d1dcf48c2aebde46c3cdc08461c8 = $this->env->getExtension("native_profiler");
        $__internal_a9920b1eacf618b509e1f5ad7a7ed9d228c9d1dcf48c2aebde46c3cdc08461c8->enter($__internal_a9920b1eacf618b509e1f5ad7a7ed9d228c9d1dcf48c2aebde46c3cdc08461c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Mentor:mentor_studenti.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a9920b1eacf618b509e1f5ad7a7ed9d228c9d1dcf48c2aebde46c3cdc08461c8->leave($__internal_a9920b1eacf618b509e1f5ad7a7ed9d228c9d1dcf48c2aebde46c3cdc08461c8_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_1ac6eed65ab952896f7cfd6aec91cc34a75086fc3b861d18307da975f195415a = $this->env->getExtension("native_profiler");
        $__internal_1ac6eed65ab952896f7cfd6aec91cc34a75086fc3b861d18307da975f195415a->enter($__internal_1ac6eed65ab952896f7cfd6aec91cc34a75086fc3b861d18307da975f195415a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/table_student.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_1ac6eed65ab952896f7cfd6aec91cc34a75086fc3b861d18307da975f195415a->leave($__internal_1ac6eed65ab952896f7cfd6aec91cc34a75086fc3b861d18307da975f195415a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5c80989c95af5c19597f0d424e0027eb28720f05ab7c36f1d8736e80b8456af7 = $this->env->getExtension("native_profiler");
        $__internal_5c80989c95af5c19597f0d424e0027eb28720f05ab7c36f1d8736e80b8456af7->enter($__internal_5c80989c95af5c19597f0d424e0027eb28720f05ab7c36f1d8736e80b8456af7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    Popis studenata
";
        
        $__internal_5c80989c95af5c19597f0d424e0027eb28720f05ab7c36f1d8736e80b8456af7->leave($__internal_5c80989c95af5c19597f0d424e0027eb28720f05ab7c36f1d8736e80b8456af7_prof);

    }

    // line 11
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_2463d6bb7ac73b5e8acb793466de1510a5a4c5fe5073a0044ed0aa0e4ddd84c7 = $this->env->getExtension("native_profiler");
        $__internal_2463d6bb7ac73b5e8acb793466de1510a5a4c5fe5073a0044ed0aa0e4ddd84c7->enter($__internal_2463d6bb7ac73b5e8acb793466de1510a5a4c5fe5073a0044ed0aa0e4ddd84c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 12
        echo "    <li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_2463d6bb7ac73b5e8acb793466de1510a5a4c5fe5073a0044ed0aa0e4ddd84c7->leave($__internal_2463d6bb7ac73b5e8acb793466de1510a5a4c5fe5073a0044ed0aa0e4ddd84c7_prof);

    }

    // line 19
    public function block_content($context, array $blocks = array())
    {
        $__internal_c6049946998aa573e7d25f0fedd798b9b6694e50008cd8a99576ea4b03311e04 = $this->env->getExtension("native_profiler");
        $__internal_c6049946998aa573e7d25f0fedd798b9b6694e50008cd8a99576ea4b03311e04->enter($__internal_c6049946998aa573e7d25f0fedd798b9b6694e50008cd8a99576ea4b03311e04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 20
        echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-8 col-md-offset-2\">

                <table class=\"table\">
                    <thead>
                    <tr>
                        <th>Student</th>
                    </tr>
                    </thead>
                    <tbody id=\"lista_studenata\">
                    ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["studenti"]) ? $context["studenti"] : $this->getContext($context, "studenti")));
        foreach ($context['_seq'] as $context["_key"] => $context["student"]) {
            // line 32
            echo "                        <tr>
                            <td>
                                <a href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mentor_studentList", array("id" => $this->getAttribute($context["student"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["student"], "username", array()), "html", null, true);
            echo "</a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['student'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
";
        
        $__internal_c6049946998aa573e7d25f0fedd798b9b6694e50008cd8a99576ea4b03311e04->leave($__internal_c6049946998aa573e7d25f0fedd798b9b6694e50008cd8a99576ea4b03311e04_prof);

    }

    // line 46
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_30f742a823f53206ea9d62a9d4571042e2b55e6b34a16a438615c5f0ab9c9c30 = $this->env->getExtension("native_profiler");
        $__internal_30f742a823f53206ea9d62a9d4571042e2b55e6b34a16a438615c5f0ab9c9c30->enter($__internal_30f742a823f53206ea9d62a9d4571042e2b55e6b34a16a438615c5f0ab9c9c30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 47
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/mentor_studenti.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_30f742a823f53206ea9d62a9d4571042e2b55e6b34a16a438615c5f0ab9c9c30->leave($__internal_30f742a823f53206ea9d62a9d4571042e2b55e6b34a16a438615c5f0ab9c9c30_prof);

    }

    public function getTemplateName()
    {
        return ":Mentor:mentor_studenti.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 48,  151 => 47,  145 => 46,  133 => 38,  121 => 34,  117 => 32,  113 => 31,  100 => 20,  94 => 19,  83 => 13,  78 => 12,  72 => 11,  64 => 8,  58 => 7,  49 => 4,  44 => 3,  38 => 2,  11 => 1,);
    }
}
/* {% extends 'Student/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/table_student.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     Popis studenata*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li class="active"><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* */
/* {% block content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row">*/
/*             <div class="col-md-8 col-md-offset-2">*/
/* */
/*                 <table class="table">*/
/*                     <thead>*/
/*                     <tr>*/
/*                         <th>Student</th>*/
/*                     </tr>*/
/*                     </thead>*/
/*                     <tbody id="lista_studenata">*/
/*                     {% for student in studenti %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 <a href="{{ path('mentor_studentList', {'id': student.id}) }}">{{ student.username }}</a>*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('js/mentor_studenti.js') }}"></script>*/
/* {% endblock %}*/
