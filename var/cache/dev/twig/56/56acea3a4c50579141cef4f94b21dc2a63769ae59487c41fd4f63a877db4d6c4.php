<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_936f7e6dfc4d67124645fb69ed60573f06bf719d9ce802c76735897aecf817d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73890a17263d5adfb8f1cd8bc9a67b282c25d96effc27f66beec709255205a0e = $this->env->getExtension("native_profiler");
        $__internal_73890a17263d5adfb8f1cd8bc9a67b282c25d96effc27f66beec709255205a0e->enter($__internal_73890a17263d5adfb8f1cd8bc9a67b282c25d96effc27f66beec709255205a0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_73890a17263d5adfb8f1cd8bc9a67b282c25d96effc27f66beec709255205a0e->leave($__internal_73890a17263d5adfb8f1cd8bc9a67b282c25d96effc27f66beec709255205a0e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
