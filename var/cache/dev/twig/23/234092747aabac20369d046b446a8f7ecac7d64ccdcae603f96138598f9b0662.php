<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_516907a5854b890fcc7c8c62e0d472bce7022556117c631e204c3687382bb9fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b69a8aa2fa8c0e10feb5ddd72afb0d864f2234becb51f67ab1962008ee5f9ac5 = $this->env->getExtension("native_profiler");
        $__internal_b69a8aa2fa8c0e10feb5ddd72afb0d864f2234becb51f67ab1962008ee5f9ac5->enter($__internal_b69a8aa2fa8c0e10feb5ddd72afb0d864f2234becb51f67ab1962008ee5f9ac5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_b69a8aa2fa8c0e10feb5ddd72afb0d864f2234becb51f67ab1962008ee5f9ac5->leave($__internal_b69a8aa2fa8c0e10feb5ddd72afb0d864f2234becb51f67ab1962008ee5f9ac5_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
