<?php

/* Subject/new.html.twig */
class __TwigTemplate_709b4f54b0f70d47342565d6d21cf9e3795f5c95b22edeab05ec673aef0b3e38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate(":Subject:layout.html.twig", "Subject/new.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return ":Subject:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d59c7e7f36b4228149b499a60994266f9277ecf784fe053450f97426a47a298 = $this->env->getExtension("native_profiler");
        $__internal_2d59c7e7f36b4228149b499a60994266f9277ecf784fe053450f97426a47a298->enter($__internal_2d59c7e7f36b4228149b499a60994266f9277ecf784fe053450f97426a47a298_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Subject/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2d59c7e7f36b4228149b499a60994266f9277ecf784fe053450f97426a47a298->leave($__internal_2d59c7e7f36b4228149b499a60994266f9277ecf784fe053450f97426a47a298_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_5a8f27c18464f722135f6cec9341dad4cac9866b694b7e0700c9e325b662c6ca = $this->env->getExtension("native_profiler");
        $__internal_5a8f27c18464f722135f6cec9341dad4cac9866b694b7e0700c9e325b662c6ca->enter($__internal_5a8f27c18464f722135f6cec9341dad4cac9866b694b7e0700c9e325b662c6ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_5a8f27c18464f722135f6cec9341dad4cac9866b694b7e0700c9e325b662c6ca->leave($__internal_5a8f27c18464f722135f6cec9341dad4cac9866b694b7e0700c9e325b662c6ca_prof);

    }

    // line 8
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_0ac1f9e67292bf550e41fe168d1fb4222977985a76933757fd90e3688ceedb65 = $this->env->getExtension("native_profiler");
        $__internal_0ac1f9e67292bf550e41fe168d1fb4222977985a76933757fd90e3688ceedb65->enter($__internal_0ac1f9e67292bf550e41fe168d1fb4222977985a76933757fd90e3688ceedb65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 9
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_0ac1f9e67292bf550e41fe168d1fb4222977985a76933757fd90e3688ceedb65->leave($__internal_0ac1f9e67292bf550e41fe168d1fb4222977985a76933757fd90e3688ceedb65_prof);

    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        $__internal_7eac5458b44112b5ea3b50e33108be9f76b8314bd679671d5be8b51c579f1be5 = $this->env->getExtension("native_profiler");
        $__internal_7eac5458b44112b5ea3b50e33108be9f76b8314bd679671d5be8b51c579f1be5->enter($__internal_7eac5458b44112b5ea3b50e33108be9f76b8314bd679671d5be8b51c579f1be5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "
    ";
        // line 17
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 18
        echo "    <h2><center>Kreiraj predmet</center></h2>

    ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input class=\"btn btn-primary btn-block\" type=\"submit\" value=\"Kreiraj\"/>
    </div>
    ";
        // line 25
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_7eac5458b44112b5ea3b50e33108be9f76b8314bd679671d5be8b51c579f1be5->leave($__internal_7eac5458b44112b5ea3b50e33108be9f76b8314bd679671d5be8b51c579f1be5_prof);

    }

    public function getTemplateName()
    {
        return "Subject/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 25,  97 => 21,  93 => 20,  89 => 18,  87 => 17,  84 => 16,  78 => 15,  67 => 10,  62 => 9,  56 => 8,  47 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends ':Subject:layout.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     {% form_theme form 'bootstrap_3_layout.html.twig' %}*/
/*     <h2><center>Kreiraj predmet</center></h2>*/
/* */
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     <div>*/
/*         <input class="btn btn-primary btn-block" type="submit" value="Kreiraj"/>*/
/*     </div>*/
/*     {{ form_end(form) }}*/
/* {% endblock %}*/
