<?php

/* FOSUserBundle:Resetting:checkEmail.html.twig */
class __TwigTemplate_2ab7b7d88ee27c5ade659fa6364732782a5a238dc7d31f15823963fde6e21011 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:checkEmail.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_11cc61ff9dd7c6f399389251f0e5ecbf578d5c24f0ae695302277002e9aa55ae = $this->env->getExtension("native_profiler");
        $__internal_11cc61ff9dd7c6f399389251f0e5ecbf578d5c24f0ae695302277002e9aa55ae->enter($__internal_11cc61ff9dd7c6f399389251f0e5ecbf578d5c24f0ae695302277002e9aa55ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_11cc61ff9dd7c6f399389251f0e5ecbf578d5c24f0ae695302277002e9aa55ae->leave($__internal_11cc61ff9dd7c6f399389251f0e5ecbf578d5c24f0ae695302277002e9aa55ae_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_d5a08221f243038d7cde29e4d1d93bf8093b031ad4bddb81c27216c5027beaea = $this->env->getExtension("native_profiler");
        $__internal_d5a08221f243038d7cde29e4d1d93bf8093b031ad4bddb81c27216c5027beaea->enter($__internal_d5a08221f243038d7cde29e4d1d93bf8093b031ad4bddb81c27216c5027beaea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_d5a08221f243038d7cde29e4d1d93bf8093b031ad4bddb81c27216c5027beaea->leave($__internal_d5a08221f243038d7cde29e4d1d93bf8093b031ad4bddb81c27216c5027beaea_prof);

    }

    // line 7
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_d8d5d9e5266eeb4f5689e724a89d617f49c924d51fdbf7477efe5f7bf795aded = $this->env->getExtension("native_profiler");
        $__internal_d8d5d9e5266eeb4f5689e724a89d617f49c924d51fdbf7477efe5f7bf795aded->enter($__internal_d8d5d9e5266eeb4f5689e724a89d617f49c924d51fdbf7477efe5f7bf795aded_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 8
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_d8d5d9e5266eeb4f5689e724a89d617f49c924d51fdbf7477efe5f7bf795aded->leave($__internal_d8d5d9e5266eeb4f5689e724a89d617f49c924d51fdbf7477efe5f7bf795aded_prof);

    }

    // line 14
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c11ea1cb8f51dda96a19844771538dbb7be3ddefc05d2f8544f2b7ef80a7c866 = $this->env->getExtension("native_profiler");
        $__internal_c11ea1cb8f51dda96a19844771538dbb7be3ddefc05d2f8544f2b7ef80a7c866->enter($__internal_c11ea1cb8f51dda96a19844771538dbb7be3ddefc05d2f8544f2b7ef80a7c866_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 15
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\" style=\"background-color: rgba(255,255,255,0.8)\">
                <p>
                    ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email"))), "FOSUserBundle"), "html", null, true);
        echo "
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_c11ea1cb8f51dda96a19844771538dbb7be3ddefc05d2f8544f2b7ef80a7c866->leave($__internal_c11ea1cb8f51dda96a19844771538dbb7be3ddefc05d2f8544f2b7ef80a7c866_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 19,  73 => 15,  67 => 14,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{  parent() }}*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3" style="background-color: rgba(255,255,255,0.8)">*/
/*                 <p>*/
/*                     {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/*                 </p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
/* */
/* */
/* */
/* */
/* */
/* */
