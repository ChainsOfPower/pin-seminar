<?php

/* Student/layout.html.twig */
class __TwigTemplate_d1345031b9afad78a02db47c27ba5c175f1a561599a0a5511e86ccc0bdedbf90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "Student/layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_76a250d7c85c7a5c894ce12adb1aa4fc89ad13bb4b0ccd096150ebfdb1787647 = $this->env->getExtension("native_profiler");
        $__internal_76a250d7c85c7a5c894ce12adb1aa4fc89ad13bb4b0ccd096150ebfdb1787647->enter($__internal_76a250d7c85c7a5c894ce12adb1aa4fc89ad13bb4b0ccd096150ebfdb1787647_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Student/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_76a250d7c85c7a5c894ce12adb1aa4fc89ad13bb4b0ccd096150ebfdb1787647->leave($__internal_76a250d7c85c7a5c894ce12adb1aa4fc89ad13bb4b0ccd096150ebfdb1787647_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_75337b27abc7e92a4c61d71d30990329a5b1a50964408ce3c3c4b31e65f247e1 = $this->env->getExtension("native_profiler");
        $__internal_75337b27abc7e92a4c61d71d30990329a5b1a50964408ce3c3c4b31e65f247e1->enter($__internal_75337b27abc7e92a4c61d71d30990329a5b1a50964408ce3c3c4b31e65f247e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_75337b27abc7e92a4c61d71d30990329a5b1a50964408ce3c3c4b31e65f247e1->leave($__internal_75337b27abc7e92a4c61d71d30990329a5b1a50964408ce3c3c4b31e65f247e1_prof);

    }

    // line 6
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_f4f5ba96df38c6f0a16eb7408c379051fb7df235c9a3c3dd297b094308522b54 = $this->env->getExtension("native_profiler");
        $__internal_f4f5ba96df38c6f0a16eb7408c379051fb7df235c9a3c3dd297b094308522b54->enter($__internal_f4f5ba96df38c6f0a16eb7408c379051fb7df235c9a3c3dd297b094308522b54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 7
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 26
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 28
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

";
        
        $__internal_f4f5ba96df38c6f0a16eb7408c379051fb7df235c9a3c3dd297b094308522b54->leave($__internal_f4f5ba96df38c6f0a16eb7408c379051fb7df235c9a3c3dd297b094308522b54_prof);

    }

    // line 26
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_3828d11a3808c0261847f964c359b2ade16a514c98dd64639310da8ed9b915c7 = $this->env->getExtension("native_profiler");
        $__internal_3828d11a3808c0261847f964c359b2ade16a514c98dd64639310da8ed9b915c7->enter($__internal_3828d11a3808c0261847f964c359b2ade16a514c98dd64639310da8ed9b915c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 27
        echo "                    ";
        
        $__internal_3828d11a3808c0261847f964c359b2ade16a514c98dd64639310da8ed9b915c7->leave($__internal_3828d11a3808c0261847f964c359b2ade16a514c98dd64639310da8ed9b915c7_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_218d22e715fb08588029041412083a73af4cfcf572b8f6f5f9dc2b89b956036e = $this->env->getExtension("native_profiler");
        $__internal_218d22e715fb08588029041412083a73af4cfcf572b8f6f5f9dc2b89b956036e->enter($__internal_218d22e715fb08588029041412083a73af4cfcf572b8f6f5f9dc2b89b956036e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>

";
        
        $__internal_218d22e715fb08588029041412083a73af4cfcf572b8f6f5f9dc2b89b956036e->leave($__internal_218d22e715fb08588029041412083a73af4cfcf572b8f6f5f9dc2b89b956036e_prof);

    }

    public function getTemplateName()
    {
        return "Student/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 39,  120 => 38,  116 => 37,  110 => 36,  103 => 27,  97 => 26,  85 => 28,  83 => 26,  72 => 18,  59 => 7,  53 => 6,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                     {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/* {{ parent() }}*/
/*     <script src="{{ asset('bundles/fosjsrouting/js/router.js') }}"></script>*/
/*     <script src="{{ path('fos_js_routing_js', { callback: 'fos.Router.setData' }) }}"></script>*/
/* */
/* {% endblock %}*/
