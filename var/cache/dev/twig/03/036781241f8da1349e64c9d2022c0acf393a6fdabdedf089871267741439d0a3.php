<?php

/* @FOSUser/Group/show.html.twig */
class __TwigTemplate_d536ee7469011eeca017211854d8a5be5528373f7b8756ec09016f10432572ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Group/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdd1f9678c31c8ec9b3ed0246dddc7893939a870fd57a2956cafbd45a5636e67 = $this->env->getExtension("native_profiler");
        $__internal_bdd1f9678c31c8ec9b3ed0246dddc7893939a870fd57a2956cafbd45a5636e67->enter($__internal_bdd1f9678c31c8ec9b3ed0246dddc7893939a870fd57a2956cafbd45a5636e67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bdd1f9678c31c8ec9b3ed0246dddc7893939a870fd57a2956cafbd45a5636e67->leave($__internal_bdd1f9678c31c8ec9b3ed0246dddc7893939a870fd57a2956cafbd45a5636e67_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_17484498dc183b3ea5b33639ae98b113531f9b3aeaa8c1e80be36be022f0311a = $this->env->getExtension("native_profiler");
        $__internal_17484498dc183b3ea5b33639ae98b113531f9b3aeaa8c1e80be36be022f0311a->enter($__internal_17484498dc183b3ea5b33639ae98b113531f9b3aeaa8c1e80be36be022f0311a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:show_content.html.twig", "@FOSUser/Group/show.html.twig", 4)->display($context);
        
        $__internal_17484498dc183b3ea5b33639ae98b113531f9b3aeaa8c1e80be36be022f0311a->leave($__internal_17484498dc183b3ea5b33639ae98b113531f9b3aeaa8c1e80be36be022f0311a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
