<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_5d3a9a7a752bbb8530dc907872a3ad95fb86c70bc834d8c01a95b6418307b70e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_00cab754714d91671a717addbd14efece695c68e45d4b0405299a9226a5a49bf = $this->env->getExtension("native_profiler");
        $__internal_00cab754714d91671a717addbd14efece695c68e45d4b0405299a9226a5a49bf->enter($__internal_00cab754714d91671a717addbd14efece695c68e45d4b0405299a9226a5a49bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_00cab754714d91671a717addbd14efece695c68e45d4b0405299a9226a5a49bf->leave($__internal_00cab754714d91671a717addbd14efece695c68e45d4b0405299a9226a5a49bf_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
