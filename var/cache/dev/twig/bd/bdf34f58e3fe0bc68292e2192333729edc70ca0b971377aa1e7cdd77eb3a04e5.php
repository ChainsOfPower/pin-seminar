<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_ec35fe9fed4126c22e2e271962b53b857322cb861e70774bf986d465fe2236c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d25a773f1ff8c28ce9e818da55ed0d41ccc59a7016755ff0118a2f29d447805 = $this->env->getExtension("native_profiler");
        $__internal_4d25a773f1ff8c28ce9e818da55ed0d41ccc59a7016755ff0118a2f29d447805->enter($__internal_4d25a773f1ff8c28ce9e818da55ed0d41ccc59a7016755ff0118a2f29d447805_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_4d25a773f1ff8c28ce9e818da55ed0d41ccc59a7016755ff0118a2f29d447805->leave($__internal_4d25a773f1ff8c28ce9e818da55ed0d41ccc59a7016755ff0118a2f29d447805_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
