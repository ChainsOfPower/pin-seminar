<?php

/* subject/edit.html.twig */
class __TwigTemplate_4a129fa6a737e3586ed54a376bac0adbadaadb3d0245818210eb3b2068923f40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate(":Subject:layout.html.twig", "subject/edit.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return ":Subject:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b0808ccea1d00ae9ffaba7c599dcbbe85f00d0ff96ef628a1208cb91dbfb21d3 = $this->env->getExtension("native_profiler");
        $__internal_b0808ccea1d00ae9ffaba7c599dcbbe85f00d0ff96ef628a1208cb91dbfb21d3->enter($__internal_b0808ccea1d00ae9ffaba7c599dcbbe85f00d0ff96ef628a1208cb91dbfb21d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subject/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b0808ccea1d00ae9ffaba7c599dcbbe85f00d0ff96ef628a1208cb91dbfb21d3->leave($__internal_b0808ccea1d00ae9ffaba7c599dcbbe85f00d0ff96ef628a1208cb91dbfb21d3_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_37050eb05f5ae8833b2ad6c1d84036714c0bef015623f45184caef3b425435bb = $this->env->getExtension("native_profiler");
        $__internal_37050eb05f5ae8833b2ad6c1d84036714c0bef015623f45184caef3b425435bb->enter($__internal_37050eb05f5ae8833b2ad6c1d84036714c0bef015623f45184caef3b425435bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_37050eb05f5ae8833b2ad6c1d84036714c0bef015623f45184caef3b425435bb->leave($__internal_37050eb05f5ae8833b2ad6c1d84036714c0bef015623f45184caef3b425435bb_prof);

    }

    // line 8
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_fdf274291fe112a1d539dc1c15f5cc77bf004909bceb7fd65d1449907d394cd7 = $this->env->getExtension("native_profiler");
        $__internal_fdf274291fe112a1d539dc1c15f5cc77bf004909bceb7fd65d1449907d394cd7->enter($__internal_fdf274291fe112a1d539dc1c15f5cc77bf004909bceb7fd65d1449907d394cd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 9
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_fdf274291fe112a1d539dc1c15f5cc77bf004909bceb7fd65d1449907d394cd7->leave($__internal_fdf274291fe112a1d539dc1c15f5cc77bf004909bceb7fd65d1449907d394cd7_prof);

    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        $__internal_d9fc4c8e7897e1c2cdfb7580d598eb5dec8c67d52b618b5cfa087d835e753464 = $this->env->getExtension("native_profiler");
        $__internal_d9fc4c8e7897e1c2cdfb7580d598eb5dec8c67d52b618b5cfa087d835e753464->enter($__internal_d9fc4c8e7897e1c2cdfb7580d598eb5dec8c67d52b618b5cfa087d835e753464_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "    ";
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 17
        echo "    <h2>
        <center>Uredi predmet</center>
    </h2>

    ";
        // line 21
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
    ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'widget');
        echo "
    <div>
        <input class=\"btn btn-primary btn-block\" type=\"submit\" value=\"Uredi\"/>
    </div>

    ";
        // line 27
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "


    ";
        // line 30
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
    <input class=\"btn btn-primary btn-block\" type=\"submit\" value=\"Obriši\">
    ";
        // line 32
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
";
        
        $__internal_d9fc4c8e7897e1c2cdfb7580d598eb5dec8c67d52b618b5cfa087d835e753464->leave($__internal_d9fc4c8e7897e1c2cdfb7580d598eb5dec8c67d52b618b5cfa087d835e753464_prof);

    }

    public function getTemplateName()
    {
        return "subject/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 32,  111 => 30,  105 => 27,  97 => 22,  93 => 21,  87 => 17,  84 => 16,  78 => 15,  67 => 10,  62 => 9,  56 => 8,  47 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends  ':Subject:layout.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     {% form_theme edit_form 'bootstrap_3_layout.html.twig' %}*/
/*     <h2>*/
/*         <center>Uredi predmet</center>*/
/*     </h2>*/
/* */
/*     {{ form_start(edit_form) }}*/
/*     {{ form_widget(edit_form) }}*/
/*     <div>*/
/*         <input class="btn btn-primary btn-block" type="submit" value="Uredi"/>*/
/*     </div>*/
/* */
/*     {{ form_end(edit_form) }}*/
/* */
/* */
/*     {{ form_start(delete_form) }}*/
/*     <input class="btn btn-primary btn-block" type="submit" value="Obriši">*/
/*     {{ form_end(delete_form) }}*/
/* {% endblock %}*/
