<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_62220fd3144fa5b80ad274c843c003086a6ef143814a700c20ebaee8772508a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_037f83672c404af0553572f134ec79f505ea38f20dfd810f47e15f5f3dce90a5 = $this->env->getExtension("native_profiler");
        $__internal_037f83672c404af0553572f134ec79f505ea38f20dfd810f47e15f5f3dce90a5->enter($__internal_037f83672c404af0553572f134ec79f505ea38f20dfd810f47e15f5f3dce90a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_037f83672c404af0553572f134ec79f505ea38f20dfd810f47e15f5f3dce90a5->leave($__internal_037f83672c404af0553572f134ec79f505ea38f20dfd810f47e15f5f3dce90a5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
