<?php

/* :Subject:new.html.twig */
class __TwigTemplate_d96ae9576eeeea4b6d92d373c89a796ea1af6a08070cbe2c4298384b89efb75b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate(":Subject:layout.html.twig", ":Subject:new.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return ":Subject:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b9cab8424a7c272ea45f05662842b85f42bc308fe0ae02a98d83e3afce9643b = $this->env->getExtension("native_profiler");
        $__internal_0b9cab8424a7c272ea45f05662842b85f42bc308fe0ae02a98d83e3afce9643b->enter($__internal_0b9cab8424a7c272ea45f05662842b85f42bc308fe0ae02a98d83e3afce9643b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Subject:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b9cab8424a7c272ea45f05662842b85f42bc308fe0ae02a98d83e3afce9643b->leave($__internal_0b9cab8424a7c272ea45f05662842b85f42bc308fe0ae02a98d83e3afce9643b_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_131f17ec85e325d768539617ba6b90f4b887698ff7ccb6f9ce7ec1d159c63859 = $this->env->getExtension("native_profiler");
        $__internal_131f17ec85e325d768539617ba6b90f4b887698ff7ccb6f9ce7ec1d159c63859->enter($__internal_131f17ec85e325d768539617ba6b90f4b887698ff7ccb6f9ce7ec1d159c63859_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_131f17ec85e325d768539617ba6b90f4b887698ff7ccb6f9ce7ec1d159c63859->leave($__internal_131f17ec85e325d768539617ba6b90f4b887698ff7ccb6f9ce7ec1d159c63859_prof);

    }

    // line 8
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_b675ddd8a779a63c619bfd0e6c503fea4c2df37b7ca1a16b1ad49c1d68e8b77a = $this->env->getExtension("native_profiler");
        $__internal_b675ddd8a779a63c619bfd0e6c503fea4c2df37b7ca1a16b1ad49c1d68e8b77a->enter($__internal_b675ddd8a779a63c619bfd0e6c503fea4c2df37b7ca1a16b1ad49c1d68e8b77a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 9
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_b675ddd8a779a63c619bfd0e6c503fea4c2df37b7ca1a16b1ad49c1d68e8b77a->leave($__internal_b675ddd8a779a63c619bfd0e6c503fea4c2df37b7ca1a16b1ad49c1d68e8b77a_prof);

    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        $__internal_f08f9b5ab1b4f2a0332a7de55247a6cf5760370cbe48b85adae2d51c473a8ebc = $this->env->getExtension("native_profiler");
        $__internal_f08f9b5ab1b4f2a0332a7de55247a6cf5760370cbe48b85adae2d51c473a8ebc->enter($__internal_f08f9b5ab1b4f2a0332a7de55247a6cf5760370cbe48b85adae2d51c473a8ebc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "
    ";
        // line 17
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 18
        echo "    <h2><center>Kreiraj predmet</center></h2>

    ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input class=\"btn btn-primary btn-block\" type=\"submit\" value=\"Kreiraj\"/>
    </div>
    ";
        // line 25
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_f08f9b5ab1b4f2a0332a7de55247a6cf5760370cbe48b85adae2d51c473a8ebc->leave($__internal_f08f9b5ab1b4f2a0332a7de55247a6cf5760370cbe48b85adae2d51c473a8ebc_prof);

    }

    public function getTemplateName()
    {
        return ":Subject:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 25,  97 => 21,  93 => 20,  89 => 18,  87 => 17,  84 => 16,  78 => 15,  67 => 10,  62 => 9,  56 => 8,  47 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends ':Subject:layout.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     {% form_theme form 'bootstrap_3_layout.html.twig' %}*/
/*     <h2><center>Kreiraj predmet</center></h2>*/
/* */
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     <div>*/
/*         <input class="btn btn-primary btn-block" type="submit" value="Kreiraj"/>*/
/*     </div>*/
/*     {{ form_end(form) }}*/
/* {% endblock %}*/
