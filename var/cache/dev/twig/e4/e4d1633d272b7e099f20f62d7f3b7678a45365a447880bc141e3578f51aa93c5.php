<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_27844b35810c079a7714944649ab90356d258a7fd590754d87e2722f5592dce6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da978b368826e2989e884980ac0c66c367ccfff0d2aa57bcddd8d9a7939ee7b1 = $this->env->getExtension("native_profiler");
        $__internal_da978b368826e2989e884980ac0c66c367ccfff0d2aa57bcddd8d9a7939ee7b1->enter($__internal_da978b368826e2989e884980ac0c66c367ccfff0d2aa57bcddd8d9a7939ee7b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_da978b368826e2989e884980ac0c66c367ccfff0d2aa57bcddd8d9a7939ee7b1->leave($__internal_da978b368826e2989e884980ac0c66c367ccfff0d2aa57bcddd8d9a7939ee7b1_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_e64b3216a8e6a4bd30f922ec02ea0a8cee48b601f61083cf19e9ad6088570306 = $this->env->getExtension("native_profiler");
        $__internal_e64b3216a8e6a4bd30f922ec02ea0a8cee48b601f61083cf19e9ad6088570306->enter($__internal_e64b3216a8e6a4bd30f922ec02ea0a8cee48b601f61083cf19e9ad6088570306_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_e64b3216a8e6a4bd30f922ec02ea0a8cee48b601f61083cf19e9ad6088570306->leave($__internal_e64b3216a8e6a4bd30f922ec02ea0a8cee48b601f61083cf19e9ad6088570306_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
