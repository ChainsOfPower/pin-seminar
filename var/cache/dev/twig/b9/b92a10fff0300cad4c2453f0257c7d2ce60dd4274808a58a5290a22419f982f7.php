<?php

/* Subject/layout.html.twig */
class __TwigTemplate_bf15f20982cb3b902fbbace040ec507a0cdea16b07279638ba209e51372cd3ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "Subject/layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95d623150e796896c71f16a35d7b7e1033f0d0269beb244430d6f6c1586bee4c = $this->env->getExtension("native_profiler");
        $__internal_95d623150e796896c71f16a35d7b7e1033f0d0269beb244430d6f6c1586bee4c->enter($__internal_95d623150e796896c71f16a35d7b7e1033f0d0269beb244430d6f6c1586bee4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Subject/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_95d623150e796896c71f16a35d7b7e1033f0d0269beb244430d6f6c1586bee4c->leave($__internal_95d623150e796896c71f16a35d7b7e1033f0d0269beb244430d6f6c1586bee4c_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_86d76d18844febaf2e7f25f34e718ef5d4e2e28256bc8fd2e8daf7b128794b5d = $this->env->getExtension("native_profiler");
        $__internal_86d76d18844febaf2e7f25f34e718ef5d4e2e28256bc8fd2e8daf7b128794b5d->enter($__internal_86d76d18844febaf2e7f25f34e718ef5d4e2e28256bc8fd2e8daf7b128794b5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_86d76d18844febaf2e7f25f34e718ef5d4e2e28256bc8fd2e8daf7b128794b5d->leave($__internal_86d76d18844febaf2e7f25f34e718ef5d4e2e28256bc8fd2e8daf7b128794b5d_prof);

    }

    // line 6
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_93ae182b09b1038cfb4070cc00be66dd8621ad7308c44a55a6043fb846fe207b = $this->env->getExtension("native_profiler");
        $__internal_93ae182b09b1038cfb4070cc00be66dd8621ad7308c44a55a6043fb846fe207b->enter($__internal_93ae182b09b1038cfb4070cc00be66dd8621ad7308c44a55a6043fb846fe207b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 7
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 26
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 28
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

";
        
        $__internal_93ae182b09b1038cfb4070cc00be66dd8621ad7308c44a55a6043fb846fe207b->leave($__internal_93ae182b09b1038cfb4070cc00be66dd8621ad7308c44a55a6043fb846fe207b_prof);

    }

    // line 26
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_9cb5a82a4d4574aeb73630b3969e2e8f63ce0abd49c9e6ee031d2448d9c48ccd = $this->env->getExtension("native_profiler");
        $__internal_9cb5a82a4d4574aeb73630b3969e2e8f63ce0abd49c9e6ee031d2448d9c48ccd->enter($__internal_9cb5a82a4d4574aeb73630b3969e2e8f63ce0abd49c9e6ee031d2448d9c48ccd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 27
        echo "                    ";
        
        $__internal_9cb5a82a4d4574aeb73630b3969e2e8f63ce0abd49c9e6ee031d2448d9c48ccd->leave($__internal_9cb5a82a4d4574aeb73630b3969e2e8f63ce0abd49c9e6ee031d2448d9c48ccd_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5c43de28e82b8eefa18f75b998c2793369b00b6d5a8fa544a2dc15bb0dac006a = $this->env->getExtension("native_profiler");
        $__internal_5c43de28e82b8eefa18f75b998c2793369b00b6d5a8fa544a2dc15bb0dac006a->enter($__internal_5c43de28e82b8eefa18f75b998c2793369b00b6d5a8fa544a2dc15bb0dac006a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>
";
        
        $__internal_5c43de28e82b8eefa18f75b998c2793369b00b6d5a8fa544a2dc15bb0dac006a->leave($__internal_5c43de28e82b8eefa18f75b998c2793369b00b6d5a8fa544a2dc15bb0dac006a_prof);

    }

    public function getTemplateName()
    {
        return "Subject/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 39,  121 => 38,  116 => 37,  110 => 36,  103 => 27,  97 => 26,  85 => 28,  83 => 26,  72 => 18,  59 => 7,  53 => 6,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                     {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('bundles/fosjsrouting/js/router.js') }}"></script>*/
/*     <script src="{{ path('fos_js_routing_js', { callback: 'fos.Router.setData' }) }}"></script>*/
/* {% endblock %}*/
