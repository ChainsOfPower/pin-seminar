<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_45dfd3f23bc970ed7b505cdf8b79c2bcb35c6e4a008e2a6c420f190da5d958d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_261f3b8c1dbf485873c4dbc0541ae2b5e818df9fb296414b5194c2067fec8c69 = $this->env->getExtension("native_profiler");
        $__internal_261f3b8c1dbf485873c4dbc0541ae2b5e818df9fb296414b5194c2067fec8c69->enter($__internal_261f3b8c1dbf485873c4dbc0541ae2b5e818df9fb296414b5194c2067fec8c69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_261f3b8c1dbf485873c4dbc0541ae2b5e818df9fb296414b5194c2067fec8c69->leave($__internal_261f3b8c1dbf485873c4dbc0541ae2b5e818df9fb296414b5194c2067fec8c69_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_54e7916b831a8316a0535ad2557e71a4a24d73d773453fccfcb86f808fcdf836 = $this->env->getExtension("native_profiler");
        $__internal_54e7916b831a8316a0535ad2557e71a4a24d73d773453fccfcb86f808fcdf836->enter($__internal_54e7916b831a8316a0535ad2557e71a4a24d73d773453fccfcb86f808fcdf836_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_54e7916b831a8316a0535ad2557e71a4a24d73d773453fccfcb86f808fcdf836->leave($__internal_54e7916b831a8316a0535ad2557e71a4a24d73d773453fccfcb86f808fcdf836_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:list_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
