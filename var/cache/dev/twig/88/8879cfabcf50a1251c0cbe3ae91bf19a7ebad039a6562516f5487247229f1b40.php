<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_fb863fd1863a383a80dd8ef60aec4a39d4276715b587c2ece87082ba7bca5f22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_62758b9b55dc7de21b917a699831b40599d3c6ac622c8e946e68024bb51fb50b = $this->env->getExtension("native_profiler");
        $__internal_62758b9b55dc7de21b917a699831b40599d3c6ac622c8e946e68024bb51fb50b->enter($__internal_62758b9b55dc7de21b917a699831b40599d3c6ac622c8e946e68024bb51fb50b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_62758b9b55dc7de21b917a699831b40599d3c6ac622c8e946e68024bb51fb50b->leave($__internal_62758b9b55dc7de21b917a699831b40599d3c6ac622c8e946e68024bb51fb50b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
