<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_8739a9f60213748d177f3afc018381cb34f115ad3aa71330af96ada47bf63d83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0875d97184772ec64963f9c0a9c7e5cdea589828bf52b3edbcc08b4637c78831 = $this->env->getExtension("native_profiler");
        $__internal_0875d97184772ec64963f9c0a9c7e5cdea589828bf52b3edbcc08b4637c78831->enter($__internal_0875d97184772ec64963f9c0a9c7e5cdea589828bf52b3edbcc08b4637c78831_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "@Twig/Exception/error.atom.twig", 1)->display($context);
        
        $__internal_0875d97184772ec64963f9c0a9c7e5cdea589828bf52b3edbcc08b4637c78831->leave($__internal_0875d97184772ec64963f9c0a9c7e5cdea589828bf52b3edbcc08b4637c78831_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
