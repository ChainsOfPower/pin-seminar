<?php

/* @FOSUser/Resetting/reset.html.twig */
class __TwigTemplate_d3014165e40cacb104f68eb66081b65a4a04ecd681905d42feea62b3480f4aa5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Resetting/reset.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7bb8b702cc04a306bcae3a9cbb46e63d1010ad5e84846959950161236659e611 = $this->env->getExtension("native_profiler");
        $__internal_7bb8b702cc04a306bcae3a9cbb46e63d1010ad5e84846959950161236659e611->enter($__internal_7bb8b702cc04a306bcae3a9cbb46e63d1010ad5e84846959950161236659e611_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7bb8b702cc04a306bcae3a9cbb46e63d1010ad5e84846959950161236659e611->leave($__internal_7bb8b702cc04a306bcae3a9cbb46e63d1010ad5e84846959950161236659e611_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_cb39667204aea26221332e45db5c166709afdfe9b4ea404bba5278d2d07d2619 = $this->env->getExtension("native_profiler");
        $__internal_cb39667204aea26221332e45db5c166709afdfe9b4ea404bba5278d2d07d2619->enter($__internal_cb39667204aea26221332e45db5c166709afdfe9b4ea404bba5278d2d07d2619_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_cb39667204aea26221332e45db5c166709afdfe9b4ea404bba5278d2d07d2619->leave($__internal_cb39667204aea26221332e45db5c166709afdfe9b4ea404bba5278d2d07d2619_prof);

    }

    // line 8
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a3eeebe6af06fbd53acac0b2dc46ac8bb497724da0b86741d0f6cf2b013d3cb9 = $this->env->getExtension("native_profiler");
        $__internal_a3eeebe6af06fbd53acac0b2dc46ac8bb497724da0b86741d0f6cf2b013d3cb9->enter($__internal_a3eeebe6af06fbd53acac0b2dc46ac8bb497724da0b86741d0f6cf2b013d3cb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 9
        echo "    <h2><center>Unesi novu šifru</center></h2>
";
        // line 10
        $this->loadTemplate("FOSUserBundle:Resetting:reset_content.html.twig", "@FOSUser/Resetting/reset.html.twig", 10)->display($context);
        
        $__internal_a3eeebe6af06fbd53acac0b2dc46ac8bb497724da0b86741d0f6cf2b013d3cb9->leave($__internal_a3eeebe6af06fbd53acac0b2dc46ac8bb497724da0b86741d0f6cf2b013d3cb9_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 10,  61 => 9,  55 => 8,  46 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <h2><center>Unesi novu šifru</center></h2>*/
/* {% include "FOSUserBundle:Resetting:reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
