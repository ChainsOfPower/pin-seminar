<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_21a5ee9da32d527a8b8c17745e1c476a5c273788081348dcf7ccd2593039df92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cfb3b342e7d9a13a0c1538f3f18fbcb17ee4bdbc791c7e2088c5adf56f1d46b3 = $this->env->getExtension("native_profiler");
        $__internal_cfb3b342e7d9a13a0c1538f3f18fbcb17ee4bdbc791c7e2088c5adf56f1d46b3->enter($__internal_cfb3b342e7d9a13a0c1538f3f18fbcb17ee4bdbc791c7e2088c5adf56f1d46b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_cfb3b342e7d9a13a0c1538f3f18fbcb17ee4bdbc791c7e2088c5adf56f1d46b3->leave($__internal_cfb3b342e7d9a13a0c1538f3f18fbcb17ee4bdbc791c7e2088c5adf56f1d46b3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_rows') ?>*/
/* */
