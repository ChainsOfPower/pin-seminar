<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_f5001da045e2a4ff05d377197e85cc9f068bca757817cee756443a4e431cc3fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c976e3f6f2e967c20307149a36059945402bee4dd4faebc5b2c1ac25e632b41d = $this->env->getExtension("native_profiler");
        $__internal_c976e3f6f2e967c20307149a36059945402bee4dd4faebc5b2c1ac25e632b41d->enter($__internal_c976e3f6f2e967c20307149a36059945402bee4dd4faebc5b2c1ac25e632b41d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c976e3f6f2e967c20307149a36059945402bee4dd4faebc5b2c1ac25e632b41d->leave($__internal_c976e3f6f2e967c20307149a36059945402bee4dd4faebc5b2c1ac25e632b41d_prof);

    }

    // line 5
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_9a848f61c9a59182ab6cb9fbb4d86e440937e7611208dbf793c0a7e742eb70d8 = $this->env->getExtension("native_profiler");
        $__internal_9a848f61c9a59182ab6cb9fbb4d86e440937e7611208dbf793c0a7e742eb70d8->enter($__internal_9a848f61c9a59182ab6cb9fbb4d86e440937e7611208dbf793c0a7e742eb70d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("student_homepage");
        echo "\">Upisni list</a> </li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_9a848f61c9a59182ab6cb9fbb4d86e440937e7611208dbf793c0a7e742eb70d8->leave($__internal_9a848f61c9a59182ab6cb9fbb4d86e440937e7611208dbf793c0a7e742eb70d8_prof);

    }

    // line 11
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_53318f66f033ec87206a8476ebdc30c58966e330d900a8937321d4551c63804b = $this->env->getExtension("native_profiler");
        $__internal_53318f66f033ec87206a8476ebdc30c58966e330d900a8937321d4551c63804b->enter($__internal_53318f66f033ec87206a8476ebdc30c58966e330d900a8937321d4551c63804b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 12
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\" style=\"background-color: rgba(255,255,255,0.8)\">
                <p>";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
                ";
        // line 16
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 17
            echo "                    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
                ";
        }
        // line 19
        echo "            </div>
        </div>
    </div>
";
        
        $__internal_53318f66f033ec87206a8476ebdc30c58966e330d900a8937321d4551c63804b->leave($__internal_53318f66f033ec87206a8476ebdc30c58966e330d900a8937321d4551c63804b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 19,  70 => 17,  68 => 16,  64 => 15,  59 => 12,  53 => 11,  41 => 6,  35 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('student_homepage') }}">Upisni list</a> </li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3" style="background-color: rgba(255,255,255,0.8)">*/
/*                 <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>*/
/*                 {% if targetUrl %}*/
/*                     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*                 {% endif %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
/* */
