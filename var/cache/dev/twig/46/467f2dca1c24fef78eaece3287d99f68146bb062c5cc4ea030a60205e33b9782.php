<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_2d6384eca92ce600708fa406a3b629bd19ce13a38ad456a38d6c91aae9f49456 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6c125e4d561ff6e1e13ece3de441284ad2cc7fb3bd7f624b6d331652d8b89766 = $this->env->getExtension("native_profiler");
        $__internal_6c125e4d561ff6e1e13ece3de441284ad2cc7fb3bd7f624b6d331652d8b89766->enter($__internal_6c125e4d561ff6e1e13ece3de441284ad2cc7fb3bd7f624b6d331652d8b89766_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_6c125e4d561ff6e1e13ece3de441284ad2cc7fb3bd7f624b6d331652d8b89766->leave($__internal_6c125e4d561ff6e1e13ece3de441284ad2cc7fb3bd7f624b6d331652d8b89766_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
