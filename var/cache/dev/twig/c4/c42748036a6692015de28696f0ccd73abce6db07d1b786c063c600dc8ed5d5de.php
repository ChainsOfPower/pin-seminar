<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_9d00e1621050919e5bfdeb57eb5bd2db5586075f1ca9f326acf3578cc06fdacb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 2);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2434d004c44cc041f16922aa39598602536c282e0db13befeeeee66570ea086a = $this->env->getExtension("native_profiler");
        $__internal_2434d004c44cc041f16922aa39598602536c282e0db13befeeeee66570ea086a->enter($__internal_2434d004c44cc041f16922aa39598602536c282e0db13befeeeee66570ea086a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2434d004c44cc041f16922aa39598602536c282e0db13befeeeee66570ea086a->leave($__internal_2434d004c44cc041f16922aa39598602536c282e0db13befeeeee66570ea086a_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_37f9d8659068394af4e88370e05a0f55e41077c0222c49286333747a101b09bf = $this->env->getExtension("native_profiler");
        $__internal_37f9d8659068394af4e88370e05a0f55e41077c0222c49286333747a101b09bf->enter($__internal_37f9d8659068394af4e88370e05a0f55e41077c0222c49286333747a101b09bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_37f9d8659068394af4e88370e05a0f55e41077c0222c49286333747a101b09bf->leave($__internal_37f9d8659068394af4e88370e05a0f55e41077c0222c49286333747a101b09bf_prof);

    }

    // line 9
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_edb1e589bdd51c0aaff385721d4da97bfe518fe768ba876f7ccf03da0da26494 = $this->env->getExtension("native_profiler");
        $__internal_edb1e589bdd51c0aaff385721d4da97bfe518fe768ba876f7ccf03da0da26494->enter($__internal_edb1e589bdd51c0aaff385721d4da97bfe518fe768ba876f7ccf03da0da26494_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 10
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li class=\"active\"><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_edb1e589bdd51c0aaff385721d4da97bfe518fe768ba876f7ccf03da0da26494->leave($__internal_edb1e589bdd51c0aaff385721d4da97bfe518fe768ba876f7ccf03da0da26494_prof);

    }

    // line 14
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6de8530d0ea125a0a85261817221de24a0da8ae87b1b7eee2f68bbf9c7d5aa16 = $this->env->getExtension("native_profiler");
        $__internal_6de8530d0ea125a0a85261817221de24a0da8ae87b1b7eee2f68bbf9c7d5aa16->enter($__internal_6de8530d0ea125a0a85261817221de24a0da8ae87b1b7eee2f68bbf9c7d5aa16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 15
        echo "    <h2><center>Registriraj se</center></h2>
";
        // line 16
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 16)->display($context);
        
        $__internal_6de8530d0ea125a0a85261817221de24a0da8ae87b1b7eee2f68bbf9c7d5aa16->leave($__internal_6de8530d0ea125a0a85261817221de24a0da8ae87b1b7eee2f68bbf9c7d5aa16_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 16,  77 => 15,  71 => 14,  62 => 10,  56 => 9,  47 => 6,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li class="active"><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <h2><center>Registriraj se</center></h2>*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
