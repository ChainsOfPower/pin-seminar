<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_3511381e641bddb6f46d8c994e55fe50a258b2f3532461eff10b655071e4a3ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58389d86a1fe1f2ac4e1f5d6c85a321317c31c212ba076ab964e276c5b8614a0 = $this->env->getExtension("native_profiler");
        $__internal_58389d86a1fe1f2ac4e1f5d6c85a321317c31c212ba076ab964e276c5b8614a0->enter($__internal_58389d86a1fe1f2ac4e1f5d6c85a321317c31c212ba076ab964e276c5b8614a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_58389d86a1fe1f2ac4e1f5d6c85a321317c31c212ba076ab964e276c5b8614a0->leave($__internal_58389d86a1fe1f2ac4e1f5d6c85a321317c31c212ba076ab964e276c5b8614a0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
