<?php

/* FOSUserBundle:ChangePassword:changePassword.html.twig */
class __TwigTemplate_8d48f9bc4c6ea29ec33af357cc2ff2303f169d5a1f380ecc5c9fc2d6e050310c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_07ce91fab8ed88fef9b6ccca415f027fcbe068b8817e06457ece42071c39d22a = $this->env->getExtension("native_profiler");
        $__internal_07ce91fab8ed88fef9b6ccca415f027fcbe068b8817e06457ece42071c39d22a->enter($__internal_07ce91fab8ed88fef9b6ccca415f027fcbe068b8817e06457ece42071c39d22a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changePassword.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_07ce91fab8ed88fef9b6ccca415f027fcbe068b8817e06457ece42071c39d22a->leave($__internal_07ce91fab8ed88fef9b6ccca415f027fcbe068b8817e06457ece42071c39d22a_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_dabca4e2956a83dee1b436d8573ff3d3c4647b0efc83c6a5fae8e683edd39701 = $this->env->getExtension("native_profiler");
        $__internal_dabca4e2956a83dee1b436d8573ff3d3c4647b0efc83c6a5fae8e683edd39701->enter($__internal_dabca4e2956a83dee1b436d8573ff3d3c4647b0efc83c6a5fae8e683edd39701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_dabca4e2956a83dee1b436d8573ff3d3c4647b0efc83c6a5fae8e683edd39701->leave($__internal_dabca4e2956a83dee1b436d8573ff3d3c4647b0efc83c6a5fae8e683edd39701_prof);

    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        $__internal_9b128ca3d2dab8c3ddf776a6674b3fdcf709a2a476e7c5b91f2a3692872e71e7 = $this->env->getExtension("native_profiler");
        $__internal_9b128ca3d2dab8c3ddf776a6674b3fdcf709a2a476e7c5b91f2a3692872e71e7->enter($__internal_9b128ca3d2dab8c3ddf776a6674b3fdcf709a2a476e7c5b91f2a3692872e71e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 9
        echo "    Promijeni šifru
";
        
        $__internal_9b128ca3d2dab8c3ddf776a6674b3fdcf709a2a476e7c5b91f2a3692872e71e7->leave($__internal_9b128ca3d2dab8c3ddf776a6674b3fdcf709a2a476e7c5b91f2a3692872e71e7_prof);

    }

    // line 13
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_9335644e448f67cf695cbb2608d5d272eca6b851090bcd210a3f0ef1368c2427 = $this->env->getExtension("native_profiler");
        $__internal_9335644e448f67cf695cbb2608d5d272eca6b851090bcd210a3f0ef1368c2427->enter($__internal_9335644e448f67cf695cbb2608d5d272eca6b851090bcd210a3f0ef1368c2427_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 14
        echo "        ";
        if ($this->env->getExtension('security')->isGranted("ROLE_STUDENT")) {
            // line 15
            echo "            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("student_homepage");
            echo "\">Upisni list</a></li>
            <li class=\"active\"><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
            <li><a href=\"/logout\">Odjavi se</a></li>
        ";
        }
        // line 19
        echo "        ";
        if ($this->env->getExtension('security')->isGranted("ROLE_MENTOR")) {
            // line 20
            echo "            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("mentor_studenti");
            echo "\">Studenti</a></li>
            <li><a href=\"";
            // line 21
            echo $this->env->getExtension('routing')->getPath("subject_index");
            echo "\">Predmeti</a></li>
            <li class=\"active\"><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
            <li><a href=\"/logout\">Odjavi se</a></li>
        ";
        }
        // line 25
        echo "    ";
        
        $__internal_9335644e448f67cf695cbb2608d5d272eca6b851090bcd210a3f0ef1368c2427->leave($__internal_9335644e448f67cf695cbb2608d5d272eca6b851090bcd210a3f0ef1368c2427_prof);

    }

    // line 29
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_847d5cbe40a8d2294f8604b3bf01bfad3a9b5d7c2e42bb6ec86a56526b9f8b73 = $this->env->getExtension("native_profiler");
        $__internal_847d5cbe40a8d2294f8604b3bf01bfad3a9b5d7c2e42bb6ec86a56526b9f8b73->enter($__internal_847d5cbe40a8d2294f8604b3bf01bfad3a9b5d7c2e42bb6ec86a56526b9f8b73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 30
        echo "    <h2>
        <center>Promijeni šifru</center>
    </h2>
    ";
        // line 33
        $this->loadTemplate("FOSUserBundle:ChangePassword:changePassword_content.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 33)->display($context);
        
        $__internal_847d5cbe40a8d2294f8604b3bf01bfad3a9b5d7c2e42bb6ec86a56526b9f8b73->leave($__internal_847d5cbe40a8d2294f8604b3bf01bfad3a9b5d7c2e42bb6ec86a56526b9f8b73_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 33,  116 => 30,  110 => 29,  103 => 25,  96 => 21,  91 => 20,  88 => 19,  80 => 15,  77 => 14,  71 => 13,  63 => 9,  57 => 8,  48 => 5,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     Promijeni šifru*/
/* {% endblock %}*/
/* */
/* */
/*     {% block navBarLinks %}*/
/*         {% if is_granted('ROLE_STUDENT') %}*/
/*             <li><a href="{{ path('student_homepage') }}">Upisni list</a></li>*/
/*             <li class="active"><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*             <li><a href="/logout">Odjavi se</a></li>*/
/*         {% endif %}*/
/*         {% if is_granted('ROLE_MENTOR') %}*/
/*             <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*             <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*             <li class="active"><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*             <li><a href="/logout">Odjavi se</a></li>*/
/*         {% endif %}*/
/*     {% endblock %}*/
/* */
/* */
/* */
/* {% block fos_user_content %}*/
/*     <h2>*/
/*         <center>Promijeni šifru</center>*/
/*     </h2>*/
/*     {% include "FOSUserBundle:ChangePassword:changePassword_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
