<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_37c864114e3334ada4bb0ac98c9eda17bf97314cb8027ceaba03b7254b71e23d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_24da047308b79a79440c43da2256cfc421827190da47c8fcadd7bb1bc5ee53ac = $this->env->getExtension("native_profiler");
        $__internal_24da047308b79a79440c43da2256cfc421827190da47c8fcadd7bb1bc5ee53ac->enter($__internal_24da047308b79a79440c43da2256cfc421827190da47c8fcadd7bb1bc5ee53ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_24da047308b79a79440c43da2256cfc421827190da47c8fcadd7bb1bc5ee53ac->leave($__internal_24da047308b79a79440c43da2256cfc421827190da47c8fcadd7bb1bc5ee53ac_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
