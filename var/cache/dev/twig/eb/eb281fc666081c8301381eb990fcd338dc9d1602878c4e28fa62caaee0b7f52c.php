<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_1c77423a98553b3cec2c5982bacf55b694b2c10cf67ca8303f53967cc568c0cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf6035b554a21ba3bc46ecbc494981e6b9a13fe35b5675bfd2dffd09c9fde4ca = $this->env->getExtension("native_profiler");
        $__internal_bf6035b554a21ba3bc46ecbc494981e6b9a13fe35b5675bfd2dffd09c9fde4ca->enter($__internal_bf6035b554a21ba3bc46ecbc494981e6b9a13fe35b5675bfd2dffd09c9fde4ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bf6035b554a21ba3bc46ecbc494981e6b9a13fe35b5675bfd2dffd09c9fde4ca->leave($__internal_bf6035b554a21ba3bc46ecbc494981e6b9a13fe35b5675bfd2dffd09c9fde4ca_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c495d1a828d551758ef0c62b513e5b118ab1d96adb9d2f7445e833aa4e051bff = $this->env->getExtension("native_profiler");
        $__internal_c495d1a828d551758ef0c62b513e5b118ab1d96adb9d2f7445e833aa4e051bff->enter($__internal_c495d1a828d551758ef0c62b513e5b118ab1d96adb9d2f7445e833aa4e051bff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_c495d1a828d551758ef0c62b513e5b118ab1d96adb9d2f7445e833aa4e051bff->leave($__internal_c495d1a828d551758ef0c62b513e5b118ab1d96adb9d2f7445e833aa4e051bff_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_3fc3472f2b90341584e371bef01d8eac8c87670053557dfffd8b4be139acff40 = $this->env->getExtension("native_profiler");
        $__internal_3fc3472f2b90341584e371bef01d8eac8c87670053557dfffd8b4be139acff40->enter($__internal_3fc3472f2b90341584e371bef01d8eac8c87670053557dfffd8b4be139acff40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_3fc3472f2b90341584e371bef01d8eac8c87670053557dfffd8b4be139acff40->leave($__internal_3fc3472f2b90341584e371bef01d8eac8c87670053557dfffd8b4be139acff40_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_d6177cf6e3ae915a455c0fe4e00be0a9b02979b71b254f0f251b832e994cf630 = $this->env->getExtension("native_profiler");
        $__internal_d6177cf6e3ae915a455c0fe4e00be0a9b02979b71b254f0f251b832e994cf630->enter($__internal_d6177cf6e3ae915a455c0fe4e00be0a9b02979b71b254f0f251b832e994cf630_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_d6177cf6e3ae915a455c0fe4e00be0a9b02979b71b254f0f251b832e994cf630->leave($__internal_d6177cf6e3ae915a455c0fe4e00be0a9b02979b71b254f0f251b832e994cf630_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
