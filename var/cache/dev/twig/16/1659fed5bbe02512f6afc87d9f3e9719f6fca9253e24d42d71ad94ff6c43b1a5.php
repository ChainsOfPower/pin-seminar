<?php

/* Subject/show.html.twig */
class __TwigTemplate_6e7cde861565f7657ef56beaf92ad0697c8a87c2eee8668686f866912a445703 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Subject/layout.html.twig", "Subject/show.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Subject/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_909ec58bf03a28963d5773946d9b1a14bab6045b1987021bea691f2d28edd9b5 = $this->env->getExtension("native_profiler");
        $__internal_909ec58bf03a28963d5773946d9b1a14bab6045b1987021bea691f2d28edd9b5->enter($__internal_909ec58bf03a28963d5773946d9b1a14bab6045b1987021bea691f2d28edd9b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Subject/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_909ec58bf03a28963d5773946d9b1a14bab6045b1987021bea691f2d28edd9b5->leave($__internal_909ec58bf03a28963d5773946d9b1a14bab6045b1987021bea691f2d28edd9b5_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3aa3e20633d57d32f898aea92a1edee2077a1089cb9c731068477e749f882ec5 = $this->env->getExtension("native_profiler");
        $__internal_3aa3e20633d57d32f898aea92a1edee2077a1089cb9c731068477e749f882ec5->enter($__internal_3aa3e20633d57d32f898aea92a1edee2077a1089cb9c731068477e749f882ec5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/table_student.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_3aa3e20633d57d32f898aea92a1edee2077a1089cb9c731068477e749f882ec5->leave($__internal_3aa3e20633d57d32f898aea92a1edee2077a1089cb9c731068477e749f882ec5_prof);

    }

    // line 7
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_1298f22cae5cbb49a032e3e973eed5cd03923fb64b9b4fb9d8cf9e3448a2c8ed = $this->env->getExtension("native_profiler");
        $__internal_1298f22cae5cbb49a032e3e973eed5cd03923fb64b9b4fb9d8cf9e3448a2c8ed->enter($__internal_1298f22cae5cbb49a032e3e973eed5cd03923fb64b9b4fb9d8cf9e3448a2c8ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 8
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_1298f22cae5cbb49a032e3e973eed5cd03923fb64b9b4fb9d8cf9e3448a2c8ed->leave($__internal_1298f22cae5cbb49a032e3e973eed5cd03923fb64b9b4fb9d8cf9e3448a2c8ed_prof);

    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        $__internal_a1997e5656ec32035b7aacf80de966a2c67a6aff738be4a82f70ce434dd33f4f = $this->env->getExtension("native_profiler");
        $__internal_a1997e5656ec32035b7aacf80de966a2c67a6aff738be4a82f70ce434dd33f4f->enter($__internal_a1997e5656ec32035b7aacf80de966a2c67a6aff738be4a82f70ce434dd33f4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 15
        echo "
    <h2>
        <center>Predmet</center>
    </h2>
    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\">
                <table class=\"table\">
                    <tbody>
                    <tr>
                        <th>Id</th>
                        <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subject"]) ? $context["subject"] : $this->getContext($context, "subject")), "id", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th>Ime</th>
                        <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subject"]) ? $context["subject"] : $this->getContext($context, "subject")), "ime", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th>Kod</th>
                        <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subject"]) ? $context["subject"] : $this->getContext($context, "subject")), "kod", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th>Program</th>
                        <td>";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subject"]) ? $context["subject"] : $this->getContext($context, "subject")), "program", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th>Bodovi</th>
                        <td>";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subject"]) ? $context["subject"] : $this->getContext($context, "subject")), "bodovi", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th>Semestar redovni</th>
                        <td>";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subject"]) ? $context["subject"] : $this->getContext($context, "subject")), "semRedovni", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th>Semestar izvanredni</th>
                        <td>";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subject"]) ? $context["subject"] : $this->getContext($context, "subject")), "semIzvanredni", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th>Izborni</th>
                        <td>";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subject"]) ? $context["subject"] : $this->getContext($context, "subject")), "izborni", array()), "html", null, true);
        echo "</td>
                    </tr>
                    </tbody>
                </table>


                ";
        // line 60
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input class=\"btn btn-primary btn-block\" type=\"submit\" value=\"Obriši\">
                ";
        // line 62
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_a1997e5656ec32035b7aacf80de966a2c67a6aff738be4a82f70ce434dd33f4f->leave($__internal_a1997e5656ec32035b7aacf80de966a2c67a6aff738be4a82f70ce434dd33f4f_prof);

    }

    public function getTemplateName()
    {
        return "Subject/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 62,  155 => 60,  146 => 54,  139 => 50,  132 => 46,  125 => 42,  118 => 38,  111 => 34,  104 => 30,  97 => 26,  84 => 15,  78 => 14,  67 => 9,  62 => 8,  56 => 7,  47 => 4,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'Subject/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/table_student.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <h2>*/
/*         <center>Predmet</center>*/
/*     </h2>*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3">*/
/*                 <table class="table">*/
/*                     <tbody>*/
/*                     <tr>*/
/*                         <th>Id</th>*/
/*                         <td>{{ subject.id }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th>Ime</th>*/
/*                         <td>{{ subject.ime }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th>Kod</th>*/
/*                         <td>{{ subject.kod }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th>Program</th>*/
/*                         <td>{{ subject.program }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th>Bodovi</th>*/
/*                         <td>{{ subject.bodovi }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th>Semestar redovni</th>*/
/*                         <td>{{ subject.semRedovni }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th>Semestar izvanredni</th>*/
/*                         <td>{{ subject.semIzvanredni }}</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <th>Izborni</th>*/
/*                         <td>{{ subject.izborni }}</td>*/
/*                     </tr>*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/* */
/*                 {{ form_start(delete_form) }}*/
/*                 <input class="btn btn-primary btn-block" type="submit" value="Obriši">*/
/*                 {{ form_end(delete_form) }}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* */
/* */
