<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_82ab471af202b52c72c0689ca30cffdb4bd78276cd8459b0f6becf422de36be4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_035e75f61e83c8c6ee714d412634fffdd0e579813be8e518827d0a091e90cdfb = $this->env->getExtension("native_profiler");
        $__internal_035e75f61e83c8c6ee714d412634fffdd0e579813be8e518827d0a091e90cdfb->enter($__internal_035e75f61e83c8c6ee714d412634fffdd0e579813be8e518827d0a091e90cdfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_035e75f61e83c8c6ee714d412634fffdd0e579813be8e518827d0a091e90cdfb->leave($__internal_035e75f61e83c8c6ee714d412634fffdd0e579813be8e518827d0a091e90cdfb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
