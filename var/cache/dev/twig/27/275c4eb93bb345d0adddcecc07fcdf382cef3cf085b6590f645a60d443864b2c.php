<?php

/* @FOSUser/Resetting/passwordAlreadyRequested.html.twig */
class __TwigTemplate_0ff53663db5a509b677b069a450fd633196af35563b2dd5b448cc425fd1f6bd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Resetting/passwordAlreadyRequested.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a1519dcca4749b2aea9df45b1367b2b90b781782a7f26fd09957aa0b5e60272 = $this->env->getExtension("native_profiler");
        $__internal_1a1519dcca4749b2aea9df45b1367b2b90b781782a7f26fd09957aa0b5e60272->enter($__internal_1a1519dcca4749b2aea9df45b1367b2b90b781782a7f26fd09957aa0b5e60272_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/passwordAlreadyRequested.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a1519dcca4749b2aea9df45b1367b2b90b781782a7f26fd09957aa0b5e60272->leave($__internal_1a1519dcca4749b2aea9df45b1367b2b90b781782a7f26fd09957aa0b5e60272_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_1e64e91b7b7781c93f66c71953dbd9306fd6e8d3c6766b6b084277108f44a8ec = $this->env->getExtension("native_profiler");
        $__internal_1e64e91b7b7781c93f66c71953dbd9306fd6e8d3c6766b6b084277108f44a8ec->enter($__internal_1e64e91b7b7781c93f66c71953dbd9306fd6e8d3c6766b6b084277108f44a8ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_1e64e91b7b7781c93f66c71953dbd9306fd6e8d3c6766b6b084277108f44a8ec->leave($__internal_1e64e91b7b7781c93f66c71953dbd9306fd6e8d3c6766b6b084277108f44a8ec_prof);

    }

    // line 7
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_c9e8441046def92330ed04d9d7a7b3dc57042f800c13f605d63a1901d0bae6d3 = $this->env->getExtension("native_profiler");
        $__internal_c9e8441046def92330ed04d9d7a7b3dc57042f800c13f605d63a1901d0bae6d3->enter($__internal_c9e8441046def92330ed04d9d7a7b3dc57042f800c13f605d63a1901d0bae6d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 8
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_c9e8441046def92330ed04d9d7a7b3dc57042f800c13f605d63a1901d0bae6d3->leave($__internal_c9e8441046def92330ed04d9d7a7b3dc57042f800c13f605d63a1901d0bae6d3_prof);

    }

    // line 14
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2d3b7dfca22afcd1be49fb4e3bd69c2d126d7cc6d5425d15b4ee0d743d8c05e6 = $this->env->getExtension("native_profiler");
        $__internal_2d3b7dfca22afcd1be49fb4e3bd69c2d126d7cc6d5425d15b4ee0d743d8c05e6->enter($__internal_2d3b7dfca22afcd1be49fb4e3bd69c2d126d7cc6d5425d15b4ee0d743d8c05e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 15
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\" style=\"background-color: rgba(255,255,255,0.8)\">
                <p>";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.password_already_requested", array(), "FOSUserBundle"), "html", null, true);
        echo "</p>
            </div>
        </div>
    </div>
";
        
        $__internal_2d3b7dfca22afcd1be49fb4e3bd69c2d126d7cc6d5425d15b4ee0d743d8c05e6->leave($__internal_2d3b7dfca22afcd1be49fb4e3bd69c2d126d7cc6d5425d15b4ee0d743d8c05e6_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/passwordAlreadyRequested.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 18,  73 => 15,  67 => 14,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{  parent() }}*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3" style="background-color: rgba(255,255,255,0.8)">*/
/*                 <p>{{ 'resetting.password_already_requested'|trans }}</p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
/* */
/* */
/* */
/* */
/* */
/* */
