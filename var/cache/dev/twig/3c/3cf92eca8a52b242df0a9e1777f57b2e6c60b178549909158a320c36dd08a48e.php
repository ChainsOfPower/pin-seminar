<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_9e4c922fcdf4ee7076db36cee93edb84bbfdef912842c5cacf1f15de0cdc9c1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab26923cd19d3db103deb82ebab5562152bfa9783ffdb5f6577af46e33d0cbb7 = $this->env->getExtension("native_profiler");
        $__internal_ab26923cd19d3db103deb82ebab5562152bfa9783ffdb5f6577af46e33d0cbb7->enter($__internal_ab26923cd19d3db103deb82ebab5562152bfa9783ffdb5f6577af46e33d0cbb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_ab26923cd19d3db103deb82ebab5562152bfa9783ffdb5f6577af46e33d0cbb7->leave($__internal_ab26923cd19d3db103deb82ebab5562152bfa9783ffdb5f6577af46e33d0cbb7_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_88a1bc6db3f5256b0f29ef34419d6d686969aeab0f4e66a393e7bebf8b8c7c55 = $this->env->getExtension("native_profiler");
        $__internal_88a1bc6db3f5256b0f29ef34419d6d686969aeab0f4e66a393e7bebf8b8c7c55->enter($__internal_88a1bc6db3f5256b0f29ef34419d6d686969aeab0f4e66a393e7bebf8b8c7c55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_88a1bc6db3f5256b0f29ef34419d6d686969aeab0f4e66a393e7bebf8b8c7c55->leave($__internal_88a1bc6db3f5256b0f29ef34419d6d686969aeab0f4e66a393e7bebf8b8c7c55_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_b292718a3bb7866a82079f22a5239ee6166f1a61d047fad1e370d3fe2af02b85 = $this->env->getExtension("native_profiler");
        $__internal_b292718a3bb7866a82079f22a5239ee6166f1a61d047fad1e370d3fe2af02b85->enter($__internal_b292718a3bb7866a82079f22a5239ee6166f1a61d047fad1e370d3fe2af02b85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_b292718a3bb7866a82079f22a5239ee6166f1a61d047fad1e370d3fe2af02b85->leave($__internal_b292718a3bb7866a82079f22a5239ee6166f1a61d047fad1e370d3fe2af02b85_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_35d9dae573a602c689b09de614a425d9ae3708fa4542f30ed00aa237478639fe = $this->env->getExtension("native_profiler");
        $__internal_35d9dae573a602c689b09de614a425d9ae3708fa4542f30ed00aa237478639fe->enter($__internal_35d9dae573a602c689b09de614a425d9ae3708fa4542f30ed00aa237478639fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_35d9dae573a602c689b09de614a425d9ae3708fa4542f30ed00aa237478639fe->leave($__internal_35d9dae573a602c689b09de614a425d9ae3708fa4542f30ed00aa237478639fe_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
