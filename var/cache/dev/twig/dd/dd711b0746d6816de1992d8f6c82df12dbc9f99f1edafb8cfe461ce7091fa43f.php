<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_533f09394272e739900916197d0d466824b03e52aa77ea351a65648bfe1e445a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aec198b7a7726c5dcae346b90afeb5649fe3f81295dfd708bc6ebb702e28e46d = $this->env->getExtension("native_profiler");
        $__internal_aec198b7a7726c5dcae346b90afeb5649fe3f81295dfd708bc6ebb702e28e46d->enter($__internal_aec198b7a7726c5dcae346b90afeb5649fe3f81295dfd708bc6ebb702e28e46d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_aec198b7a7726c5dcae346b90afeb5649fe3f81295dfd708bc6ebb702e28e46d->leave($__internal_aec198b7a7726c5dcae346b90afeb5649fe3f81295dfd708bc6ebb702e28e46d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
