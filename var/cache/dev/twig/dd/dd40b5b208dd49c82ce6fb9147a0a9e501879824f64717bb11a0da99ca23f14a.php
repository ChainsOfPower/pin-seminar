<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_6cd8c9921b0a61ffc5ee2c0a165fde35d6766eef3d00ba0e4670b3e6739c95d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Registration/register.html.twig", 2);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8eba25f4df924e2b6e6b197040d13ecb19addb20d053d0f0720e295e6ac0de2f = $this->env->getExtension("native_profiler");
        $__internal_8eba25f4df924e2b6e6b197040d13ecb19addb20d053d0f0720e295e6ac0de2f->enter($__internal_8eba25f4df924e2b6e6b197040d13ecb19addb20d053d0f0720e295e6ac0de2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8eba25f4df924e2b6e6b197040d13ecb19addb20d053d0f0720e295e6ac0de2f->leave($__internal_8eba25f4df924e2b6e6b197040d13ecb19addb20d053d0f0720e295e6ac0de2f_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0da1450f1d256bf2579b6d648100d818ad4fbf458457856b5e800034cb34668a = $this->env->getExtension("native_profiler");
        $__internal_0da1450f1d256bf2579b6d648100d818ad4fbf458457856b5e800034cb34668a->enter($__internal_0da1450f1d256bf2579b6d648100d818ad4fbf458457856b5e800034cb34668a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_0da1450f1d256bf2579b6d648100d818ad4fbf458457856b5e800034cb34668a->leave($__internal_0da1450f1d256bf2579b6d648100d818ad4fbf458457856b5e800034cb34668a_prof);

    }

    // line 9
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_93e5ae16d6169af41d7855be5b69aeeb90060422aa367341f0c4a9d13aa68d27 = $this->env->getExtension("native_profiler");
        $__internal_93e5ae16d6169af41d7855be5b69aeeb90060422aa367341f0c4a9d13aa68d27->enter($__internal_93e5ae16d6169af41d7855be5b69aeeb90060422aa367341f0c4a9d13aa68d27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 10
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li class=\"active\"><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_93e5ae16d6169af41d7855be5b69aeeb90060422aa367341f0c4a9d13aa68d27->leave($__internal_93e5ae16d6169af41d7855be5b69aeeb90060422aa367341f0c4a9d13aa68d27_prof);

    }

    // line 14
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9fa0d458dcaf19d33edd9b08b9da01c71f58fcd48a4aedf5ab1732f0100c0b9e = $this->env->getExtension("native_profiler");
        $__internal_9fa0d458dcaf19d33edd9b08b9da01c71f58fcd48a4aedf5ab1732f0100c0b9e->enter($__internal_9fa0d458dcaf19d33edd9b08b9da01c71f58fcd48a4aedf5ab1732f0100c0b9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 15
        echo "    <h2><center>Registriraj se</center></h2>
";
        // line 16
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "@FOSUser/Registration/register.html.twig", 16)->display($context);
        
        $__internal_9fa0d458dcaf19d33edd9b08b9da01c71f58fcd48a4aedf5ab1732f0100c0b9e->leave($__internal_9fa0d458dcaf19d33edd9b08b9da01c71f58fcd48a4aedf5ab1732f0100c0b9e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 16,  77 => 15,  71 => 14,  62 => 10,  56 => 9,  47 => 6,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* */
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li class="active"><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <h2><center>Registriraj se</center></h2>*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
