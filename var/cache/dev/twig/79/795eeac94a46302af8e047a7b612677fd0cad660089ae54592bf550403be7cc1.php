<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_452ef5a336a0330f3b1e7ce9c5ba12c1890279d5340c038003692f8464f432a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Profile/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4456159832d78d0b6eef9d773c0a0805d32a1d1c8860dde08631cf966af1ce59 = $this->env->getExtension("native_profiler");
        $__internal_4456159832d78d0b6eef9d773c0a0805d32a1d1c8860dde08631cf966af1ce59->enter($__internal_4456159832d78d0b6eef9d773c0a0805d32a1d1c8860dde08631cf966af1ce59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4456159832d78d0b6eef9d773c0a0805d32a1d1c8860dde08631cf966af1ce59->leave($__internal_4456159832d78d0b6eef9d773c0a0805d32a1d1c8860dde08631cf966af1ce59_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fc2857f1f32d26a6d0517ae8ad919a6388fbd3b84c7d59990b341cc1dd69afca = $this->env->getExtension("native_profiler");
        $__internal_fc2857f1f32d26a6d0517ae8ad919a6388fbd3b84c7d59990b341cc1dd69afca->enter($__internal_fc2857f1f32d26a6d0517ae8ad919a6388fbd3b84c7d59990b341cc1dd69afca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "@FOSUser/Profile/show.html.twig", 4)->display($context);
        
        $__internal_fc2857f1f32d26a6d0517ae8ad919a6388fbd3b84c7d59990b341cc1dd69afca->leave($__internal_fc2857f1f32d26a6d0517ae8ad919a6388fbd3b84c7d59990b341cc1dd69afca_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
