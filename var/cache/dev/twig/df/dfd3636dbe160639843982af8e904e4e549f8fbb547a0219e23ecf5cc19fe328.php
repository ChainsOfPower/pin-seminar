<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_fdef4507121b0caaf6b5ed7f37a791fdee5a789277b0db0355a6d0860eef0e79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f17553109a74c808563cb6d35d87e406f37aeb14d7c8dc7b32b2dd3150c9953e = $this->env->getExtension("native_profiler");
        $__internal_f17553109a74c808563cb6d35d87e406f37aeb14d7c8dc7b32b2dd3150c9953e->enter($__internal_f17553109a74c808563cb6d35d87e406f37aeb14d7c8dc7b32b2dd3150c9953e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_f17553109a74c808563cb6d35d87e406f37aeb14d7c8dc7b32b2dd3150c9953e->leave($__internal_f17553109a74c808563cb6d35d87e406f37aeb14d7c8dc7b32b2dd3150c9953e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
