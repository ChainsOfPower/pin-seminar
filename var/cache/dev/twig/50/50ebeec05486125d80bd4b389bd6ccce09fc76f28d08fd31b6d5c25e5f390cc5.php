<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_3f1aa40913d38dcff1f3b8ef7f617fe2ac37a3eb7a840c4db1150c16dbac0a28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_deb76e16dd4f675cbb12757ff1dbcc40e14c5f4c261351e840da3d0d801042fc = $this->env->getExtension("native_profiler");
        $__internal_deb76e16dd4f675cbb12757ff1dbcc40e14c5f4c261351e840da3d0d801042fc->enter($__internal_deb76e16dd4f675cbb12757ff1dbcc40e14c5f4c261351e840da3d0d801042fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_deb76e16dd4f675cbb12757ff1dbcc40e14c5f4c261351e840da3d0d801042fc->leave($__internal_deb76e16dd4f675cbb12757ff1dbcc40e14c5f4c261351e840da3d0d801042fc_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_05e0da70dd7ed6f65257b5a1b49fcc7c785ab994f9b40857daa1307c5f9edf67 = $this->env->getExtension("native_profiler");
        $__internal_05e0da70dd7ed6f65257b5a1b49fcc7c785ab994f9b40857daa1307c5f9edf67->enter($__internal_05e0da70dd7ed6f65257b5a1b49fcc7c785ab994f9b40857daa1307c5f9edf67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_05e0da70dd7ed6f65257b5a1b49fcc7c785ab994f9b40857daa1307c5f9edf67->leave($__internal_05e0da70dd7ed6f65257b5a1b49fcc7c785ab994f9b40857daa1307c5f9edf67_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
