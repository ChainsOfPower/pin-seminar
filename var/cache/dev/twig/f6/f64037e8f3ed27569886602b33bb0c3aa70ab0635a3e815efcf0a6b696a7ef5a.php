<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_4b6faa18e91a53e8a6ee3e070e800e41c3750a0bbc17490d6e04180bed572a67 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d36db90c125c54695c81c6a578b536dbcf4f9dc0bb0c5f43794ec2588f8fdbe = $this->env->getExtension("native_profiler");
        $__internal_8d36db90c125c54695c81c6a578b536dbcf4f9dc0bb0c5f43794ec2588f8fdbe->enter($__internal_8d36db90c125c54695c81c6a578b536dbcf4f9dc0bb0c5f43794ec2588f8fdbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8d36db90c125c54695c81c6a578b536dbcf4f9dc0bb0c5f43794ec2588f8fdbe->leave($__internal_8d36db90c125c54695c81c6a578b536dbcf4f9dc0bb0c5f43794ec2588f8fdbe_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_669cae6f8c3b7f801d48a67ab766eddb6d1d47c5b5c8ea177e7369cf3646086b = $this->env->getExtension("native_profiler");
        $__internal_669cae6f8c3b7f801d48a67ab766eddb6d1d47c5b5c8ea177e7369cf3646086b->enter($__internal_669cae6f8c3b7f801d48a67ab766eddb6d1d47c5b5c8ea177e7369cf3646086b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_669cae6f8c3b7f801d48a67ab766eddb6d1d47c5b5c8ea177e7369cf3646086b->leave($__internal_669cae6f8c3b7f801d48a67ab766eddb6d1d47c5b5c8ea177e7369cf3646086b_prof);

    }

    // line 8
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_076167ae8a5fbabddb5b991dead35dcae3ae216d863b6c8ac0f56c04051b91e3 = $this->env->getExtension("native_profiler");
        $__internal_076167ae8a5fbabddb5b991dead35dcae3ae216d863b6c8ac0f56c04051b91e3->enter($__internal_076167ae8a5fbabddb5b991dead35dcae3ae216d863b6c8ac0f56c04051b91e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 9
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_076167ae8a5fbabddb5b991dead35dcae3ae216d863b6c8ac0f56c04051b91e3->leave($__internal_076167ae8a5fbabddb5b991dead35dcae3ae216d863b6c8ac0f56c04051b91e3_prof);

    }

    // line 13
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5c8ce7325330ce51aac1f1726cda94ee57e95f93749915076ca6f2fa28b8c270 = $this->env->getExtension("native_profiler");
        $__internal_5c8ce7325330ce51aac1f1726cda94ee57e95f93749915076ca6f2fa28b8c270->enter($__internal_5c8ce7325330ce51aac1f1726cda94ee57e95f93749915076ca6f2fa28b8c270_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 14
        echo "    <h2><center>Resetiraj šifru</center></h2>
";
        // line 15
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 15)->display($context);
        
        $__internal_5c8ce7325330ce51aac1f1726cda94ee57e95f93749915076ca6f2fa28b8c270->leave($__internal_5c8ce7325330ce51aac1f1726cda94ee57e95f93749915076ca6f2fa28b8c270_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 15,  77 => 14,  71 => 13,  62 => 9,  56 => 8,  47 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <h2><center>Resetiraj šifru</center></h2>*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
