<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_22ab96b555680567d399bf1f38fee8be8b54e81912d6b6541df4fc04695b30a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d05a60c8e337e37226227500ecfe9f3bd1ed7084763febe988c1f37ad447fc4 = $this->env->getExtension("native_profiler");
        $__internal_3d05a60c8e337e37226227500ecfe9f3bd1ed7084763febe988c1f37ad447fc4->enter($__internal_3d05a60c8e337e37226227500ecfe9f3bd1ed7084763febe988c1f37ad447fc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_3d05a60c8e337e37226227500ecfe9f3bd1ed7084763febe988c1f37ad447fc4->leave($__internal_3d05a60c8e337e37226227500ecfe9f3bd1ed7084763febe988c1f37ad447fc4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
