<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_fbdf2e4edcc8873f318bfac406779dffeb065edb0f9ab7025385366ad0d9efe2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58135c08748a2dc2a1e82219f76eb8259941914fcb42e6e8d8714744f223018d = $this->env->getExtension("native_profiler");
        $__internal_58135c08748a2dc2a1e82219f76eb8259941914fcb42e6e8d8714744f223018d->enter($__internal_58135c08748a2dc2a1e82219f76eb8259941914fcb42e6e8d8714744f223018d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_58135c08748a2dc2a1e82219f76eb8259941914fcb42e6e8d8714744f223018d->leave($__internal_58135c08748a2dc2a1e82219f76eb8259941914fcb42e6e8d8714744f223018d_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_ded75f371b2397a2d806da44b11aaebd63077a8b0889ffa080627b6e735cef50 = $this->env->getExtension("native_profiler");
        $__internal_ded75f371b2397a2d806da44b11aaebd63077a8b0889ffa080627b6e735cef50->enter($__internal_ded75f371b2397a2d806da44b11aaebd63077a8b0889ffa080627b6e735cef50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_ded75f371b2397a2d806da44b11aaebd63077a8b0889ffa080627b6e735cef50->leave($__internal_ded75f371b2397a2d806da44b11aaebd63077a8b0889ffa080627b6e735cef50_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
