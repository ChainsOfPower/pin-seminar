<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_7dec85b8710af447d64d540ba68b945ed13ccbda18b7ad03f60b93800feeb693 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0e4eceb26da94f217672ccea4dac7eea0b6eaea0936c43443900efa1cc3b088 = $this->env->getExtension("native_profiler");
        $__internal_a0e4eceb26da94f217672ccea4dac7eea0b6eaea0936c43443900efa1cc3b088->enter($__internal_a0e4eceb26da94f217672ccea4dac7eea0b6eaea0936c43443900efa1cc3b088_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_a0e4eceb26da94f217672ccea4dac7eea0b6eaea0936c43443900efa1cc3b088->leave($__internal_a0e4eceb26da94f217672ccea4dac7eea0b6eaea0936c43443900efa1cc3b088_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
