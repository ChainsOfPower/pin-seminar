<?php

/* @FOSUser/Registration/checkEmail.html.twig */
class __TwigTemplate_8c559f9a36f7c2b0b7d00de24a9a59c1e7080663c038f27a5922084be86e3f22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Registration/checkEmail.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99a8a7424791c894b7cfd1e28c83cc1c8ff0525c35f82170b1ba407f022ba435 = $this->env->getExtension("native_profiler");
        $__internal_99a8a7424791c894b7cfd1e28c83cc1c8ff0525c35f82170b1ba407f022ba435->enter($__internal_99a8a7424791c894b7cfd1e28c83cc1c8ff0525c35f82170b1ba407f022ba435_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_99a8a7424791c894b7cfd1e28c83cc1c8ff0525c35f82170b1ba407f022ba435->leave($__internal_99a8a7424791c894b7cfd1e28c83cc1c8ff0525c35f82170b1ba407f022ba435_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6a26061583acbf68057f7bf635eb5484fee176cb129f092694e5972fe649f105 = $this->env->getExtension("native_profiler");
        $__internal_6a26061583acbf68057f7bf635eb5484fee176cb129f092694e5972fe649f105->enter($__internal_6a26061583acbf68057f7bf635eb5484fee176cb129f092694e5972fe649f105_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_6a26061583acbf68057f7bf635eb5484fee176cb129f092694e5972fe649f105->leave($__internal_6a26061583acbf68057f7bf635eb5484fee176cb129f092694e5972fe649f105_prof);

    }

    // line 6
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_44d0bd78d757cba96c41177857f651d5dc8e6f3189a5b41a5b70570e65c93f0c = $this->env->getExtension("native_profiler");
        $__internal_44d0bd78d757cba96c41177857f651d5dc8e6f3189a5b41a5b70570e65c93f0c->enter($__internal_44d0bd78d757cba96c41177857f651d5dc8e6f3189a5b41a5b70570e65c93f0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 7
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_44d0bd78d757cba96c41177857f651d5dc8e6f3189a5b41a5b70570e65c93f0c->leave($__internal_44d0bd78d757cba96c41177857f651d5dc8e6f3189a5b41a5b70570e65c93f0c_prof);

    }

    // line 13
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c6f2322340d6bebc73323e6cad0da8f72a4576038bcf23f76aba964e5299659f = $this->env->getExtension("native_profiler");
        $__internal_c6f2322340d6bebc73323e6cad0da8f72a4576038bcf23f76aba964e5299659f->enter($__internal_c6f2322340d6bebc73323e6cad0da8f72a4576038bcf23f76aba964e5299659f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 14
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\" style=\"background-color: rgba(255,255,255,0.8)\">
                <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
            </div>
        </div>
    </div>
";
        
        $__internal_c6f2322340d6bebc73323e6cad0da8f72a4576038bcf23f76aba964e5299659f->leave($__internal_c6f2322340d6bebc73323e6cad0da8f72a4576038bcf23f76aba964e5299659f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 17,  73 => 14,  67 => 13,  58 => 7,  52 => 6,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* {% block stylesheets %}*/
/*     {{  parent() }}*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3" style="background-color: rgba(255,255,255,0.8)">*/
/*                 <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
/* */
