<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_10f532f8622e5b5c2737ea94a23543bd11da7dd07bcdac356e2e33d04c33595d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_177a521ff3c5efc5028017b93bcdc9e1ac20aa85c9555223923794a551193220 = $this->env->getExtension("native_profiler");
        $__internal_177a521ff3c5efc5028017b93bcdc9e1ac20aa85c9555223923794a551193220->enter($__internal_177a521ff3c5efc5028017b93bcdc9e1ac20aa85c9555223923794a551193220_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_177a521ff3c5efc5028017b93bcdc9e1ac20aa85c9555223923794a551193220->leave($__internal_177a521ff3c5efc5028017b93bcdc9e1ac20aa85c9555223923794a551193220_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_110717c947138b3cbecc25620da01a51fd84fb65b8e76a9e17c69e26485cdff0 = $this->env->getExtension("native_profiler");
        $__internal_110717c947138b3cbecc25620da01a51fd84fb65b8e76a9e17c69e26485cdff0->enter($__internal_110717c947138b3cbecc25620da01a51fd84fb65b8e76a9e17c69e26485cdff0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_110717c947138b3cbecc25620da01a51fd84fb65b8e76a9e17c69e26485cdff0->leave($__internal_110717c947138b3cbecc25620da01a51fd84fb65b8e76a9e17c69e26485cdff0_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
