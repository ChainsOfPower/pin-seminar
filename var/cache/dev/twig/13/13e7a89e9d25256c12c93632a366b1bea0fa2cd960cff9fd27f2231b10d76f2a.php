<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_956ed8dde48311a4826751c4c2bc7d3b6940ec1a5a05ba9d55042352dc6ac45c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7a8c3729c8a313f159fbfbd73dfbf528273b25eef63dd06837e7ae706c4bb03 = $this->env->getExtension("native_profiler");
        $__internal_b7a8c3729c8a313f159fbfbd73dfbf528273b25eef63dd06837e7ae706c4bb03->enter($__internal_b7a8c3729c8a313f159fbfbd73dfbf528273b25eef63dd06837e7ae706c4bb03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_b7a8c3729c8a313f159fbfbd73dfbf528273b25eef63dd06837e7ae706c4bb03->leave($__internal_b7a8c3729c8a313f159fbfbd73dfbf528273b25eef63dd06837e7ae706c4bb03_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
