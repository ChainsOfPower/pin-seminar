<?php

/* @FOSUser/ChangePassword/changePassword.html.twig */
class __TwigTemplate_88d67fe5ab7575db731aefe31276d64521ed24d89dea9b14fd75af9c5195fea5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/ChangePassword/changePassword.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_08f78896bc5dcd852a8b43b572c32d70be5e5e58bd21eab31c8a4cee29165fee = $this->env->getExtension("native_profiler");
        $__internal_08f78896bc5dcd852a8b43b572c32d70be5e5e58bd21eab31c8a4cee29165fee->enter($__internal_08f78896bc5dcd852a8b43b572c32d70be5e5e58bd21eab31c8a4cee29165fee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/changePassword.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_08f78896bc5dcd852a8b43b572c32d70be5e5e58bd21eab31c8a4cee29165fee->leave($__internal_08f78896bc5dcd852a8b43b572c32d70be5e5e58bd21eab31c8a4cee29165fee_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_2d782451722280fcfd1a174291a1e15574d3aa31a1b8f47874aead5a3b532d91 = $this->env->getExtension("native_profiler");
        $__internal_2d782451722280fcfd1a174291a1e15574d3aa31a1b8f47874aead5a3b532d91->enter($__internal_2d782451722280fcfd1a174291a1e15574d3aa31a1b8f47874aead5a3b532d91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_2d782451722280fcfd1a174291a1e15574d3aa31a1b8f47874aead5a3b532d91->leave($__internal_2d782451722280fcfd1a174291a1e15574d3aa31a1b8f47874aead5a3b532d91_prof);

    }

    // line 9
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_63faeb1d22070b64f293ac526d98e1eb2466d7cacdfc52d65490f517a34376c0 = $this->env->getExtension("native_profiler");
        $__internal_63faeb1d22070b64f293ac526d98e1eb2466d7cacdfc52d65490f517a34376c0->enter($__internal_63faeb1d22070b64f293ac526d98e1eb2466d7cacdfc52d65490f517a34376c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 10
        echo "        ";
        if ($this->env->getExtension('security')->isGranted("ROLE_STUDENT")) {
            // line 11
            echo "            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("student_homepage");
            echo "\">Upisni list</a></li>
            <li class=\"active\"><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
            <li><a href=\"/logout\">Odjavi se</a></li>
        ";
        }
        // line 15
        echo "        ";
        if ($this->env->getExtension('security')->isGranted("ROLE_MENTOR")) {
            // line 16
            echo "            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("mentor_studenti");
            echo "\">Studenti</a></li>
            <li><a href=\"";
            // line 17
            echo $this->env->getExtension('routing')->getPath("subject_index");
            echo "\">Predmeti</a></li>
            <li class=\"active\"><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
            <li><a href=\"/logout\">Odjavi se</a></li>
        ";
        }
        // line 21
        echo "    ";
        
        $__internal_63faeb1d22070b64f293ac526d98e1eb2466d7cacdfc52d65490f517a34376c0->leave($__internal_63faeb1d22070b64f293ac526d98e1eb2466d7cacdfc52d65490f517a34376c0_prof);

    }

    // line 25
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3a7a6ac9aff81f3b88879acb80d8f3b972c8f01f022d87b515f22e39fcf880d1 = $this->env->getExtension("native_profiler");
        $__internal_3a7a6ac9aff81f3b88879acb80d8f3b972c8f01f022d87b515f22e39fcf880d1->enter($__internal_3a7a6ac9aff81f3b88879acb80d8f3b972c8f01f022d87b515f22e39fcf880d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 26
        echo "    <h2>
        <center>Promijeni šifru</center>
    </h2>
    ";
        // line 29
        $this->loadTemplate("FOSUserBundle:ChangePassword:changePassword_content.html.twig", "@FOSUser/ChangePassword/changePassword.html.twig", 29)->display($context);
        
        $__internal_3a7a6ac9aff81f3b88879acb80d8f3b972c8f01f022d87b515f22e39fcf880d1->leave($__internal_3a7a6ac9aff81f3b88879acb80d8f3b972c8f01f022d87b515f22e39fcf880d1_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 29,  101 => 26,  95 => 25,  88 => 21,  81 => 17,  76 => 16,  73 => 15,  65 => 11,  62 => 10,  56 => 9,  47 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* */
/*     {% block navBarLinks %}*/
/*         {% if is_granted('ROLE_STUDENT') %}*/
/*             <li><a href="{{ path('student_homepage') }}">Upisni list</a></li>*/
/*             <li class="active"><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*             <li><a href="/logout">Odjavi se</a></li>*/
/*         {% endif %}*/
/*         {% if is_granted('ROLE_MENTOR') %}*/
/*             <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*             <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*             <li class="active"><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*             <li><a href="/logout">Odjavi se</a></li>*/
/*         {% endif %}*/
/*     {% endblock %}*/
/* */
/* */
/* */
/* {% block fos_user_content %}*/
/*     <h2>*/
/*         <center>Promijeni šifru</center>*/
/*     </h2>*/
/*     {% include "FOSUserBundle:ChangePassword:changePassword_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
