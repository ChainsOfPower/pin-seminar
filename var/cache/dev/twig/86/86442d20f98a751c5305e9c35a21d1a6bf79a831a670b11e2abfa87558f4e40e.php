<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_e6b6eb6bfb7352e7a85322bd9ac0d13ec1d0ac6cae7b1f86038cf7d6508dd75f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_780dbe5e85bea257028d17d1a35abe524f70d9e64fa6f0350a6b74b99ca0664b = $this->env->getExtension("native_profiler");
        $__internal_780dbe5e85bea257028d17d1a35abe524f70d9e64fa6f0350a6b74b99ca0664b->enter($__internal_780dbe5e85bea257028d17d1a35abe524f70d9e64fa6f0350a6b74b99ca0664b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_780dbe5e85bea257028d17d1a35abe524f70d9e64fa6f0350a6b74b99ca0664b->leave($__internal_780dbe5e85bea257028d17d1a35abe524f70d9e64fa6f0350a6b74b99ca0664b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
