<?php

/* @Twig/Exception/exception.json.twig */
class __TwigTemplate_cd521cfd8fede6098792a53ef2ff5271101d1bcbb23d3b35bc77286a58543bb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_882be5483f9f6361421b64cbdd85e2cb3453760c5703ff3375db6f95bb949b1c = $this->env->getExtension("native_profiler");
        $__internal_882be5483f9f6361421b64cbdd85e2cb3453760c5703ff3375db6f95bb949b1c->enter($__internal_882be5483f9f6361421b64cbdd85e2cb3453760c5703ff3375db6f95bb949b1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_882be5483f9f6361421b64cbdd85e2cb3453760c5703ff3375db6f95bb949b1c->leave($__internal_882be5483f9f6361421b64cbdd85e2cb3453760c5703ff3375db6f95bb949b1c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
