<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_8fa9be5ffb6f4afaa95b97909c55eabf839b9cfebb168a5835d15750a31b8ccc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8c8ee4730babb689941608996bd58a0bc5a0f9fe0961c6c8770663c5fe73a3f = $this->env->getExtension("native_profiler");
        $__internal_b8c8ee4730babb689941608996bd58a0bc5a0f9fe0961c6c8770663c5fe73a3f->enter($__internal_b8c8ee4730babb689941608996bd58a0bc5a0f9fe0961c6c8770663c5fe73a3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_b8c8ee4730babb689941608996bd58a0bc5a0f9fe0961c6c8770663c5fe73a3f->leave($__internal_b8c8ee4730babb689941608996bd58a0bc5a0f9fe0961c6c8770663c5fe73a3f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
