<?php

/* :Mentor:layout.html.twig */
class __TwigTemplate_5c99faf4b39ecd0d4945196e8567de4d1aa46d5b82308f2c4292e811f3a520c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Mentor:layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c6ee20157e3bd74fc75e351a406467770192686b8a684d1eb73828a133c2da6 = $this->env->getExtension("native_profiler");
        $__internal_8c6ee20157e3bd74fc75e351a406467770192686b8a684d1eb73828a133c2da6->enter($__internal_8c6ee20157e3bd74fc75e351a406467770192686b8a684d1eb73828a133c2da6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Mentor:layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8c6ee20157e3bd74fc75e351a406467770192686b8a684d1eb73828a133c2da6->leave($__internal_8c6ee20157e3bd74fc75e351a406467770192686b8a684d1eb73828a133c2da6_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b359f7da5b01f5beaffd608ed96112d85de703c61e06a94aaf830136307c4a79 = $this->env->getExtension("native_profiler");
        $__internal_b359f7da5b01f5beaffd608ed96112d85de703c61e06a94aaf830136307c4a79->enter($__internal_b359f7da5b01f5beaffd608ed96112d85de703c61e06a94aaf830136307c4a79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_b359f7da5b01f5beaffd608ed96112d85de703c61e06a94aaf830136307c4a79->leave($__internal_b359f7da5b01f5beaffd608ed96112d85de703c61e06a94aaf830136307c4a79_prof);

    }

    // line 6
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_66720e163c2958fcb8823317802cc5062233ba1894cb6046ecb49d70e8eca46a = $this->env->getExtension("native_profiler");
        $__internal_66720e163c2958fcb8823317802cc5062233ba1894cb6046ecb49d70e8eca46a->enter($__internal_66720e163c2958fcb8823317802cc5062233ba1894cb6046ecb49d70e8eca46a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 7
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 26
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 28
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

";
        
        $__internal_66720e163c2958fcb8823317802cc5062233ba1894cb6046ecb49d70e8eca46a->leave($__internal_66720e163c2958fcb8823317802cc5062233ba1894cb6046ecb49d70e8eca46a_prof);

    }

    // line 26
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_2452dccbed2584b41226d934c87d74ad4cca8b30dfd705731e7f811c9b12e1ba = $this->env->getExtension("native_profiler");
        $__internal_2452dccbed2584b41226d934c87d74ad4cca8b30dfd705731e7f811c9b12e1ba->enter($__internal_2452dccbed2584b41226d934c87d74ad4cca8b30dfd705731e7f811c9b12e1ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 27
        echo "                    ";
        
        $__internal_2452dccbed2584b41226d934c87d74ad4cca8b30dfd705731e7f811c9b12e1ba->leave($__internal_2452dccbed2584b41226d934c87d74ad4cca8b30dfd705731e7f811c9b12e1ba_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1022b6c466e9718d6a5659cd9cde1d54ed5f866586dfe67c65e26a6938fd3f39 = $this->env->getExtension("native_profiler");
        $__internal_1022b6c466e9718d6a5659cd9cde1d54ed5f866586dfe67c65e26a6938fd3f39->enter($__internal_1022b6c466e9718d6a5659cd9cde1d54ed5f866586dfe67c65e26a6938fd3f39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>

";
        
        $__internal_1022b6c466e9718d6a5659cd9cde1d54ed5f866586dfe67c65e26a6938fd3f39->leave($__internal_1022b6c466e9718d6a5659cd9cde1d54ed5f866586dfe67c65e26a6938fd3f39_prof);

    }

    public function getTemplateName()
    {
        return ":Mentor:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 39,  121 => 38,  116 => 37,  110 => 36,  103 => 27,  97 => 26,  85 => 28,  83 => 26,  72 => 18,  59 => 7,  53 => 6,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                     {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('bundles/fosjsrouting/js/router.js') }}"></script>*/
/*     <script src="{{ path('fos_js_routing_js', { callback: 'fos.Router.setData' }) }}"></script>*/
/* */
/* {% endblock %}*/
