<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_1694c9b004a0885269d270ee46451f338014393fcbb2f2ac8ee54573ff455f05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30b6bfadd5a1c4b764e1ae2f8b89bc888541e6ef0a78fdf75dac648f867bdd87 = $this->env->getExtension("native_profiler");
        $__internal_30b6bfadd5a1c4b764e1ae2f8b89bc888541e6ef0a78fdf75dac648f867bdd87->enter($__internal_30b6bfadd5a1c4b764e1ae2f8b89bc888541e6ef0a78fdf75dac648f867bdd87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_30b6bfadd5a1c4b764e1ae2f8b89bc888541e6ef0a78fdf75dac648f867bdd87->leave($__internal_30b6bfadd5a1c4b764e1ae2f8b89bc888541e6ef0a78fdf75dac648f867bdd87_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
