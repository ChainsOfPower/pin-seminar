<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_b3fae2683814c33ac56df9c48942724b927b38b1c36b4a85ec462a5eabca7939 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d34685cf01b52935bd8259e3b65d43d701ac891da07f4d243d12cf40c9a35ce3 = $this->env->getExtension("native_profiler");
        $__internal_d34685cf01b52935bd8259e3b65d43d701ac891da07f4d243d12cf40c9a35ce3->enter($__internal_d34685cf01b52935bd8259e3b65d43d701ac891da07f4d243d12cf40c9a35ce3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_d34685cf01b52935bd8259e3b65d43d701ac891da07f4d243d12cf40c9a35ce3->leave($__internal_d34685cf01b52935bd8259e3b65d43d701ac891da07f4d243d12cf40c9a35ce3_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
