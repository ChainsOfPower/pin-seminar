<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_c87a39eb1487c55a7cd275af531226abda85bed4830d0305c24cd5b8fc5cdaa2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e9814e03689cbebf41e171a5b6acf7616eb128bccd8238574010487e8f64ce21 = $this->env->getExtension("native_profiler");
        $__internal_e9814e03689cbebf41e171a5b6acf7616eb128bccd8238574010487e8f64ce21->enter($__internal_e9814e03689cbebf41e171a5b6acf7616eb128bccd8238574010487e8f64ce21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e9814e03689cbebf41e171a5b6acf7616eb128bccd8238574010487e8f64ce21->leave($__internal_e9814e03689cbebf41e171a5b6acf7616eb128bccd8238574010487e8f64ce21_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_d1c14e738df220d3120b200e028179105358d30a468a10fe8027a14b8bc321d0 = $this->env->getExtension("native_profiler");
        $__internal_d1c14e738df220d3120b200e028179105358d30a468a10fe8027a14b8bc321d0->enter($__internal_d1c14e738df220d3120b200e028179105358d30a468a10fe8027a14b8bc321d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_d1c14e738df220d3120b200e028179105358d30a468a10fe8027a14b8bc321d0->leave($__internal_d1c14e738df220d3120b200e028179105358d30a468a10fe8027a14b8bc321d0_prof);

    }

    // line 8
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5aede5ca7053df49bf6891ddf6cfd58af0652fa3b98aec20347c5ef8b08f1550 = $this->env->getExtension("native_profiler");
        $__internal_5aede5ca7053df49bf6891ddf6cfd58af0652fa3b98aec20347c5ef8b08f1550->enter($__internal_5aede5ca7053df49bf6891ddf6cfd58af0652fa3b98aec20347c5ef8b08f1550_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 9
        echo "    <h2><center>Unesi novu šifru</center></h2>
";
        // line 10
        $this->loadTemplate("FOSUserBundle:Resetting:reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 10)->display($context);
        
        $__internal_5aede5ca7053df49bf6891ddf6cfd58af0652fa3b98aec20347c5ef8b08f1550->leave($__internal_5aede5ca7053df49bf6891ddf6cfd58af0652fa3b98aec20347c5ef8b08f1550_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 10,  61 => 9,  55 => 8,  46 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <h2><center>Unesi novu šifru</center></h2>*/
/* {% include "FOSUserBundle:Resetting:reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
