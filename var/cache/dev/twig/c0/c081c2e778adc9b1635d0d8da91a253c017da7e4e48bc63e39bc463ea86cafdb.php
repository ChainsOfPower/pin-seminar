<?php

/* base.html.twig */
class __TwigTemplate_6d2c987ab4eea912dc3ad1c608b79758a9ee89ceb339aad6ae0a190fa0675118 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41e1f2865d0172fe7fd3055b028610a6f5490b2050e305b97660c8ced6d138b1 = $this->env->getExtension("native_profiler");
        $__internal_41e1f2865d0172fe7fd3055b028610a6f5490b2050e305b97660c8ced6d138b1->enter($__internal_41e1f2865d0172fe7fd3055b028610a6f5490b2050e305b97660c8ced6d138b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/app.css"), "html", null, true);
        echo "\", rel=\"stylesheet\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <script src=\"https://use.fontawesome.com/4677581ea6.js\"></script>
    </head>
    <body>
        ";
        // line 15
        $this->displayBlock('navigation', $context, $blocks);
        // line 16
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 17
        echo "        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>
        <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 19
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "    </body>
</html>
";
        
        $__internal_41e1f2865d0172fe7fd3055b028610a6f5490b2050e305b97660c8ced6d138b1->leave($__internal_41e1f2865d0172fe7fd3055b028610a6f5490b2050e305b97660c8ced6d138b1_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_987ad91df04924d413733d1acb02691e599f78b649ef20299ccf675b2e2249ca = $this->env->getExtension("native_profiler");
        $__internal_987ad91df04924d413733d1acb02691e599f78b649ef20299ccf675b2e2249ca->enter($__internal_987ad91df04924d413733d1acb02691e599f78b649ef20299ccf675b2e2249ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_987ad91df04924d413733d1acb02691e599f78b649ef20299ccf675b2e2249ca->leave($__internal_987ad91df04924d413733d1acb02691e599f78b649ef20299ccf675b2e2249ca_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_10aded0e5ebdb7a9d4db6abaf50f52d77d303894d1fd0393f828c00fd49e0c1b = $this->env->getExtension("native_profiler");
        $__internal_10aded0e5ebdb7a9d4db6abaf50f52d77d303894d1fd0393f828c00fd49e0c1b->enter($__internal_10aded0e5ebdb7a9d4db6abaf50f52d77d303894d1fd0393f828c00fd49e0c1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_10aded0e5ebdb7a9d4db6abaf50f52d77d303894d1fd0393f828c00fd49e0c1b->leave($__internal_10aded0e5ebdb7a9d4db6abaf50f52d77d303894d1fd0393f828c00fd49e0c1b_prof);

    }

    // line 15
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_d1b84117af05d0ebd6342d685227e5e123f539af3f7a20ae4f232f1e2f793e91 = $this->env->getExtension("native_profiler");
        $__internal_d1b84117af05d0ebd6342d685227e5e123f539af3f7a20ae4f232f1e2f793e91->enter($__internal_d1b84117af05d0ebd6342d685227e5e123f539af3f7a20ae4f232f1e2f793e91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        
        $__internal_d1b84117af05d0ebd6342d685227e5e123f539af3f7a20ae4f232f1e2f793e91->leave($__internal_d1b84117af05d0ebd6342d685227e5e123f539af3f7a20ae4f232f1e2f793e91_prof);

    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        $__internal_8848f51c74f694c1a6fd2c7e3ac3322ed90c83b2ce483994e154db3e1b00ff45 = $this->env->getExtension("native_profiler");
        $__internal_8848f51c74f694c1a6fd2c7e3ac3322ed90c83b2ce483994e154db3e1b00ff45->enter($__internal_8848f51c74f694c1a6fd2c7e3ac3322ed90c83b2ce483994e154db3e1b00ff45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_8848f51c74f694c1a6fd2c7e3ac3322ed90c83b2ce483994e154db3e1b00ff45->leave($__internal_8848f51c74f694c1a6fd2c7e3ac3322ed90c83b2ce483994e154db3e1b00ff45_prof);

    }

    // line 19
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_62730b055cc6929fcf6a07d0e669fe5c92d52b742992a7bd114c5ab98d9f91a1 = $this->env->getExtension("native_profiler");
        $__internal_62730b055cc6929fcf6a07d0e669fe5c92d52b742992a7bd114c5ab98d9f91a1->enter($__internal_62730b055cc6929fcf6a07d0e669fe5c92d52b742992a7bd114c5ab98d9f91a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_62730b055cc6929fcf6a07d0e669fe5c92d52b742992a7bd114c5ab98d9f91a1->leave($__internal_62730b055cc6929fcf6a07d0e669fe5c92d52b742992a7bd114c5ab98d9f91a1_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 19,  113 => 16,  102 => 15,  91 => 6,  80 => 5,  71 => 20,  69 => 19,  65 => 18,  62 => 17,  59 => 16,  57 => 15,  49 => 10,  44 => 8,  39 => 7,  37 => 6,  33 => 5,  27 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*         <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />*/
/*         <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />*/
/*         <link href="{{ asset('css/app.css') }}", rel="stylesheet" />*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <script src="https://use.fontawesome.com/4677581ea6.js"></script>*/
/*     </head>*/
/*     <body>*/
/*         {% block navigation %}{% endblock %}*/
/*         {% block content %}{% endblock %}*/
/*         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>*/
/*         <script src="{{ asset('js/bootstrap.min.js') }}"></script>*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
