<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_bed602dd8dbd0aeeaf13d2721c99258b22a9cb5e99793ee32b946d457b251155 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d9934404a6ef243ea51b82c61638988974cc407a7079202e34f64dff780c0f6 = $this->env->getExtension("native_profiler");
        $__internal_0d9934404a6ef243ea51b82c61638988974cc407a7079202e34f64dff780c0f6->enter($__internal_0d9934404a6ef243ea51b82c61638988974cc407a7079202e34f64dff780c0f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_0d9934404a6ef243ea51b82c61638988974cc407a7079202e34f64dff780c0f6->leave($__internal_0d9934404a6ef243ea51b82c61638988974cc407a7079202e34f64dff780c0f6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
