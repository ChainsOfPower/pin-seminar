<?php

/* @FOSUser/Group/edit.html.twig */
class __TwigTemplate_7d4bd95b770c2c3e0e1e5a80362394afc30061079ff78f2464e776745f080b70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Group/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_efdb72a5b144e16986181a7eb04a306c33ca585fcf680a955afdf69db0e0830b = $this->env->getExtension("native_profiler");
        $__internal_efdb72a5b144e16986181a7eb04a306c33ca585fcf680a955afdf69db0e0830b->enter($__internal_efdb72a5b144e16986181a7eb04a306c33ca585fcf680a955afdf69db0e0830b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_efdb72a5b144e16986181a7eb04a306c33ca585fcf680a955afdf69db0e0830b->leave($__internal_efdb72a5b144e16986181a7eb04a306c33ca585fcf680a955afdf69db0e0830b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_91485370385bd947ff4cce8357864260e5c4edef8d648dd5834826e72062905a = $this->env->getExtension("native_profiler");
        $__internal_91485370385bd947ff4cce8357864260e5c4edef8d648dd5834826e72062905a->enter($__internal_91485370385bd947ff4cce8357864260e5c4edef8d648dd5834826e72062905a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:edit_content.html.twig", "@FOSUser/Group/edit.html.twig", 4)->display($context);
        
        $__internal_91485370385bd947ff4cce8357864260e5c4edef8d648dd5834826e72062905a->leave($__internal_91485370385bd947ff4cce8357864260e5c4edef8d648dd5834826e72062905a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
