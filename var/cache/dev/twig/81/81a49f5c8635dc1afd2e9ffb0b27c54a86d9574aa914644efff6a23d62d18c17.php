<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_0136072d2e454e5b7ad809c28f5dd90ecfe46c845b5bf3e2beb053fd25cbbedf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e35828476fc0a514c3f6852210b222ce62189e8d7f5e46a6dacf60b07064a81 = $this->env->getExtension("native_profiler");
        $__internal_8e35828476fc0a514c3f6852210b222ce62189e8d7f5e46a6dacf60b07064a81->enter($__internal_8e35828476fc0a514c3f6852210b222ce62189e8d7f5e46a6dacf60b07064a81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_8e35828476fc0a514c3f6852210b222ce62189e8d7f5e46a6dacf60b07064a81->leave($__internal_8e35828476fc0a514c3f6852210b222ce62189e8d7f5e46a6dacf60b07064a81_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
