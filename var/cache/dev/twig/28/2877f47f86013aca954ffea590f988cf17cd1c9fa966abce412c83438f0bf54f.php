<?php

/* @FOSUser/Group/list.html.twig */
class __TwigTemplate_08271bb78e38b04c3f6d7bf881c988d5e4f2faf6a96b46874eb4a3aa591410c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Group/list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5a1d3f81b9a8675947f2b5d1005b44599d7ce0b3ecff4bb1518b7485d6df25de = $this->env->getExtension("native_profiler");
        $__internal_5a1d3f81b9a8675947f2b5d1005b44599d7ce0b3ecff4bb1518b7485d6df25de->enter($__internal_5a1d3f81b9a8675947f2b5d1005b44599d7ce0b3ecff4bb1518b7485d6df25de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a1d3f81b9a8675947f2b5d1005b44599d7ce0b3ecff4bb1518b7485d6df25de->leave($__internal_5a1d3f81b9a8675947f2b5d1005b44599d7ce0b3ecff4bb1518b7485d6df25de_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0f1a980b3e8d54451d4085db35e75be8ad0d3ab36ceb17a9cb09ea90c6601bd8 = $this->env->getExtension("native_profiler");
        $__internal_0f1a980b3e8d54451d4085db35e75be8ad0d3ab36ceb17a9cb09ea90c6601bd8->enter($__internal_0f1a980b3e8d54451d4085db35e75be8ad0d3ab36ceb17a9cb09ea90c6601bd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:list_content.html.twig", "@FOSUser/Group/list.html.twig", 4)->display($context);
        
        $__internal_0f1a980b3e8d54451d4085db35e75be8ad0d3ab36ceb17a9cb09ea90c6601bd8->leave($__internal_0f1a980b3e8d54451d4085db35e75be8ad0d3ab36ceb17a9cb09ea90c6601bd8_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:list_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
