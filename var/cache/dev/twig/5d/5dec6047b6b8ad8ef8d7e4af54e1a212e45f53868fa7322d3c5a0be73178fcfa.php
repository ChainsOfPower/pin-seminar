<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_4c410e8419cafcd8ac9561db8b4031f156b7a43cdf1ee9509f0262b824f1c68a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_beebdd9973caa47cd6781cb03190e5362217aa5da5c66a08c98fd5a8ffacf59c = $this->env->getExtension("native_profiler");
        $__internal_beebdd9973caa47cd6781cb03190e5362217aa5da5c66a08c98fd5a8ffacf59c->enter($__internal_beebdd9973caa47cd6781cb03190e5362217aa5da5c66a08c98fd5a8ffacf59c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_beebdd9973caa47cd6781cb03190e5362217aa5da5c66a08c98fd5a8ffacf59c->leave($__internal_beebdd9973caa47cd6781cb03190e5362217aa5da5c66a08c98fd5a8ffacf59c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
