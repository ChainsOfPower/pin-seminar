<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_c972a5c58028c37b80a2bd6cfa883b5d522fed3375053c3d3b16ffb216b3b8b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2697166de19389fdf72e45e08a30b54e220a366c0667a41ba89f906e07f0a02b = $this->env->getExtension("native_profiler");
        $__internal_2697166de19389fdf72e45e08a30b54e220a366c0667a41ba89f906e07f0a02b->enter($__internal_2697166de19389fdf72e45e08a30b54e220a366c0667a41ba89f906e07f0a02b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_2697166de19389fdf72e45e08a30b54e220a366c0667a41ba89f906e07f0a02b->leave($__internal_2697166de19389fdf72e45e08a30b54e220a366c0667a41ba89f906e07f0a02b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
