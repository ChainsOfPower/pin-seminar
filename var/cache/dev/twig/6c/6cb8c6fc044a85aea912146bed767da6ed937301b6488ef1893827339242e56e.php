<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_bac9ee6359dadbe55708397f78cfe4961be7c69c96e7df5186a3090918b34d40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_955c139f894181af1234468b8939df614f91c40e5029bd34d276bdef563af1b3 = $this->env->getExtension("native_profiler");
        $__internal_955c139f894181af1234468b8939df614f91c40e5029bd34d276bdef563af1b3->enter($__internal_955c139f894181af1234468b8939df614f91c40e5029bd34d276bdef563af1b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_955c139f894181af1234468b8939df614f91c40e5029bd34d276bdef563af1b3->leave($__internal_955c139f894181af1234468b8939df614f91c40e5029bd34d276bdef563af1b3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
