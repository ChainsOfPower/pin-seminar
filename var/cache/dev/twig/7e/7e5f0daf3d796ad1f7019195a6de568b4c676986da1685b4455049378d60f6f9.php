<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_88a53e244a52fa8bad955b093f87888f42e576af7541bd5b2cd8e7814449e436 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_43c7b1d736d90aa06029a7a83ed696076cd84424c9b86c38b09a1867173f86a3 = $this->env->getExtension("native_profiler");
        $__internal_43c7b1d736d90aa06029a7a83ed696076cd84424c9b86c38b09a1867173f86a3->enter($__internal_43c7b1d736d90aa06029a7a83ed696076cd84424c9b86c38b09a1867173f86a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_43c7b1d736d90aa06029a7a83ed696076cd84424c9b86c38b09a1867173f86a3->leave($__internal_43c7b1d736d90aa06029a7a83ed696076cd84424c9b86c38b09a1867173f86a3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
