<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_01ce2a53e49cbeb8a7019b0bb3557600a3fb3cb274e4fcba0c45a5e56ff0e46d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0181aad791c409a073098d28a521d5d13f3d5363284a840e342886ec8a5d44a2 = $this->env->getExtension("native_profiler");
        $__internal_0181aad791c409a073098d28a521d5d13f3d5363284a840e342886ec8a5d44a2->enter($__internal_0181aad791c409a073098d28a521d5d13f3d5363284a840e342886ec8a5d44a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_0181aad791c409a073098d28a521d5d13f3d5363284a840e342886ec8a5d44a2->leave($__internal_0181aad791c409a073098d28a521d5d13f3d5363284a840e342886ec8a5d44a2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
