<?php

/* ::base.html.twig */
class __TwigTemplate_8caeee6289e1639f3acdad2cec9abf98f7c0b4bad42995b995aa58c731111814 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b9763d29aeac6557e958325b55579eca963dee84948d345f4c9a5c917950cd8 = $this->env->getExtension("native_profiler");
        $__internal_8b9763d29aeac6557e958325b55579eca963dee84948d345f4c9a5c917950cd8->enter($__internal_8b9763d29aeac6557e958325b55579eca963dee84948d345f4c9a5c917950cd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/app.css"), "html", null, true);
        echo "\", rel=\"stylesheet\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <script src=\"https://use.fontawesome.com/4677581ea6.js\"></script>
    </head>
    <body>
        ";
        // line 15
        $this->displayBlock('navigation', $context, $blocks);
        // line 16
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 17
        echo "        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>
        <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 19
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "    </body>
</html>
";
        
        $__internal_8b9763d29aeac6557e958325b55579eca963dee84948d345f4c9a5c917950cd8->leave($__internal_8b9763d29aeac6557e958325b55579eca963dee84948d345f4c9a5c917950cd8_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_63cb41f96fb2d8dbf2b8c17a743fc1a7ee4dfb96afcc2677a91975dbcf88b472 = $this->env->getExtension("native_profiler");
        $__internal_63cb41f96fb2d8dbf2b8c17a743fc1a7ee4dfb96afcc2677a91975dbcf88b472->enter($__internal_63cb41f96fb2d8dbf2b8c17a743fc1a7ee4dfb96afcc2677a91975dbcf88b472_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_63cb41f96fb2d8dbf2b8c17a743fc1a7ee4dfb96afcc2677a91975dbcf88b472->leave($__internal_63cb41f96fb2d8dbf2b8c17a743fc1a7ee4dfb96afcc2677a91975dbcf88b472_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3cab4c92cfe5285c7b466946c07b8b9d17d0c4bf29f70f1a19f336971452421a = $this->env->getExtension("native_profiler");
        $__internal_3cab4c92cfe5285c7b466946c07b8b9d17d0c4bf29f70f1a19f336971452421a->enter($__internal_3cab4c92cfe5285c7b466946c07b8b9d17d0c4bf29f70f1a19f336971452421a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3cab4c92cfe5285c7b466946c07b8b9d17d0c4bf29f70f1a19f336971452421a->leave($__internal_3cab4c92cfe5285c7b466946c07b8b9d17d0c4bf29f70f1a19f336971452421a_prof);

    }

    // line 15
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_032a5a0ea1df6c45c3530a237ce6aef18723ab47bd7790e58832238cf5618589 = $this->env->getExtension("native_profiler");
        $__internal_032a5a0ea1df6c45c3530a237ce6aef18723ab47bd7790e58832238cf5618589->enter($__internal_032a5a0ea1df6c45c3530a237ce6aef18723ab47bd7790e58832238cf5618589_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        
        $__internal_032a5a0ea1df6c45c3530a237ce6aef18723ab47bd7790e58832238cf5618589->leave($__internal_032a5a0ea1df6c45c3530a237ce6aef18723ab47bd7790e58832238cf5618589_prof);

    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        $__internal_e81fe0dfd6812eaa315f6e302040aaa773091a0cc41683b0d5e9742a96a81623 = $this->env->getExtension("native_profiler");
        $__internal_e81fe0dfd6812eaa315f6e302040aaa773091a0cc41683b0d5e9742a96a81623->enter($__internal_e81fe0dfd6812eaa315f6e302040aaa773091a0cc41683b0d5e9742a96a81623_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_e81fe0dfd6812eaa315f6e302040aaa773091a0cc41683b0d5e9742a96a81623->leave($__internal_e81fe0dfd6812eaa315f6e302040aaa773091a0cc41683b0d5e9742a96a81623_prof);

    }

    // line 19
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_4aa9d3f422450c60413c2ccd324bbf7c12bb82b61f26662efa381e1a91a6704e = $this->env->getExtension("native_profiler");
        $__internal_4aa9d3f422450c60413c2ccd324bbf7c12bb82b61f26662efa381e1a91a6704e->enter($__internal_4aa9d3f422450c60413c2ccd324bbf7c12bb82b61f26662efa381e1a91a6704e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_4aa9d3f422450c60413c2ccd324bbf7c12bb82b61f26662efa381e1a91a6704e->leave($__internal_4aa9d3f422450c60413c2ccd324bbf7c12bb82b61f26662efa381e1a91a6704e_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 19,  113 => 16,  102 => 15,  91 => 6,  80 => 5,  71 => 20,  69 => 19,  65 => 18,  62 => 17,  59 => 16,  57 => 15,  49 => 10,  44 => 8,  39 => 7,  37 => 6,  33 => 5,  27 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*         <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />*/
/*         <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />*/
/*         <link href="{{ asset('css/app.css') }}", rel="stylesheet" />*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <script src="https://use.fontawesome.com/4677581ea6.js"></script>*/
/*     </head>*/
/*     <body>*/
/*         {% block navigation %}{% endblock %}*/
/*         {% block content %}{% endblock %}*/
/*         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>*/
/*         <script src="{{ asset('js/bootstrap.min.js') }}"></script>*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
