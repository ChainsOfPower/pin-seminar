<?php

/* Home/layout.html.twig */
class __TwigTemplate_3e58c37f32c78742c8a79c82ac64df0c3458ce3a6b068a11ca9dcafb619ea0fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "Home/layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35f11b596fb95f1d9b185dc9b723560b639b12ce1391709d51fd72f16f787938 = $this->env->getExtension("native_profiler");
        $__internal_35f11b596fb95f1d9b185dc9b723560b639b12ce1391709d51fd72f16f787938->enter($__internal_35f11b596fb95f1d9b185dc9b723560b639b12ce1391709d51fd72f16f787938_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Home/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_35f11b596fb95f1d9b185dc9b723560b639b12ce1391709d51fd72f16f787938->leave($__internal_35f11b596fb95f1d9b185dc9b723560b639b12ce1391709d51fd72f16f787938_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_88e7d77d50721bf6ebc21e43f636eac6332bce2a953feaabb65a5f2c9f34b292 = $this->env->getExtension("native_profiler");
        $__internal_88e7d77d50721bf6ebc21e43f636eac6332bce2a953feaabb65a5f2c9f34b292->enter($__internal_88e7d77d50721bf6ebc21e43f636eac6332bce2a953feaabb65a5f2c9f34b292_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_88e7d77d50721bf6ebc21e43f636eac6332bce2a953feaabb65a5f2c9f34b292->leave($__internal_88e7d77d50721bf6ebc21e43f636eac6332bce2a953feaabb65a5f2c9f34b292_prof);

    }

    // line 6
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_cedb2a10b8ded3df6d34445eb26730827dd78862862da9f7453aeba57e9c9a3e = $this->env->getExtension("native_profiler");
        $__internal_cedb2a10b8ded3df6d34445eb26730827dd78862862da9f7453aeba57e9c9a3e->enter($__internal_cedb2a10b8ded3df6d34445eb26730827dd78862862da9f7453aeba57e9c9a3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 7
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                   ";
        // line 26
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 28
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

";
        
        $__internal_cedb2a10b8ded3df6d34445eb26730827dd78862862da9f7453aeba57e9c9a3e->leave($__internal_cedb2a10b8ded3df6d34445eb26730827dd78862862da9f7453aeba57e9c9a3e_prof);

    }

    // line 26
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_ea89fb25cb6f33f754605670e77ba42c9169dfa76d06ff5bd0c9b0f82283e812 = $this->env->getExtension("native_profiler");
        $__internal_ea89fb25cb6f33f754605670e77ba42c9169dfa76d06ff5bd0c9b0f82283e812->enter($__internal_ea89fb25cb6f33f754605670e77ba42c9169dfa76d06ff5bd0c9b0f82283e812_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 27
        echo "                    ";
        
        $__internal_ea89fb25cb6f33f754605670e77ba42c9169dfa76d06ff5bd0c9b0f82283e812->leave($__internal_ea89fb25cb6f33f754605670e77ba42c9169dfa76d06ff5bd0c9b0f82283e812_prof);

    }

    public function getTemplateName()
    {
        return "Home/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 27,  96 => 26,  84 => 28,  82 => 26,  71 => 18,  58 => 7,  52 => 6,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                    {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
