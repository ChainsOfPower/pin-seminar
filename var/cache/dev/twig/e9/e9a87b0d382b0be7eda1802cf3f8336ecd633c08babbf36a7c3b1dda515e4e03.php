<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_90280dacf84ac9f7bda63da71e9053b0b997b6b227ae4c0534c6268da876f170 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f9710207768d6d671a1eb0b6d54e648d85e2260143710bf5c1aabe1fb5760e1 = $this->env->getExtension("native_profiler");
        $__internal_8f9710207768d6d671a1eb0b6d54e648d85e2260143710bf5c1aabe1fb5760e1->enter($__internal_8f9710207768d6d671a1eb0b6d54e648d85e2260143710bf5c1aabe1fb5760e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8f9710207768d6d671a1eb0b6d54e648d85e2260143710bf5c1aabe1fb5760e1->leave($__internal_8f9710207768d6d671a1eb0b6d54e648d85e2260143710bf5c1aabe1fb5760e1_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8f96b361e2a2d86d2b3382fe49ddb174fdd4a52c2cbec7bcf55a6972ed41b44f = $this->env->getExtension("native_profiler");
        $__internal_8f96b361e2a2d86d2b3382fe49ddb174fdd4a52c2cbec7bcf55a6972ed41b44f->enter($__internal_8f96b361e2a2d86d2b3382fe49ddb174fdd4a52c2cbec7bcf55a6972ed41b44f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
";
        
        $__internal_8f96b361e2a2d86d2b3382fe49ddb174fdd4a52c2cbec7bcf55a6972ed41b44f->leave($__internal_8f96b361e2a2d86d2b3382fe49ddb174fdd4a52c2cbec7bcf55a6972ed41b44f_prof);

    }

    // line 10
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_97d8227a35c81f4bae9418d45bc79b69f23dcfcb79071e2bd4fb35e7e774ddca = $this->env->getExtension("native_profiler");
        $__internal_97d8227a35c81f4bae9418d45bc79b69f23dcfcb79071e2bd4fb35e7e774ddca->enter($__internal_97d8227a35c81f4bae9418d45bc79b69f23dcfcb79071e2bd4fb35e7e774ddca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 11
        echo "    <li class=\"active\"><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_97d8227a35c81f4bae9418d45bc79b69f23dcfcb79071e2bd4fb35e7e774ddca->leave($__internal_97d8227a35c81f4bae9418d45bc79b69f23dcfcb79071e2bd4fb35e7e774ddca_prof);

    }

    // line 16
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_26841e8017eca0309066171b885439ffc3b4d4e89e98dea05ee3dedb5cf5f2c0 = $this->env->getExtension("native_profiler");
        $__internal_26841e8017eca0309066171b885439ffc3b4d4e89e98dea05ee3dedb5cf5f2c0->enter($__internal_26841e8017eca0309066171b885439ffc3b4d4e89e98dea05ee3dedb5cf5f2c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 17
        echo "

    <div class=\"container\">

        <h2><center>Logiraj se</center></h2>
        <form action=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\" class=\"form-signin\">
            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />

            <label for=\"username\">E-mail:</label>
            <input type=\"email\" id=\"username\" name=\"_username\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" class=\"form-control\" />

            <label for=\"password\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\"/>
            ";
        // line 30
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 31
            echo "                <div class=\"text-danger\">Pogrešni podaci</div>
            ";
        }
        // line 33
        echo "
            <div class=\"checkbox\">
                <label>
                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />Zapamti me
                </label>
            </div>

            <input class=\"btn btn-primary btn-block\" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"Logiraj se\" />

            <a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
        echo "\">Zaboravljena lozinka?</a>
        </form>



    </div>

";
        
        $__internal_26841e8017eca0309066171b885439ffc3b4d4e89e98dea05ee3dedb5cf5f2c0->leave($__internal_26841e8017eca0309066171b885439ffc3b4d4e89e98dea05ee3dedb5cf5f2c0_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 42,  110 => 33,  106 => 31,  104 => 30,  99 => 28,  94 => 26,  88 => 23,  84 => 22,  77 => 17,  71 => 16,  62 => 11,  56 => 10,  47 => 7,  42 => 6,  36 => 5,  11 => 1,);
    }
}
/* {% extends '@FOSUser/layout.html.twig' %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet" />*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li class="active"><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* */
/* {% block fos_user_content %}*/
/* */
/* */
/*     <div class="container">*/
/* */
/*         <h2><center>Logiraj se</center></h2>*/
/*         <form action="{{ path("fos_user_security_check") }}" method="post" class="form-signin">*/
/*             <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/* */
/*             <label for="username">E-mail:</label>*/
/*             <input type="email" id="username" name="_username" value="{{ last_username }}" required="required" class="form-control" />*/
/* */
/*             <label for="password">{{ 'security.login.password'|trans }}</label>*/
/*             <input type="password" id="password" name="_password" required="required" class="form-control"/>*/
/*             {% if error %}*/
/*                 <div class="text-danger">Pogrešni podaci</div>*/
/*             {% endif %}*/
/* */
/*             <div class="checkbox">*/
/*                 <label>*/
/*                     <input type="checkbox" id="remember_me" name="_remember_me" value="on" />Zapamti me*/
/*                 </label>*/
/*             </div>*/
/* */
/*             <input class="btn btn-primary btn-block" type="submit" id="_submit" name="_submit" value="Logiraj se" />*/
/* */
/*             <a href="{{ path('fos_user_resetting_request') }}">Zaboravljena lozinka?</a>*/
/*         </form>*/
/* */
/* */
/* */
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
