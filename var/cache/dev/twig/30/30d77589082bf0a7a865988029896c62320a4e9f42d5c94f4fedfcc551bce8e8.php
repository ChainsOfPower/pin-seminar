<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_50ea774c7145b08bad762a7f08a0fbad58ecbd4e7e919090ae7d280c7ee77497 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_467cf9036d5726a6d887b414792fbc5c7cb1442ef2d70a8f5e58d848b3a303b9 = $this->env->getExtension("native_profiler");
        $__internal_467cf9036d5726a6d887b414792fbc5c7cb1442ef2d70a8f5e58d848b3a303b9->enter($__internal_467cf9036d5726a6d887b414792fbc5c7cb1442ef2d70a8f5e58d848b3a303b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_467cf9036d5726a6d887b414792fbc5c7cb1442ef2d70a8f5e58d848b3a303b9->leave($__internal_467cf9036d5726a6d887b414792fbc5c7cb1442ef2d70a8f5e58d848b3a303b9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
