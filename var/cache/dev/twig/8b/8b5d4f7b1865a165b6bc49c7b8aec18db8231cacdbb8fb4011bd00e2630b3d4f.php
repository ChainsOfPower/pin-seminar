<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_5694bdbed76a303a7912348d324fd3d8c9e5eec45a3a9eb450099db3a249b0f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7806eb4eceec03a4d3f9f8f4b52dbbfa1e2885e56065a8bd159b67fd3aee858 = $this->env->getExtension("native_profiler");
        $__internal_e7806eb4eceec03a4d3f9f8f4b52dbbfa1e2885e56065a8bd159b67fd3aee858->enter($__internal_e7806eb4eceec03a4d3f9f8f4b52dbbfa1e2885e56065a8bd159b67fd3aee858_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_e7806eb4eceec03a4d3f9f8f4b52dbbfa1e2885e56065a8bd159b67fd3aee858->leave($__internal_e7806eb4eceec03a4d3f9f8f4b52dbbfa1e2885e56065a8bd159b67fd3aee858_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
