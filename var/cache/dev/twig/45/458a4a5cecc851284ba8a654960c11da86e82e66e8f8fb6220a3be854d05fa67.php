<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_039a85b8a0d0061f32c0907f1b3b9190924c8a8f98c97b1cd1293e4ffbe080e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "FOSUserBundle::layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d4daaa7e452ae4c66ba3e682191df79c5466cfc1ce0d65fe65bab5ec8cb121c3 = $this->env->getExtension("native_profiler");
        $__internal_d4daaa7e452ae4c66ba3e682191df79c5466cfc1ce0d65fe65bab5ec8cb121c3->enter($__internal_d4daaa7e452ae4c66ba3e682191df79c5466cfc1ce0d65fe65bab5ec8cb121c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d4daaa7e452ae4c66ba3e682191df79c5466cfc1ce0d65fe65bab5ec8cb121c3->leave($__internal_d4daaa7e452ae4c66ba3e682191df79c5466cfc1ce0d65fe65bab5ec8cb121c3_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_4fe92c5df75763161871bca1e3a7f9943b3132ac56db7bcd20039c97c60eb396 = $this->env->getExtension("native_profiler");
        $__internal_4fe92c5df75763161871bca1e3a7f9943b3132ac56db7bcd20039c97c60eb396->enter($__internal_4fe92c5df75763161871bca1e3a7f9943b3132ac56db7bcd20039c97c60eb396_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_4fe92c5df75763161871bca1e3a7f9943b3132ac56db7bcd20039c97c60eb396->leave($__internal_4fe92c5df75763161871bca1e3a7f9943b3132ac56db7bcd20039c97c60eb396_prof);

    }

    // line 7
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_2eb9396cabad852b5ff559ae5a4b2ef95e61da95287b1ef5a7a63da2b026ac06 = $this->env->getExtension("native_profiler");
        $__internal_2eb9396cabad852b5ff559ae5a4b2ef95e61da95287b1ef5a7a63da2b026ac06->enter($__internal_2eb9396cabad852b5ff559ae5a4b2ef95e61da95287b1ef5a7a63da2b026ac06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 8
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 27
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 29
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    ";
        
        $__internal_2eb9396cabad852b5ff559ae5a4b2ef95e61da95287b1ef5a7a63da2b026ac06->leave($__internal_2eb9396cabad852b5ff559ae5a4b2ef95e61da95287b1ef5a7a63da2b026ac06_prof);

    }

    // line 27
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_cc279ddb56fa43bbd84cbe6a2360953fb6885739456e728ff87f58b2a3b0f954 = $this->env->getExtension("native_profiler");
        $__internal_cc279ddb56fa43bbd84cbe6a2360953fb6885739456e728ff87f58b2a3b0f954->enter($__internal_cc279ddb56fa43bbd84cbe6a2360953fb6885739456e728ff87f58b2a3b0f954_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 28
        echo "                    ";
        
        $__internal_cc279ddb56fa43bbd84cbe6a2360953fb6885739456e728ff87f58b2a3b0f954->leave($__internal_cc279ddb56fa43bbd84cbe6a2360953fb6885739456e728ff87f58b2a3b0f954_prof);

    }

    // line 35
    public function block_title($context, array $blocks = array())
    {
        $__internal_63432b0d64549fbac0da0277396d3730d191884a2d9a371a531825580e025666 = $this->env->getExtension("native_profiler");
        $__internal_63432b0d64549fbac0da0277396d3730d191884a2d9a371a531825580e025666->enter($__internal_63432b0d64549fbac0da0277396d3730d191884a2d9a371a531825580e025666_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_63432b0d64549fbac0da0277396d3730d191884a2d9a371a531825580e025666->leave($__internal_63432b0d64549fbac0da0277396d3730d191884a2d9a371a531825580e025666_prof);

    }

    // line 37
    public function block_content($context, array $blocks = array())
    {
        $__internal_396d6f941c624df8d41e844fc8874010dcf96d4dbbb347a14efbfedc2865fd95 = $this->env->getExtension("native_profiler");
        $__internal_396d6f941c624df8d41e844fc8874010dcf96d4dbbb347a14efbfedc2865fd95->enter($__internal_396d6f941c624df8d41e844fc8874010dcf96d4dbbb347a14efbfedc2865fd95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 38
        echo "
    ";
        // line 39
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 40
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 41
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 42
                    echo "                <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo " pagination-centered\">
                    <h2><center> ";
                    // line 43
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo " </center></h2>
                </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "    ";
        }
        // line 48
        echo "
    <div>
        ";
        // line 50
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 52
        echo "    </div>
";
        
        $__internal_396d6f941c624df8d41e844fc8874010dcf96d4dbbb347a14efbfedc2865fd95->leave($__internal_396d6f941c624df8d41e844fc8874010dcf96d4dbbb347a14efbfedc2865fd95_prof);

    }

    // line 50
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_19ca4db4d544aa8b7c28567e20cbc7dbe032a27c394c16550ebf8c8627f44546 = $this->env->getExtension("native_profiler");
        $__internal_19ca4db4d544aa8b7c28567e20cbc7dbe032a27c394c16550ebf8c8627f44546->enter($__internal_19ca4db4d544aa8b7c28567e20cbc7dbe032a27c394c16550ebf8c8627f44546_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 51
        echo "        ";
        
        $__internal_19ca4db4d544aa8b7c28567e20cbc7dbe032a27c394c16550ebf8c8627f44546->leave($__internal_19ca4db4d544aa8b7c28567e20cbc7dbe032a27c394c16550ebf8c8627f44546_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 51,  181 => 50,  173 => 52,  171 => 50,  167 => 48,  164 => 47,  158 => 46,  149 => 43,  144 => 42,  139 => 41,  134 => 40,  132 => 39,  129 => 38,  123 => 37,  111 => 35,  104 => 28,  98 => 27,  87 => 29,  85 => 27,  74 => 19,  61 => 8,  55 => 7,  45 => 4,  39 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                     {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/*     {% endblock %}*/
/* */
/* {% block title %} {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     {% if app.request.hasPreviousSession %}*/
/*         {% for type, messages in app.session.flashbag.all() %}*/
/*             {% for message in messages %}*/
/*                 <div class="flash-{{ type }} pagination-centered">*/
/*                     <h2><center> {{ message }} </center></h2>*/
/*                 </div>*/
/*             {% endfor %}*/
/*         {% endfor %}*/
/*     {% endif %}*/
/* */
/*     <div>*/
/*         {% block fos_user_content %}*/
/*         {% endblock fos_user_content %}*/
/*     </div>*/
/* {% endblock %}*/
