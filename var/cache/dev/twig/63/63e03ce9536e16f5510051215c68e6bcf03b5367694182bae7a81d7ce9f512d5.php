<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_ad3cb4ae02713f28e58fc2bb6606c93d76f4b72224590c8d726e0d7248e9485a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d2349e6c22275ca4336d70f20bc0ab3fcdab7cfc16f929ad8e624f26862818c = $this->env->getExtension("native_profiler");
        $__internal_7d2349e6c22275ca4336d70f20bc0ab3fcdab7cfc16f929ad8e624f26862818c->enter($__internal_7d2349e6c22275ca4336d70f20bc0ab3fcdab7cfc16f929ad8e624f26862818c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7d2349e6c22275ca4336d70f20bc0ab3fcdab7cfc16f929ad8e624f26862818c->leave($__internal_7d2349e6c22275ca4336d70f20bc0ab3fcdab7cfc16f929ad8e624f26862818c_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_df97b52355f9c34c93c124d57505cd5ce13e4282320e6bd401adde1293d35d9d = $this->env->getExtension("native_profiler");
        $__internal_df97b52355f9c34c93c124d57505cd5ce13e4282320e6bd401adde1293d35d9d->enter($__internal_df97b52355f9c34c93c124d57505cd5ce13e4282320e6bd401adde1293d35d9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_df97b52355f9c34c93c124d57505cd5ce13e4282320e6bd401adde1293d35d9d->leave($__internal_df97b52355f9c34c93c124d57505cd5ce13e4282320e6bd401adde1293d35d9d_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_714e3cb361ac7b863b0864d65b265a69144074cc1e335d20bc3c4839ee71f730 = $this->env->getExtension("native_profiler");
        $__internal_714e3cb361ac7b863b0864d65b265a69144074cc1e335d20bc3c4839ee71f730->enter($__internal_714e3cb361ac7b863b0864d65b265a69144074cc1e335d20bc3c4839ee71f730_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_714e3cb361ac7b863b0864d65b265a69144074cc1e335d20bc3c4839ee71f730->leave($__internal_714e3cb361ac7b863b0864d65b265a69144074cc1e335d20bc3c4839ee71f730_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d9079c6205daff99464a062d6f32ceeb43daf1c7c982692f132cf6d4d9cf6e8c = $this->env->getExtension("native_profiler");
        $__internal_d9079c6205daff99464a062d6f32ceeb43daf1c7c982692f132cf6d4d9cf6e8c->enter($__internal_d9079c6205daff99464a062d6f32ceeb43daf1c7c982692f132cf6d4d9cf6e8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_d9079c6205daff99464a062d6f32ceeb43daf1c7c982692f132cf6d4d9cf6e8c->leave($__internal_d9079c6205daff99464a062d6f32ceeb43daf1c7c982692f132cf6d4d9cf6e8c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
