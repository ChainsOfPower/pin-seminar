<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_6f7339abcf7d1f808a25262c044ad9c4c7d9fcaa4c199ac792f4acf16c1bbbd6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a4b07b027dfd0e089cfb6807f91df69c789afc7b32f9d06f6271b198c9aa69b = $this->env->getExtension("native_profiler");
        $__internal_0a4b07b027dfd0e089cfb6807f91df69c789afc7b32f9d06f6271b198c9aa69b->enter($__internal_0a4b07b027dfd0e089cfb6807f91df69c789afc7b32f9d06f6271b198c9aa69b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0a4b07b027dfd0e089cfb6807f91df69c789afc7b32f9d06f6271b198c9aa69b->leave($__internal_0a4b07b027dfd0e089cfb6807f91df69c789afc7b32f9d06f6271b198c9aa69b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ce06e4c7096bdf5b8bd1415fd0daab1d80417d8a515b739eed878fb2ba59efa7 = $this->env->getExtension("native_profiler");
        $__internal_ce06e4c7096bdf5b8bd1415fd0daab1d80417d8a515b739eed878fb2ba59efa7->enter($__internal_ce06e4c7096bdf5b8bd1415fd0daab1d80417d8a515b739eed878fb2ba59efa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_ce06e4c7096bdf5b8bd1415fd0daab1d80417d8a515b739eed878fb2ba59efa7->leave($__internal_ce06e4c7096bdf5b8bd1415fd0daab1d80417d8a515b739eed878fb2ba59efa7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
