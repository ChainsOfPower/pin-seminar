<?php

/* :Student:izvanredni_home.html.twig */
class __TwigTemplate_627145f4a311e4d344e25b85d13eb63fe773eed93900f077c99f8e1181dd2d3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Student/layout.html.twig", ":Student:izvanredni_home.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Student/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_43ac396ca620ce41945de750851ec383efc65073933516e4ea6411c896bf18f0 = $this->env->getExtension("native_profiler");
        $__internal_43ac396ca620ce41945de750851ec383efc65073933516e4ea6411c896bf18f0->enter($__internal_43ac396ca620ce41945de750851ec383efc65073933516e4ea6411c896bf18f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Student:izvanredni_home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_43ac396ca620ce41945de750851ec383efc65073933516e4ea6411c896bf18f0->leave($__internal_43ac396ca620ce41945de750851ec383efc65073933516e4ea6411c896bf18f0_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_5bdb8ab5b13a3d3555a4363c9064c5ce584213805acb822663219bf9762b925e = $this->env->getExtension("native_profiler");
        $__internal_5bdb8ab5b13a3d3555a4363c9064c5ce584213805acb822663219bf9762b925e->enter($__internal_5bdb8ab5b13a3d3555a4363c9064c5ce584213805acb822663219bf9762b925e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/table_student.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_5bdb8ab5b13a3d3555a4363c9064c5ce584213805acb822663219bf9762b925e->leave($__internal_5bdb8ab5b13a3d3555a4363c9064c5ce584213805acb822663219bf9762b925e_prof);

    }

    // line 6
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_876f0ec8b6467ba9432d0b006160567e23d269f3eb1f51750973726314c8683c = $this->env->getExtension("native_profiler");
        $__internal_876f0ec8b6467ba9432d0b006160567e23d269f3eb1f51750973726314c8683c->enter($__internal_876f0ec8b6467ba9432d0b006160567e23d269f3eb1f51750973726314c8683c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 7
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("ROLE_STUDENT")) {
            // line 8
            echo "        <li class=\"active\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("student_homepage");
            echo "\">Upisni list</a></li>
        <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
        <li><a href=\"/logout\">Odjavi se</a></li>
    ";
        }
        // line 12
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("ROLE_MENTOR")) {
            // line 13
            echo "        <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("mentor_studenti");
            echo "\">Studenti</a></li>
        <li><a href=\"";
            // line 14
            echo $this->env->getExtension('routing')->getPath("subject_index");
            echo "\">Predmeti</a></li>
        <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
        <li><a href=\"/logout\">Odjavi se</a></li>
    ";
        }
        
        $__internal_876f0ec8b6467ba9432d0b006160567e23d269f3eb1f51750973726314c8683c->leave($__internal_876f0ec8b6467ba9432d0b006160567e23d269f3eb1f51750973726314c8683c_prof);

    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        $__internal_328b86ee422367812871286a1e6c60f03af15e93d95456ff175b9e89fd318a3e = $this->env->getExtension("native_profiler");
        $__internal_328b86ee422367812871286a1e6c60f03af15e93d95456ff175b9e89fd318a3e->enter($__internal_328b86ee422367812871286a1e6c60f03af15e93d95456ff175b9e89fd318a3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 22
        echo "    <div style=\"display: none\" id=\"token\">";
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "</div>
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-6\">
                <h2>Predmeti:</h2>
                <div class=\"sadrzaj\">
                    <ul class=\"list-group\" id=\"neupisani_predmeti_studenta\">
                        ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["neupisani"]) ? $context["neupisani"] : $this->getContext($context, "neupisani")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 30
            echo "                            <li class=\"list-group-item\"><a href=\"javascript:void(0)\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\"><i
                                            class=\"fa fa-plus-square fa-lg\"
                                            aria-hidden=\"true\"></i></a> ";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                    </ul>
                </div>


            </div>

            <div class=\"col-md-6\" id=\"upisani_predmeti_studenta\">

                <h2>Upisi (";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "username", array()), "html", null, true);
        echo ")</h2>
                <span style=\"display: none\" id=\"korisnik\">";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "id", array()), "html", null, true);
        echo "</span>
                <h3>Semestar 1:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"prviSemestar\">

                    ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prviSemestar"]) ? $context["prviSemestar"] : $this->getContext($context, "prviSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 50
            echo "                        <tr>
                            <td>
                                ";
            // line 52
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 53
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 57
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 59
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "                    </tbody>
                </table>

                <h3>Semestar 2:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"drugiSemestar\">

                    ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["drugiSemestar"]) ? $context["drugiSemestar"] : $this->getContext($context, "drugiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 72
            echo "                        <tr>
                            <td>
                                ";
            // line 74
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 75
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 79
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 81
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 86
        echo "                    </tbody>
                </table>

                <h3>Semestar 3:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"treciSemestar\">

                    ";
        // line 93
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["treciSemestar"]) ? $context["treciSemestar"] : $this->getContext($context, "treciSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 94
            echo "                        <tr>
                            <td>
                                ";
            // line 96
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 97
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 101
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 103
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "                    </tbody>
                </table>

                <h3>Semestar 4:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"cetvrtiSemestar\">

                    ";
        // line 115
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cetvrtiSemestar"]) ? $context["cetvrtiSemestar"] : $this->getContext($context, "cetvrtiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 116
            echo "                        <tr>
                            <td>
                                ";
            // line 118
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 119
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 123
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 125
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "                    </tbody>
                </table>

                <h3>Semestar 5:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"petiSemestar\">

                    ";
        // line 137
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["petiSemestar"]) ? $context["petiSemestar"] : $this->getContext($context, "petiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 138
            echo "                        <tr>
                            <td>
                                ";
            // line 140
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 141
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 145
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 147
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 148
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 152
        echo "                    </tbody>
                </table>

                <h3>Semestar 6:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"sestiSemestar\">

                    ";
        // line 159
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sestiSemestar"]) ? $context["sestiSemestar"] : $this->getContext($context, "sestiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 160
            echo "                        <tr>
                            <td>
                                ";
            // line 162
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 163
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 167
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 169
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 170
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 174
        echo "                    </tbody>
                </table>

                <h3>Semestar 7:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"sedmiSemestar\">

                    ";
        // line 181
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sedmiSemestar"]) ? $context["sedmiSemestar"] : $this->getContext($context, "sedmiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 182
            echo "                        <tr>
                            <td>
                                ";
            // line 184
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 185
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 189
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 191
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 192
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 196
        echo "                    </tbody>
                </table>

                <h3>Semestar 8:</h3>
                <table class=\"table table-bordered\">
                    <tbody id=\"osmiSemestar\">

                    ";
        // line 203
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["osmiSemestar"]) ? $context["osmiSemestar"] : $this->getContext($context, "osmiSemestar")));
        foreach ($context['_seq'] as $context["_key"] => $context["predmet"]) {
            // line 204
            echo "                        <tr>
                            <td>
                                ";
            // line 206
            if (($this->getAttribute($context["predmet"], "status", array()) == "upisano")) {
                // line 207
                echo "                                    <a href=\"javascript:void(0)\"><i id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
                echo "\"
                                                                    class=\"fa fa-check-square-o fa-lg\"
                                                                    aria-hidden=\"true\"></i></a>
                                ";
            } else {
                // line 211
                echo "                                    <i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>
                                ";
            }
            // line 213
            echo "                                <a href=\"javascript:void(0)\"><i id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "id", array()), "html", null, true);
            echo "\" class=\"fa fa-times fa-lg\"></i></a>
                                ";
            // line 214
            echo twig_escape_filter($this->env, $this->getAttribute($context["predmet"], "ime", array()), "html", null, true);
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['predmet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 218
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
";
        
        $__internal_328b86ee422367812871286a1e6c60f03af15e93d95456ff175b9e89fd318a3e->leave($__internal_328b86ee422367812871286a1e6c60f03af15e93d95456ff175b9e89fd318a3e_prof);

    }

    // line 225
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_a7dd5f38269271107257a851b1ce5231b139352d53b9351b769285d775b1ecb7 = $this->env->getExtension("native_profiler");
        $__internal_a7dd5f38269271107257a851b1ce5231b139352d53b9351b769285d775b1ecb7->enter($__internal_a7dd5f38269271107257a851b1ce5231b139352d53b9351b769285d775b1ecb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 226
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 227
        if ($this->env->getExtension('security')->isGranted("ROLE_STUDENT")) {
            // line 228
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/izvanredni_student.js"), "html", null, true);
            echo "\"></script>
    ";
        }
        // line 230
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("ROLE_MENTOR")) {
            // line 231
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/mentor_izvanredni_student.js"), "html", null, true);
            echo "\"></script>
    ";
        }
        
        $__internal_a7dd5f38269271107257a851b1ce5231b139352d53b9351b769285d775b1ecb7->leave($__internal_a7dd5f38269271107257a851b1ce5231b139352d53b9351b769285d775b1ecb7_prof);

    }

    public function getTemplateName()
    {
        return ":Student:izvanredni_home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  545 => 231,  542 => 230,  536 => 228,  534 => 227,  529 => 226,  523 => 225,  511 => 218,  501 => 214,  496 => 213,  492 => 211,  484 => 207,  482 => 206,  478 => 204,  474 => 203,  465 => 196,  455 => 192,  450 => 191,  446 => 189,  438 => 185,  436 => 184,  432 => 182,  428 => 181,  419 => 174,  409 => 170,  404 => 169,  400 => 167,  392 => 163,  390 => 162,  386 => 160,  382 => 159,  373 => 152,  363 => 148,  358 => 147,  354 => 145,  346 => 141,  344 => 140,  340 => 138,  336 => 137,  327 => 130,  317 => 126,  312 => 125,  308 => 123,  300 => 119,  298 => 118,  294 => 116,  290 => 115,  281 => 108,  271 => 104,  266 => 103,  262 => 101,  254 => 97,  252 => 96,  248 => 94,  244 => 93,  235 => 86,  225 => 82,  220 => 81,  216 => 79,  208 => 75,  206 => 74,  202 => 72,  198 => 71,  189 => 64,  179 => 60,  174 => 59,  170 => 57,  162 => 53,  160 => 52,  156 => 50,  152 => 49,  144 => 44,  140 => 43,  130 => 35,  121 => 32,  115 => 30,  111 => 29,  100 => 22,  94 => 21,  82 => 14,  77 => 13,  74 => 12,  66 => 8,  63 => 7,  57 => 6,  48 => 4,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'Student/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/table_student.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* {% block navBarLinks %}*/
/*     {% if is_granted('ROLE_STUDENT') %}*/
/*         <li class="active"><a href="{{ path('student_homepage') }}">Upisni list</a></li>*/
/*         <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*         <li><a href="/logout">Odjavi se</a></li>*/
/*     {% endif %}*/
/*     {% if is_granted('ROLE_MENTOR') %}*/
/*         <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*         <li><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*         <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*         <li><a href="/logout">Odjavi se</a></li>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
/* */
/* {% block content %}*/
/*     <div style="display: none" id="token">{{ token }}</div>*/
/*     <div class="container-fluid">*/
/*         <div class="row">*/
/*             <div class="col-md-6">*/
/*                 <h2>Predmeti:</h2>*/
/*                 <div class="sadrzaj">*/
/*                     <ul class="list-group" id="neupisani_predmeti_studenta">*/
/*                         {% for predmet in neupisani %}*/
/*                             <li class="list-group-item"><a href="javascript:void(0)" id="{{ predmet.id }}"><i*/
/*                                             class="fa fa-plus-square fa-lg"*/
/*                                             aria-hidden="true"></i></a> {{ predmet.ime }}*/
/*                             </li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
/* */
/* */
/*             </div>*/
/* */
/*             <div class="col-md-6" id="upisani_predmeti_studenta">*/
/* */
/*                 <h2>Upisi ({{ student.username }})</h2>*/
/*                 <span style="display: none" id="korisnik">{{ student.id }}</span>*/
/*                 <h3>Semestar 1:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="prviSemestar">*/
/* */
/*                     {% for predmet in prviSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 2:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="drugiSemestar">*/
/* */
/*                     {% for predmet in drugiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 3:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="treciSemestar">*/
/* */
/*                     {% for predmet in treciSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 4:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="cetvrtiSemestar">*/
/* */
/*                     {% for predmet in cetvrtiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 5:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="petiSemestar">*/
/* */
/*                     {% for predmet in petiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 6:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="sestiSemestar">*/
/* */
/*                     {% for predmet in sestiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 7:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="sedmiSemestar">*/
/* */
/*                     {% for predmet in sedmiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/* */
/*                 <h3>Semestar 8:</h3>*/
/*                 <table class="table table-bordered">*/
/*                     <tbody id="osmiSemestar">*/
/* */
/*                     {% for predmet in osmiSemestar %}*/
/*                         <tr>*/
/*                             <td>*/
/*                                 {% if predmet.status == 'upisano' %}*/
/*                                     <a href="javascript:void(0)"><i id="{{ predmet.id }}"*/
/*                                                                     class="fa fa-check-square-o fa-lg"*/
/*                                                                     aria-hidden="true"></i></a>*/
/*                                 {% else %}*/
/*                                     <i class="fa fa-check fa-lg" aria-hidden="true"></i>*/
/*                                 {% endif %}*/
/*                                 <a href="javascript:void(0)"><i id="{{ predmet.id }}" class="fa fa-times fa-lg"></i></a>*/
/*                                 {{ predmet.ime }}*/
/*                             </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     {% if is_granted('ROLE_STUDENT') %}*/
/*         <script src="{{ asset('js/izvanredni_student.js') }}"></script>*/
/*     {% endif %}*/
/*     {% if is_granted('ROLE_MENTOR') %}*/
/*         <script src="{{ asset('js/mentor_izvanredni_student.js') }}"></script>*/
/*     {% endif %}*/
/* {% endblock %}*/
