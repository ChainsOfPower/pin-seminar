<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_f1d51ee9fd6a87980fc9a9dcfe9495f8044961011ebe7f26c8ab31c53f5807f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d93a75255a490a672990f72d46095fd1dc85ef9213b5d52b2f418d0e38f8661c = $this->env->getExtension("native_profiler");
        $__internal_d93a75255a490a672990f72d46095fd1dc85ef9213b5d52b2f418d0e38f8661c->enter($__internal_d93a75255a490a672990f72d46095fd1dc85ef9213b5d52b2f418d0e38f8661c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_d93a75255a490a672990f72d46095fd1dc85ef9213b5d52b2f418d0e38f8661c->leave($__internal_d93a75255a490a672990f72d46095fd1dc85ef9213b5d52b2f418d0e38f8661c_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_0a6868944de68abd257c223c5b8259f43b2ad96a2e4d679bf4af2ff00dcde5c0 = $this->env->getExtension("native_profiler");
        $__internal_0a6868944de68abd257c223c5b8259f43b2ad96a2e4d679bf4af2ff00dcde5c0->enter($__internal_0a6868944de68abd257c223c5b8259f43b2ad96a2e4d679bf4af2ff00dcde5c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        echo "
";
        
        $__internal_0a6868944de68abd257c223c5b8259f43b2ad96a2e4d679bf4af2ff00dcde5c0->leave($__internal_0a6868944de68abd257c223c5b8259f43b2ad96a2e4d679bf4af2ff00dcde5c0_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_7096b32d1bc9a33453aea6ed19b54fb18aefa9ece0056a97c2bbf7ac44c1edf6 = $this->env->getExtension("native_profiler");
        $__internal_7096b32d1bc9a33453aea6ed19b54fb18aefa9ece0056a97c2bbf7ac44c1edf6->enter($__internal_7096b32d1bc9a33453aea6ed19b54fb18aefa9ece0056a97c2bbf7ac44c1edf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_7096b32d1bc9a33453aea6ed19b54fb18aefa9ece0056a97c2bbf7ac44c1edf6->leave($__internal_7096b32d1bc9a33453aea6ed19b54fb18aefa9ece0056a97c2bbf7ac44c1edf6_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_f8751d9f069f9d8fae120636f3696c9eb82934273ed3b927825ce4c4387a084f = $this->env->getExtension("native_profiler");
        $__internal_f8751d9f069f9d8fae120636f3696c9eb82934273ed3b927825ce4c4387a084f->enter($__internal_f8751d9f069f9d8fae120636f3696c9eb82934273ed3b927825ce4c4387a084f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_f8751d9f069f9d8fae120636f3696c9eb82934273ed3b927825ce4c4387a084f->leave($__internal_f8751d9f069f9d8fae120636f3696c9eb82934273ed3b927825ce4c4387a084f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
