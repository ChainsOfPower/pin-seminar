<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_3badd7afe0728bddaccb186500724db510befaed0b29f8641d7cd5188312f46f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fec08874ab8f0258554eddefac0a59c0ff356d331a50850fc45f89750f461e55 = $this->env->getExtension("native_profiler");
        $__internal_fec08874ab8f0258554eddefac0a59c0ff356d331a50850fc45f89750f461e55->enter($__internal_fec08874ab8f0258554eddefac0a59c0ff356d331a50850fc45f89750f461e55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_fec08874ab8f0258554eddefac0a59c0ff356d331a50850fc45f89750f461e55->leave($__internal_fec08874ab8f0258554eddefac0a59c0ff356d331a50850fc45f89750f461e55_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
