<?php

/* Subject/index.html.twig */
class __TwigTemplate_c442285a97eb5588b3b3d8a2e889b72a019e688a93854f58a263b8f435b0966b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Subject/layout.html.twig", "Subject/index.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Subject/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7d87626a15f27bfa21389e425a8349507ef07c83296f898b967b94cd3c8f679 = $this->env->getExtension("native_profiler");
        $__internal_f7d87626a15f27bfa21389e425a8349507ef07c83296f898b967b94cd3c8f679->enter($__internal_f7d87626a15f27bfa21389e425a8349507ef07c83296f898b967b94cd3c8f679_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Subject/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f7d87626a15f27bfa21389e425a8349507ef07c83296f898b967b94cd3c8f679->leave($__internal_f7d87626a15f27bfa21389e425a8349507ef07c83296f898b967b94cd3c8f679_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_dfdb3994a5875f11759d3322198ead099f95e9c25edd23c32c02ece8edbd59b6 = $this->env->getExtension("native_profiler");
        $__internal_dfdb3994a5875f11759d3322198ead099f95e9c25edd23c32c02ece8edbd59b6->enter($__internal_dfdb3994a5875f11759d3322198ead099f95e9c25edd23c32c02ece8edbd59b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/table_student.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_dfdb3994a5875f11759d3322198ead099f95e9c25edd23c32c02ece8edbd59b6->leave($__internal_dfdb3994a5875f11759d3322198ead099f95e9c25edd23c32c02ece8edbd59b6_prof);

    }

    // line 6
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_98dd8bae7f6991f91ed66a332d9044712d9773b0771ad421660fc8b3cf883c9f = $this->env->getExtension("native_profiler");
        $__internal_98dd8bae7f6991f91ed66a332d9044712d9773b0771ad421660fc8b3cf883c9f->enter($__internal_98dd8bae7f6991f91ed66a332d9044712d9773b0771ad421660fc8b3cf883c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 7
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("mentor_studenti");
        echo "\">Studenti</a></li>
    <li class=\"active\"><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("subject_index");
        echo "\">Predmeti</a></li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_98dd8bae7f6991f91ed66a332d9044712d9773b0771ad421660fc8b3cf883c9f->leave($__internal_98dd8bae7f6991f91ed66a332d9044712d9773b0771ad421660fc8b3cf883c9f_prof);

    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        $__internal_0837a44a7f639ab68142fc00f81e1960f0010a5c7b5c1a4a296e57ba8f68f155 = $this->env->getExtension("native_profiler");
        $__internal_0837a44a7f639ab68142fc00f81e1960f0010a5c7b5c1a4a296e57ba8f68f155->enter($__internal_0837a44a7f639ab68142fc00f81e1960f0010a5c7b5c1a4a296e57ba8f68f155_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 14
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\">
                <table class=\"table\">
                    <thead>
                    <tr>
                        <th><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("subject_new");
        echo "\">Novi predmet</a> </th>
                    </tr>
                    </thead>
                    <tbody id=\"lista_predmeta\">
                    ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["subjects"]) ? $context["subjects"] : $this->getContext($context, "subjects")));
        foreach ($context['_seq'] as $context["_key"] => $context["subject"]) {
            // line 25
            echo "                        <tr>
                            <td> ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["subject"], "ime", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($context["subject"], "kod", array()), "html", null, true);
            echo ") <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("subject_show", array("id" => $this->getAttribute($context["subject"], "id", array()))), "html", null, true);
            echo "\">Detalji</a> <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("subject_edit", array("id" => $this->getAttribute($context["subject"], "id", array()))), "html", null, true);
            echo "\">Uredi</a> </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subject'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
";
        
        $__internal_0837a44a7f639ab68142fc00f81e1960f0010a5c7b5c1a4a296e57ba8f68f155->leave($__internal_0837a44a7f639ab68142fc00f81e1960f0010a5c7b5c1a4a296e57ba8f68f155_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3d2cd4a21c691bdf80ed3297a7b14042682264b144448f821bd574fd22a5215f = $this->env->getExtension("native_profiler");
        $__internal_3d2cd4a21c691bdf80ed3297a7b14042682264b144448f821bd574fd22a5215f->enter($__internal_3d2cd4a21c691bdf80ed3297a7b14042682264b144448f821bd574fd22a5215f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/predmeti.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_3d2cd4a21c691bdf80ed3297a7b14042682264b144448f821bd574fd22a5215f->leave($__internal_3d2cd4a21c691bdf80ed3297a7b14042682264b144448f821bd574fd22a5215f_prof);

    }

    public function getTemplateName()
    {
        return "Subject/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 38,  140 => 37,  134 => 36,  122 => 29,  107 => 26,  104 => 25,  100 => 24,  93 => 20,  85 => 14,  79 => 13,  68 => 8,  63 => 7,  57 => 6,  48 => 4,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'Subject/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/table_student.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('mentor_studenti') }}">Studenti</a></li>*/
/*     <li class="active"><a href="{{ path('subject_index') }}">Predmeti</a></li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3">*/
/*                 <table class="table">*/
/*                     <thead>*/
/*                     <tr>*/
/*                         <th><a href="{{ path('subject_new') }}">Novi predmet</a> </th>*/
/*                     </tr>*/
/*                     </thead>*/
/*                     <tbody id="lista_predmeta">*/
/*                     {% for subject in subjects %}*/
/*                         <tr>*/
/*                             <td> {{ subject.ime }} ({{ subject.kod }}) <a href="{{ path('subject_show', {'id': subject.id}) }}">Detalji</a> <a href="{{ path('subject_edit', {'id': subject.id}) }}">Uredi</a> </td>*/
/*                         </tr>*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('js/predmeti.js') }}"></script>*/
/* {% endblock %}*/
