<?php

/* FOSUserBundle:Registration:checkEmail.html.twig */
class __TwigTemplate_d4d95578e8b83ffff354aa2f5ae5485bb71fc1603cfd350c14467b5e346ce2e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:checkEmail.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04e7f2ea36cacd2e7c190e1497d26aef667a09e52c08d6af6675acdf33820480 = $this->env->getExtension("native_profiler");
        $__internal_04e7f2ea36cacd2e7c190e1497d26aef667a09e52c08d6af6675acdf33820480->enter($__internal_04e7f2ea36cacd2e7c190e1497d26aef667a09e52c08d6af6675acdf33820480_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_04e7f2ea36cacd2e7c190e1497d26aef667a09e52c08d6af6675acdf33820480->leave($__internal_04e7f2ea36cacd2e7c190e1497d26aef667a09e52c08d6af6675acdf33820480_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b8ebc978cb3ef1f761659e9be5008378aca47ce32df6f410dd878fba4c13836e = $this->env->getExtension("native_profiler");
        $__internal_b8ebc978cb3ef1f761659e9be5008378aca47ce32df6f410dd878fba4c13836e->enter($__internal_b8ebc978cb3ef1f761659e9be5008378aca47ce32df6f410dd878fba4c13836e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_b8ebc978cb3ef1f761659e9be5008378aca47ce32df6f410dd878fba4c13836e->leave($__internal_b8ebc978cb3ef1f761659e9be5008378aca47ce32df6f410dd878fba4c13836e_prof);

    }

    // line 6
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_8cf113c0aee55b18bfdaa312574d8146afff8d38455b50c87b29dafc54b64148 = $this->env->getExtension("native_profiler");
        $__internal_8cf113c0aee55b18bfdaa312574d8146afff8d38455b50c87b29dafc54b64148->enter($__internal_8cf113c0aee55b18bfdaa312574d8146afff8d38455b50c87b29dafc54b64148_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 7
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_8cf113c0aee55b18bfdaa312574d8146afff8d38455b50c87b29dafc54b64148->leave($__internal_8cf113c0aee55b18bfdaa312574d8146afff8d38455b50c87b29dafc54b64148_prof);

    }

    // line 13
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_885a8b1bec78e8e01a2306f2620bee912052bc093ae7e08e629b014861a2ed54 = $this->env->getExtension("native_profiler");
        $__internal_885a8b1bec78e8e01a2306f2620bee912052bc093ae7e08e629b014861a2ed54->enter($__internal_885a8b1bec78e8e01a2306f2620bee912052bc093ae7e08e629b014861a2ed54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 14
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\" style=\"background-color: rgba(255,255,255,0.8)\">
                <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
            </div>
        </div>
    </div>
";
        
        $__internal_885a8b1bec78e8e01a2306f2620bee912052bc093ae7e08e629b014861a2ed54->leave($__internal_885a8b1bec78e8e01a2306f2620bee912052bc093ae7e08e629b014861a2ed54_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 17,  73 => 14,  67 => 13,  58 => 7,  52 => 6,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* {% block stylesheets %}*/
/*     {{  parent() }}*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3" style="background-color: rgba(255,255,255,0.8)">*/
/*                 <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
/* */
