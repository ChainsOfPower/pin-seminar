<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_41de5a5b4ec1a0bd512000accd6d125e37bc9d025abcb6979c7dd5f15e2ea6a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c7d5d95cf814e10889926422457fdcab7a5e0a1a7c7b665d687e652e39e0f2a = $this->env->getExtension("native_profiler");
        $__internal_8c7d5d95cf814e10889926422457fdcab7a5e0a1a7c7b665d687e652e39e0f2a->enter($__internal_8c7d5d95cf814e10889926422457fdcab7a5e0a1a7c7b665d687e652e39e0f2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_8c7d5d95cf814e10889926422457fdcab7a5e0a1a7c7b665d687e652e39e0f2a->leave($__internal_8c7d5d95cf814e10889926422457fdcab7a5e0a1a7c7b665d687e652e39e0f2a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
