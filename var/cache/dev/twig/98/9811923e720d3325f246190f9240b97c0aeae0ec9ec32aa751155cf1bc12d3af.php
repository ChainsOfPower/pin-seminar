<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_7e2d32969caeae44b6bd1c28d0e70a44a9ade387a6d8b3a5cdeee1af79f057e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b6093ec50a78773a9d4708e110c6902bf49a07122afb055e3c06cc7d82457b5b = $this->env->getExtension("native_profiler");
        $__internal_b6093ec50a78773a9d4708e110c6902bf49a07122afb055e3c06cc7d82457b5b->enter($__internal_b6093ec50a78773a9d4708e110c6902bf49a07122afb055e3c06cc7d82457b5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b6093ec50a78773a9d4708e110c6902bf49a07122afb055e3c06cc7d82457b5b->leave($__internal_b6093ec50a78773a9d4708e110c6902bf49a07122afb055e3c06cc7d82457b5b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8c466ed23de114917ca60a9fd1accef024295559858d4e78aa50724a57857134 = $this->env->getExtension("native_profiler");
        $__internal_8c466ed23de114917ca60a9fd1accef024295559858d4e78aa50724a57857134->enter($__internal_8c466ed23de114917ca60a9fd1accef024295559858d4e78aa50724a57857134_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_8c466ed23de114917ca60a9fd1accef024295559858d4e78aa50724a57857134->leave($__internal_8c466ed23de114917ca60a9fd1accef024295559858d4e78aa50724a57857134_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_e33ebbee66fa4b99366ffa148b961d161bfde51bf0d7510207b5dc30eb128b07 = $this->env->getExtension("native_profiler");
        $__internal_e33ebbee66fa4b99366ffa148b961d161bfde51bf0d7510207b5dc30eb128b07->enter($__internal_e33ebbee66fa4b99366ffa148b961d161bfde51bf0d7510207b5dc30eb128b07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_e33ebbee66fa4b99366ffa148b961d161bfde51bf0d7510207b5dc30eb128b07->leave($__internal_e33ebbee66fa4b99366ffa148b961d161bfde51bf0d7510207b5dc30eb128b07_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
