<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_f90119abbebc13cac3474eb76d59908fc1266a3f04ea45fe708f17c9845441e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_297071d70bd3d7e3eda7b903bfc76d7eea9351ad31e84e08448844fd7725ff5d = $this->env->getExtension("native_profiler");
        $__internal_297071d70bd3d7e3eda7b903bfc76d7eea9351ad31e84e08448844fd7725ff5d->enter($__internal_297071d70bd3d7e3eda7b903bfc76d7eea9351ad31e84e08448844fd7725ff5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_297071d70bd3d7e3eda7b903bfc76d7eea9351ad31e84e08448844fd7725ff5d->leave($__internal_297071d70bd3d7e3eda7b903bfc76d7eea9351ad31e84e08448844fd7725ff5d_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
