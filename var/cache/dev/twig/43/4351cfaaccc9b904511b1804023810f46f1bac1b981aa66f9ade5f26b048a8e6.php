<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_4d702525c165b3be84cf008d85b84e683b58eb5fc44af05cd153faefbd8bc7a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc2da755b11c4c793fb1816e02270461c3fa3eb208c32a042b41bc92952750dd = $this->env->getExtension("native_profiler");
        $__internal_dc2da755b11c4c793fb1816e02270461c3fa3eb208c32a042b41bc92952750dd->enter($__internal_dc2da755b11c4c793fb1816e02270461c3fa3eb208c32a042b41bc92952750dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dc2da755b11c4c793fb1816e02270461c3fa3eb208c32a042b41bc92952750dd->leave($__internal_dc2da755b11c4c793fb1816e02270461c3fa3eb208c32a042b41bc92952750dd_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_268800e478639a47c8a9864c356996c228e03dd0315dbecd5228eca4d1fcd0a7 = $this->env->getExtension("native_profiler");
        $__internal_268800e478639a47c8a9864c356996c228e03dd0315dbecd5228eca4d1fcd0a7->enter($__internal_268800e478639a47c8a9864c356996c228e03dd0315dbecd5228eca4d1fcd0a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_268800e478639a47c8a9864c356996c228e03dd0315dbecd5228eca4d1fcd0a7->leave($__internal_268800e478639a47c8a9864c356996c228e03dd0315dbecd5228eca4d1fcd0a7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:new_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
