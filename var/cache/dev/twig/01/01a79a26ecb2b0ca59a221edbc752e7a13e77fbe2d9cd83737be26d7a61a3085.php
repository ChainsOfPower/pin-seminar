<?php

/* :Home:index.html.twig */
class __TwigTemplate_f69aef82dc0cfe2c807b358c854a0913cd32caa5a16e32fd76b0ba655f5269ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Home/layout.html.twig", ":Home:index.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Home/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a44cf59bb1943feb7a835397676fc1846fa9ec4f6bbeaa71db27d6ac58c25b1f = $this->env->getExtension("native_profiler");
        $__internal_a44cf59bb1943feb7a835397676fc1846fa9ec4f6bbeaa71db27d6ac58c25b1f->enter($__internal_a44cf59bb1943feb7a835397676fc1846fa9ec4f6bbeaa71db27d6ac58c25b1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Home:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a44cf59bb1943feb7a835397676fc1846fa9ec4f6bbeaa71db27d6ac58c25b1f->leave($__internal_a44cf59bb1943feb7a835397676fc1846fa9ec4f6bbeaa71db27d6ac58c25b1f_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_729c610fe86dca1be59908851112ff8e56ac850f0e228f62fa07b90af8ce9e4b = $this->env->getExtension("native_profiler");
        $__internal_729c610fe86dca1be59908851112ff8e56ac850f0e228f62fa07b90af8ce9e4b->enter($__internal_729c610fe86dca1be59908851112ff8e56ac850f0e228f62fa07b90af8ce9e4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/index.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    ";
        
        $__internal_729c610fe86dca1be59908851112ff8e56ac850f0e228f62fa07b90af8ce9e4b->leave($__internal_729c610fe86dca1be59908851112ff8e56ac850f0e228f62fa07b90af8ce9e4b_prof);

    }

    // line 6
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_c5336ebda2178d9842104fd08e8d29481abd68486432fe49f422b53d317024c9 = $this->env->getExtension("native_profiler");
        $__internal_c5336ebda2178d9842104fd08e8d29481abd68486432fe49f422b53d317024c9->enter($__internal_c5336ebda2178d9842104fd08e8d29481abd68486432fe49f422b53d317024c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 7
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_c5336ebda2178d9842104fd08e8d29481abd68486432fe49f422b53d317024c9->leave($__internal_c5336ebda2178d9842104fd08e8d29481abd68486432fe49f422b53d317024c9_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_43f6295a55f6149dc4b9d35f770d9be911ae98a545c55832affae1777c3c5f7d = $this->env->getExtension("native_profiler");
        $__internal_43f6295a55f6149dc4b9d35f770d9be911ae98a545c55832affae1777c3c5f7d->enter($__internal_43f6295a55f6149dc4b9d35f770d9be911ae98a545c55832affae1777c3c5f7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\">
                <h2>Web studenata IT odjela za stručne studije</h2>
                <p>
                    Na ovom webu imate pristup svim upisanim i položenim predmetima. <br/>
                    Sve što trebate napraviti je registrirati se i potvrditi registraciju mailom. <br/>
                    Ukoliko ste već registrirani, logirajte se.
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_43f6295a55f6149dc4b9d35f770d9be911ae98a545c55832affae1777c3c5f7d->leave($__internal_43f6295a55f6149dc4b9d35f770d9be911ae98a545c55832affae1777c3c5f7d_prof);

    }

    public function getTemplateName()
    {
        return ":Home:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 12,  71 => 11,  62 => 7,  56 => 6,  47 => 4,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'Home/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/Home/index.css') }}" rel="stylesheet" />*/
/*     {% endblock %}*/
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3">*/
/*                 <h2>Web studenata IT odjela za stručne studije</h2>*/
/*                 <p>*/
/*                     Na ovom webu imate pristup svim upisanim i položenim predmetima. <br/>*/
/*                     Sve što trebate napraviti je registrirati se i potvrditi registraciju mailom. <br/>*/
/*                     Ukoliko ste već registrirani, logirajte se.*/
/*                 </p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
