<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_031db19fbd0cb18129067192180e000c863746f633143d42bc94244f0dcba18c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_009a827808bcb31d9a2c17b5dcfd985fe25fc5e9beb8927ab2788d7e80e4c4ad = $this->env->getExtension("native_profiler");
        $__internal_009a827808bcb31d9a2c17b5dcfd985fe25fc5e9beb8927ab2788d7e80e4c4ad->enter($__internal_009a827808bcb31d9a2c17b5dcfd985fe25fc5e9beb8927ab2788d7e80e4c4ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_009a827808bcb31d9a2c17b5dcfd985fe25fc5e9beb8927ab2788d7e80e4c4ad->leave($__internal_009a827808bcb31d9a2c17b5dcfd985fe25fc5e9beb8927ab2788d7e80e4c4ad_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
