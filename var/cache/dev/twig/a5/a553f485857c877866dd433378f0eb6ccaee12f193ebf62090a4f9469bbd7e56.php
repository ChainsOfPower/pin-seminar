<?php

/* @FOSUser/Resetting/email.txt.twig */
class __TwigTemplate_32c6716995cc3fc22646f0844225b0da959f858ae4fe837dc3290e915f018188 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_392c351006e07d8f3ef245c1828a867c66cf4fb0cbf5a3d62b4d41dc4187749a = $this->env->getExtension("native_profiler");
        $__internal_392c351006e07d8f3ef245c1828a867c66cf4fb0cbf5a3d62b4d41dc4187749a->enter($__internal_392c351006e07d8f3ef245c1828a867c66cf4fb0cbf5a3d62b4d41dc4187749a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_392c351006e07d8f3ef245c1828a867c66cf4fb0cbf5a3d62b4d41dc4187749a->leave($__internal_392c351006e07d8f3ef245c1828a867c66cf4fb0cbf5a3d62b4d41dc4187749a_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_ad9b0c77e47d4c8b8446686da70501011dd84f834c0647b806cbb621549bdfa4 = $this->env->getExtension("native_profiler");
        $__internal_ad9b0c77e47d4c8b8446686da70501011dd84f834c0647b806cbb621549bdfa4->enter($__internal_ad9b0c77e47d4c8b8446686da70501011dd84f834c0647b806cbb621549bdfa4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        echo "
";
        
        $__internal_ad9b0c77e47d4c8b8446686da70501011dd84f834c0647b806cbb621549bdfa4->leave($__internal_ad9b0c77e47d4c8b8446686da70501011dd84f834c0647b806cbb621549bdfa4_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_fd1acc6c74a9c4f6830734d445034b0c444ab7dfdc6a9b5b26d40cf62ab25b7b = $this->env->getExtension("native_profiler");
        $__internal_fd1acc6c74a9c4f6830734d445034b0c444ab7dfdc6a9b5b26d40cf62ab25b7b->enter($__internal_fd1acc6c74a9c4f6830734d445034b0c444ab7dfdc6a9b5b26d40cf62ab25b7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_fd1acc6c74a9c4f6830734d445034b0c444ab7dfdc6a9b5b26d40cf62ab25b7b->leave($__internal_fd1acc6c74a9c4f6830734d445034b0c444ab7dfdc6a9b5b26d40cf62ab25b7b_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_69ff8ccd741baabc95acf314b09bcabb73c1295da7413716f1d6deae359f8381 = $this->env->getExtension("native_profiler");
        $__internal_69ff8ccd741baabc95acf314b09bcabb73c1295da7413716f1d6deae359f8381->enter($__internal_69ff8ccd741baabc95acf314b09bcabb73c1295da7413716f1d6deae359f8381_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_69ff8ccd741baabc95acf314b09bcabb73c1295da7413716f1d6deae359f8381->leave($__internal_69ff8ccd741baabc95acf314b09bcabb73c1295da7413716f1d6deae359f8381_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
