<?php

/* :Subject:layout.html.twig */
class __TwigTemplate_99f06b6d52e9f30c778e496105b55dec38df66210a231cc9091fbb1f85795a79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Subject:layout.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d70013492cd9cf3e384172a44e8ef8d5db154af55843e4adb4cab7aff26ab6a = $this->env->getExtension("native_profiler");
        $__internal_2d70013492cd9cf3e384172a44e8ef8d5db154af55843e4adb4cab7aff26ab6a->enter($__internal_2d70013492cd9cf3e384172a44e8ef8d5db154af55843e4adb4cab7aff26ab6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Subject:layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2d70013492cd9cf3e384172a44e8ef8d5db154af55843e4adb4cab7aff26ab6a->leave($__internal_2d70013492cd9cf3e384172a44e8ef8d5db154af55843e4adb4cab7aff26ab6a_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_72e3f501190d6f1c06d8e189b4f7f9551affb74aef7cc7b01ab633ecce294445 = $this->env->getExtension("native_profiler");
        $__internal_72e3f501190d6f1c06d8e189b4f7f9551affb74aef7cc7b01ab633ecce294445->enter($__internal_72e3f501190d6f1c06d8e189b4f7f9551affb74aef7cc7b01ab633ecce294445_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/nav.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_72e3f501190d6f1c06d8e189b4f7f9551affb74aef7cc7b01ab633ecce294445->leave($__internal_72e3f501190d6f1c06d8e189b4f7f9551affb74aef7cc7b01ab633ecce294445_prof);

    }

    // line 6
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_05eadbe1cb68beacd1560873c94f3e9cc3d4443fdefe89c64a3d0c7150faf050 = $this->env->getExtension("native_profiler");
        $__internal_05eadbe1cb68beacd1560873c94f3e9cc3d4443fdefe89c64a3d0c7150faf050->enter($__internal_05eadbe1cb68beacd1560873c94f3e9cc3d4443fdefe89c64a3d0c7150faf050_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 7
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a id=\"logo\" href=\"/\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\"> OSS Split
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">


                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 26
        $this->displayBlock('navBarLinks', $context, $blocks);
        // line 28
        echo "                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

";
        
        $__internal_05eadbe1cb68beacd1560873c94f3e9cc3d4443fdefe89c64a3d0c7150faf050->leave($__internal_05eadbe1cb68beacd1560873c94f3e9cc3d4443fdefe89c64a3d0c7150faf050_prof);

    }

    // line 26
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_7c1d676aa541ad0e912f378d895d96ac97255a126c8ca4cce1b047b6f2c84c51 = $this->env->getExtension("native_profiler");
        $__internal_7c1d676aa541ad0e912f378d895d96ac97255a126c8ca4cce1b047b6f2c84c51->enter($__internal_7c1d676aa541ad0e912f378d895d96ac97255a126c8ca4cce1b047b6f2c84c51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 27
        echo "                    ";
        
        $__internal_7c1d676aa541ad0e912f378d895d96ac97255a126c8ca4cce1b047b6f2c84c51->leave($__internal_7c1d676aa541ad0e912f378d895d96ac97255a126c8ca4cce1b047b6f2c84c51_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ec9b179abc98a831bf15d0882925dda63a76242bd5f452892678981ad82c6c85 = $this->env->getExtension("native_profiler");
        $__internal_ec9b179abc98a831bf15d0882925dda63a76242bd5f452892678981ad82c6c85->enter($__internal_ec9b179abc98a831bf15d0882925dda63a76242bd5f452892678981ad82c6c85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>
";
        
        $__internal_ec9b179abc98a831bf15d0882925dda63a76242bd5f452892678981ad82c6c85->leave($__internal_ec9b179abc98a831bf15d0882925dda63a76242bd5f452892678981ad82c6c85_prof);

    }

    public function getTemplateName()
    {
        return ":Subject:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 39,  121 => 38,  116 => 37,  110 => 36,  103 => 27,  97 => 26,  85 => 28,  83 => 26,  72 => 18,  59 => 7,  53 => 6,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block stylesheets %}*/
/*     <link href="{{ asset('css/Home/nav.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block navigation %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a id="logo" href="/">*/
/*                     <img src="{{ asset('images/logo.png') }}"> OSS Split*/
/*                 </a>*/
/*             </div>*/
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/*                 <ul class="nav navbar-nav navbar-right">*/
/*                     {% block navBarLinks %}*/
/*                     {% endblock %}*/
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*     </nav>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ parent() }}*/
/*     <script src="{{ asset('bundles/fosjsrouting/js/router.js') }}"></script>*/
/*     <script src="{{ path('fos_js_routing_js', { callback: 'fos.Router.setData' }) }}"></script>*/
/* {% endblock %}*/
