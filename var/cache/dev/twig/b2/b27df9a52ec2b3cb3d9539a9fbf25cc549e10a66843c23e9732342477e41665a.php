<?php

/* @WebProfiler/Icon/forward.svg */
class __TwigTemplate_540fdf786bf2f6b5ca09ea68a05e9c08880c4a8183059e6773a81a14012f4d0f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d13a9f1c136347c41287f8c57d5c85e798045e4360c1ed44605350c20eafbfc = $this->env->getExtension("native_profiler");
        $__internal_7d13a9f1c136347c41287f8c57d5c85e798045e4360c1ed44605350c20eafbfc->enter($__internal_7d13a9f1c136347c41287f8c57d5c85e798045e4360c1ed44605350c20eafbfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        // line 1
        echo "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
";
        
        $__internal_7d13a9f1c136347c41287f8c57d5c85e798045e4360c1ed44605350c20eafbfc->leave($__internal_7d13a9f1c136347c41287f8c57d5c85e798045e4360c1ed44605350c20eafbfc_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/forward.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">*/
/*     <path style="fill:#aaa" d="M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,*/
/*         1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z"/>*/
/* </svg>*/
/* */
