<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_6065915b428c83acc3253e7c8dd4501e5a73ba395ce704195a420144d769d704 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45e3e5bf0c3a5555af57b49e7b7bb4738a431eb15c4f1705eb11ab8b75bb09dc = $this->env->getExtension("native_profiler");
        $__internal_45e3e5bf0c3a5555af57b49e7b7bb4738a431eb15c4f1705eb11ab8b75bb09dc->enter($__internal_45e3e5bf0c3a5555af57b49e7b7bb4738a431eb15c4f1705eb11ab8b75bb09dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_45e3e5bf0c3a5555af57b49e7b7bb4738a431eb15c4f1705eb11ab8b75bb09dc->leave($__internal_45e3e5bf0c3a5555af57b49e7b7bb4738a431eb15c4f1705eb11ab8b75bb09dc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 4,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="fos_user_group_show">*/
/*     <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>*/
/* </div>*/
/* */
