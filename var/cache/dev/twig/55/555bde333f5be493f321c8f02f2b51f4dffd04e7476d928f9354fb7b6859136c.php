<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_6d7fa99a45ab5dbd3c2fff851e38063055aeef0e387c64740a282f8d6d029555 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3e7d8e338aed929cae9e761708440fed8e9bd254b6641476e8929f3f59bcb17b = $this->env->getExtension("native_profiler");
        $__internal_3e7d8e338aed929cae9e761708440fed8e9bd254b6641476e8929f3f59bcb17b->enter($__internal_3e7d8e338aed929cae9e761708440fed8e9bd254b6641476e8929f3f59bcb17b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3e7d8e338aed929cae9e761708440fed8e9bd254b6641476e8929f3f59bcb17b->leave($__internal_3e7d8e338aed929cae9e761708440fed8e9bd254b6641476e8929f3f59bcb17b_prof);

    }

    // line 5
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_19197b8898480f9ea55698c3007ff848202e757d2b561ac1e2aaecef4510be63 = $this->env->getExtension("native_profiler");
        $__internal_19197b8898480f9ea55698c3007ff848202e757d2b561ac1e2aaecef4510be63->enter($__internal_19197b8898480f9ea55698c3007ff848202e757d2b561ac1e2aaecef4510be63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("student_homepage");
        echo "\">Upisni list</a> </li>
    <li><a href=\"/profile/change-password\">Promijeni lozinku</a></li>
    <li><a href=\"/logout\">Odjavi se</a></li>
";
        
        $__internal_19197b8898480f9ea55698c3007ff848202e757d2b561ac1e2aaecef4510be63->leave($__internal_19197b8898480f9ea55698c3007ff848202e757d2b561ac1e2aaecef4510be63_prof);

    }

    // line 11
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5eb06dc8fec029bfe1d1a850c02056139225e2ac423475bf7616153eff77b20b = $this->env->getExtension("native_profiler");
        $__internal_5eb06dc8fec029bfe1d1a850c02056139225e2ac423475bf7616153eff77b20b->enter($__internal_5eb06dc8fec029bfe1d1a850c02056139225e2ac423475bf7616153eff77b20b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 12
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\" style=\"background-color: rgba(255,255,255,0.8)\">
                <p>";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
                ";
        // line 16
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 17
            echo "                    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
                ";
        }
        // line 19
        echo "            </div>
        </div>
    </div>
";
        
        $__internal_5eb06dc8fec029bfe1d1a850c02056139225e2ac423475bf7616153eff77b20b->leave($__internal_5eb06dc8fec029bfe1d1a850c02056139225e2ac423475bf7616153eff77b20b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 19,  70 => 17,  68 => 16,  64 => 15,  59 => 12,  53 => 11,  41 => 6,  35 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="{{ path('student_homepage') }}">Upisni list</a> </li>*/
/*     <li><a href="/profile/change-password">Promijeni lozinku</a></li>*/
/*     <li><a href="/logout">Odjavi se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3" style="background-color: rgba(255,255,255,0.8)">*/
/*                 <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>*/
/*                 {% if targetUrl %}*/
/*                     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*                 {% endif %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
/* */
