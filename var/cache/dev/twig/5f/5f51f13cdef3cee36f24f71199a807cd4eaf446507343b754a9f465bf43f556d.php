<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_bafd6fb2af7f5240f3a875efa29706b11a6e4fd661caddbf2e8e94e0acccb8bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1327311d147297ac57a989f2f1c7c7f391e79562b79d369b1261d73b3b9a70da = $this->env->getExtension("native_profiler");
        $__internal_1327311d147297ac57a989f2f1c7c7f391e79562b79d369b1261d73b3b9a70da->enter($__internal_1327311d147297ac57a989f2f1c7c7f391e79562b79d369b1261d73b3b9a70da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_1327311d147297ac57a989f2f1c7c7f391e79562b79d369b1261d73b3b9a70da->leave($__internal_1327311d147297ac57a989f2f1c7c7f391e79562b79d369b1261d73b3b9a70da_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
