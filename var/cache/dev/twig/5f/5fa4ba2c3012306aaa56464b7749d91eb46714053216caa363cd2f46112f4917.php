<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_f0a2f48d4971863e923af09fb0a4de5e8d7e21b5ea0aff72f2e5dc7fb9032f22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_38652d0d6b69e214f16b5b0153305b192fa7f3c4cf68508e26b4cd12b7c9acf3 = $this->env->getExtension("native_profiler");
        $__internal_38652d0d6b69e214f16b5b0153305b192fa7f3c4cf68508e26b4cd12b7c9acf3->enter($__internal_38652d0d6b69e214f16b5b0153305b192fa7f3c4cf68508e26b4cd12b7c9acf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_38652d0d6b69e214f16b5b0153305b192fa7f3c4cf68508e26b4cd12b7c9acf3->leave($__internal_38652d0d6b69e214f16b5b0153305b192fa7f3c4cf68508e26b4cd12b7c9acf3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
