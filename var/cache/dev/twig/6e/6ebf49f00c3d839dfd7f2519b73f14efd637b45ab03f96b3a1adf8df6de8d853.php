<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_d61b86482f4576ed875d7c7c3f651bfa1abac9cb0c1d4f327e153f2155109875 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2bb9f18966d2be606f24e44c17a9e4f2e09f0b8f6f77bce51a764fece78e10b1 = $this->env->getExtension("native_profiler");
        $__internal_2bb9f18966d2be606f24e44c17a9e4f2e09f0b8f6f77bce51a764fece78e10b1->enter($__internal_2bb9f18966d2be606f24e44c17a9e4f2e09f0b8f6f77bce51a764fece78e10b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_2bb9f18966d2be606f24e44c17a9e4f2e09f0b8f6f77bce51a764fece78e10b1->leave($__internal_2bb9f18966d2be606f24e44c17a9e4f2e09f0b8f6f77bce51a764fece78e10b1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
