<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_0cb6afbeabcda19f2abec231a575b584e214e15b2258a208ac88b95ff008f7a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80e805bc0110f9341b725a31562448529e33020f4f176071d5bf829d6bf071d3 = $this->env->getExtension("native_profiler");
        $__internal_80e805bc0110f9341b725a31562448529e33020f4f176071d5bf829d6bf071d3->enter($__internal_80e805bc0110f9341b725a31562448529e33020f4f176071d5bf829d6bf071d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_80e805bc0110f9341b725a31562448529e33020f4f176071d5bf829d6bf071d3->leave($__internal_80e805bc0110f9341b725a31562448529e33020f4f176071d5bf829d6bf071d3_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
