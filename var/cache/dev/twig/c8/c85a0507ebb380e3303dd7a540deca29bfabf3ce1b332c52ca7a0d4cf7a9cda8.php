<?php

/* @FOSUser/Resetting/request.html.twig */
class __TwigTemplate_650821b5c1033123b84ca81e1264272e13efbc4f4645e96764b8b8e1eb2101aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Resetting/request.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_efb9ce83b6070e24e5d554a05e905e5e3bf6b65a1e007bebd57760337ec5b0e3 = $this->env->getExtension("native_profiler");
        $__internal_efb9ce83b6070e24e5d554a05e905e5e3bf6b65a1e007bebd57760337ec5b0e3->enter($__internal_efb9ce83b6070e24e5d554a05e905e5e3bf6b65a1e007bebd57760337ec5b0e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_efb9ce83b6070e24e5d554a05e905e5e3bf6b65a1e007bebd57760337ec5b0e3->leave($__internal_efb9ce83b6070e24e5d554a05e905e5e3bf6b65a1e007bebd57760337ec5b0e3_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a23350fc5f2c7b2c4a14517b85b23a12cb33d31eb70a59dd293e70545e4a66e2 = $this->env->getExtension("native_profiler");
        $__internal_a23350fc5f2c7b2c4a14517b85b23a12cb33d31eb70a59dd293e70545e4a66e2->enter($__internal_a23350fc5f2c7b2c4a14517b85b23a12cb33d31eb70a59dd293e70545e4a66e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/centerForm.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_a23350fc5f2c7b2c4a14517b85b23a12cb33d31eb70a59dd293e70545e4a66e2->leave($__internal_a23350fc5f2c7b2c4a14517b85b23a12cb33d31eb70a59dd293e70545e4a66e2_prof);

    }

    // line 8
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_cb18e06def9864a1461cf67d4f96e5d3206e06dcf488a00562920585afa93dac = $this->env->getExtension("native_profiler");
        $__internal_cb18e06def9864a1461cf67d4f96e5d3206e06dcf488a00562920585afa93dac->enter($__internal_cb18e06def9864a1461cf67d4f96e5d3206e06dcf488a00562920585afa93dac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 9
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_cb18e06def9864a1461cf67d4f96e5d3206e06dcf488a00562920585afa93dac->leave($__internal_cb18e06def9864a1461cf67d4f96e5d3206e06dcf488a00562920585afa93dac_prof);

    }

    // line 13
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_954f874c2145378b16d52e9152283cd32c9e858960b108262741c68848c1beda = $this->env->getExtension("native_profiler");
        $__internal_954f874c2145378b16d52e9152283cd32c9e858960b108262741c68848c1beda->enter($__internal_954f874c2145378b16d52e9152283cd32c9e858960b108262741c68848c1beda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 14
        echo "    <h2><center>Resetiraj šifru</center></h2>
";
        // line 15
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "@FOSUser/Resetting/request.html.twig", 15)->display($context);
        
        $__internal_954f874c2145378b16d52e9152283cd32c9e858960b108262741c68848c1beda->leave($__internal_954f874c2145378b16d52e9152283cd32c9e858960b108262741c68848c1beda_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 15,  77 => 14,  71 => 13,  62 => 9,  56 => 8,  47 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/centerForm.css') }}" rel="stylesheet">*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block fos_user_content %}*/
/*     <h2><center>Resetiraj šifru</center></h2>*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
