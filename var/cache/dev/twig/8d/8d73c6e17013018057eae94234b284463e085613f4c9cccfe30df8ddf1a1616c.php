<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_35120ab71f61c0d1965d0670fc59b9f7e52369e98926cacf23b97e824909f366 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26af12202a1d2051e36353d653beec995f2f676bdabc48bec3fe7adf2004b4cc = $this->env->getExtension("native_profiler");
        $__internal_26af12202a1d2051e36353d653beec995f2f676bdabc48bec3fe7adf2004b4cc->enter($__internal_26af12202a1d2051e36353d653beec995f2f676bdabc48bec3fe7adf2004b4cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_26af12202a1d2051e36353d653beec995f2f676bdabc48bec3fe7adf2004b4cc->leave($__internal_26af12202a1d2051e36353d653beec995f2f676bdabc48bec3fe7adf2004b4cc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
