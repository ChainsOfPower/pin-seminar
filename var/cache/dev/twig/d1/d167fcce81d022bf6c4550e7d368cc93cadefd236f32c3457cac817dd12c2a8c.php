<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_9f0135f166bd83abf32468083d5e141edb8bc42dac31476e95d88c5d7f651c2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01602a32669fa2703dd70cbe57bb7dae23a629e83e027c8568fd8e5866f2d89b = $this->env->getExtension("native_profiler");
        $__internal_01602a32669fa2703dd70cbe57bb7dae23a629e83e027c8568fd8e5866f2d89b->enter($__internal_01602a32669fa2703dd70cbe57bb7dae23a629e83e027c8568fd8e5866f2d89b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_01602a32669fa2703dd70cbe57bb7dae23a629e83e027c8568fd8e5866f2d89b->leave($__internal_01602a32669fa2703dd70cbe57bb7dae23a629e83e027c8568fd8e5866f2d89b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
