<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_407d4faae41f046e0641432b0be5fae838d9725510da2cf5b4e914e77d93001c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0fa12f255aa709802dffd2c3d6fe1c87e61a6cbfd54e9a6fdacddd03a83f5f2 = $this->env->getExtension("native_profiler");
        $__internal_e0fa12f255aa709802dffd2c3d6fe1c87e61a6cbfd54e9a6fdacddd03a83f5f2->enter($__internal_e0fa12f255aa709802dffd2c3d6fe1c87e61a6cbfd54e9a6fdacddd03a83f5f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_e0fa12f255aa709802dffd2c3d6fe1c87e61a6cbfd54e9a6fdacddd03a83f5f2->leave($__internal_e0fa12f255aa709802dffd2c3d6fe1c87e61a6cbfd54e9a6fdacddd03a83f5f2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
