<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_3c35b219e7ce74b14726369eed698ab7cf09d4ff3f613726bd9e573f0ffc109b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fcc3d9b9407931d6c49967e27112bdf074f5628b5e698374e4f1f204ba2f1adf = $this->env->getExtension("native_profiler");
        $__internal_fcc3d9b9407931d6c49967e27112bdf074f5628b5e698374e4f1f204ba2f1adf->enter($__internal_fcc3d9b9407931d6c49967e27112bdf074f5628b5e698374e4f1f204ba2f1adf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_fcc3d9b9407931d6c49967e27112bdf074f5628b5e698374e4f1f204ba2f1adf->leave($__internal_fcc3d9b9407931d6c49967e27112bdf074f5628b5e698374e4f1f204ba2f1adf_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
