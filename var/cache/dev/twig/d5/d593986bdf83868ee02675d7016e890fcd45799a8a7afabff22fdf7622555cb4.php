<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_f06c7c98e60d2b32d7ef212f5e8c03e69c801154f63924f7fe91c2f219338e39 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef8db6155dd1ad96b2b1ea0b3ee167ae5223c39d28fd3fc83628efd109b39a42 = $this->env->getExtension("native_profiler");
        $__internal_ef8db6155dd1ad96b2b1ea0b3ee167ae5223c39d28fd3fc83628efd109b39a42->enter($__internal_ef8db6155dd1ad96b2b1ea0b3ee167ae5223c39d28fd3fc83628efd109b39a42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_ef8db6155dd1ad96b2b1ea0b3ee167ae5223c39d28fd3fc83628efd109b39a42->leave($__internal_ef8db6155dd1ad96b2b1ea0b3ee167ae5223c39d28fd3fc83628efd109b39a42_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
