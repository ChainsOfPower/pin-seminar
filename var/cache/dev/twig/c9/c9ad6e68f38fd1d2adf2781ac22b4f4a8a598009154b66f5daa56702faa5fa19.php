<?php

/* Home/index.html.twig */
class __TwigTemplate_3ffd8a257adad821147f942a13f07c68edb69097c2d08ad70464809c8605a820 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Home/layout.html.twig", "Home/index.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Home/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d12837151de82aa6b69ef3218826f0059354d2f88b323306df610d061a1b1cd = $this->env->getExtension("native_profiler");
        $__internal_5d12837151de82aa6b69ef3218826f0059354d2f88b323306df610d061a1b1cd->enter($__internal_5d12837151de82aa6b69ef3218826f0059354d2f88b323306df610d061a1b1cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Home/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5d12837151de82aa6b69ef3218826f0059354d2f88b323306df610d061a1b1cd->leave($__internal_5d12837151de82aa6b69ef3218826f0059354d2f88b323306df610d061a1b1cd_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_92a8a97a0b8dcdb71fde775c69beaeab6a09cf20bef526b39752dc1be5baa4fa = $this->env->getExtension("native_profiler");
        $__internal_92a8a97a0b8dcdb71fde775c69beaeab6a09cf20bef526b39752dc1be5baa4fa->enter($__internal_92a8a97a0b8dcdb71fde775c69beaeab6a09cf20bef526b39752dc1be5baa4fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/Home/index.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
";
        
        $__internal_92a8a97a0b8dcdb71fde775c69beaeab6a09cf20bef526b39752dc1be5baa4fa->leave($__internal_92a8a97a0b8dcdb71fde775c69beaeab6a09cf20bef526b39752dc1be5baa4fa_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_709d86d054070b3363af94b1691366c4e4c8e5877de888f06998256ec3446c43 = $this->env->getExtension("native_profiler");
        $__internal_709d86d054070b3363af94b1691366c4e4c8e5877de888f06998256ec3446c43->enter($__internal_709d86d054070b3363af94b1691366c4e4c8e5877de888f06998256ec3446c43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    Naslovna
";
        
        $__internal_709d86d054070b3363af94b1691366c4e4c8e5877de888f06998256ec3446c43->leave($__internal_709d86d054070b3363af94b1691366c4e4c8e5877de888f06998256ec3446c43_prof);

    }

    // line 11
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_5f3ccd8eca6c4a719dd083d9ff69e915a8ccc6e621d3ec667ca04cc5ef85cda9 = $this->env->getExtension("native_profiler");
        $__internal_5f3ccd8eca6c4a719dd083d9ff69e915a8ccc6e621d3ec667ca04cc5ef85cda9->enter($__internal_5f3ccd8eca6c4a719dd083d9ff69e915a8ccc6e621d3ec667ca04cc5ef85cda9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 12
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_5f3ccd8eca6c4a719dd083d9ff69e915a8ccc6e621d3ec667ca04cc5ef85cda9->leave($__internal_5f3ccd8eca6c4a719dd083d9ff69e915a8ccc6e621d3ec667ca04cc5ef85cda9_prof);

    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        $__internal_615b520478c750d2ecf4a84d2affca239c508c871d9f91f4567da7ce27c2b2b2 = $this->env->getExtension("native_profiler");
        $__internal_615b520478c750d2ecf4a84d2affca239c508c871d9f91f4567da7ce27c2b2b2->enter($__internal_615b520478c750d2ecf4a84d2affca239c508c871d9f91f4567da7ce27c2b2b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 17
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\">
                <h2>Web studenata IT odjela za stručne studije</h2>
                <p>
                    Na ovom webu imate pristup svim upisanim i položenim predmetima. <br/>
                    Sve što trebate napraviti je registrirati se i potvrditi registraciju mailom. <br/>
                    Ukoliko ste već registrirani, logirajte se.
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_615b520478c750d2ecf4a84d2affca239c508c871d9f91f4567da7ce27c2b2b2->leave($__internal_615b520478c750d2ecf4a84d2affca239c508c871d9f91f4567da7ce27c2b2b2_prof);

    }

    public function getTemplateName()
    {
        return "Home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 17,  86 => 16,  77 => 12,  71 => 11,  63 => 8,  57 => 7,  48 => 4,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends 'Home/layout.html.twig' %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('css/Home/index.css') }}" rel="stylesheet"/>*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     Naslovna*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3">*/
/*                 <h2>Web studenata IT odjela za stručne studije</h2>*/
/*                 <p>*/
/*                     Na ovom webu imate pristup svim upisanim i položenim predmetima. <br/>*/
/*                     Sve što trebate napraviti je registrirati se i potvrditi registraciju mailom. <br/>*/
/*                     Ukoliko ste već registrirani, logirajte se.*/
/*                 </p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
