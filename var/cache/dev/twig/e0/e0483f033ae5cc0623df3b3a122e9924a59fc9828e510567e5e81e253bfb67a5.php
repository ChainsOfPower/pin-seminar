<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_e5fc65b0865cb0a0bb0f444f34cdf6022d5d1104074079e0791b99749d392183 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7526a3c594cb02888f79fe1869702da51e8df8e57f220e10c4121b0da5555079 = $this->env->getExtension("native_profiler");
        $__internal_7526a3c594cb02888f79fe1869702da51e8df8e57f220e10c4121b0da5555079->enter($__internal_7526a3c594cb02888f79fe1869702da51e8df8e57f220e10c4121b0da5555079_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_7526a3c594cb02888f79fe1869702da51e8df8e57f220e10c4121b0da5555079->leave($__internal_7526a3c594cb02888f79fe1869702da51e8df8e57f220e10c4121b0da5555079_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
