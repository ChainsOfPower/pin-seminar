<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_62ef0c6f8732ee8229ad4bd1647b316332ca4747dfff04aff2a608280f61dfd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_842cbb0953624724e8127f41c3765414ec516f12ee20bc6bce61df89d58710b8 = $this->env->getExtension("native_profiler");
        $__internal_842cbb0953624724e8127f41c3765414ec516f12ee20bc6bce61df89d58710b8->enter($__internal_842cbb0953624724e8127f41c3765414ec516f12ee20bc6bce61df89d58710b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_842cbb0953624724e8127f41c3765414ec516f12ee20bc6bce61df89d58710b8->leave($__internal_842cbb0953624724e8127f41c3765414ec516f12ee20bc6bce61df89d58710b8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
