<?php

/* @FOSUser/Group/new.html.twig */
class __TwigTemplate_8539f3be99bcdf378feb66824ce7093e99166f9d94706c4f0acc960245cc2347 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Group/new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba23bdea186be67bc8590f1a3ff3c1a09ab37caf14031f07f46aa3380582a14f = $this->env->getExtension("native_profiler");
        $__internal_ba23bdea186be67bc8590f1a3ff3c1a09ab37caf14031f07f46aa3380582a14f->enter($__internal_ba23bdea186be67bc8590f1a3ff3c1a09ab37caf14031f07f46aa3380582a14f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ba23bdea186be67bc8590f1a3ff3c1a09ab37caf14031f07f46aa3380582a14f->leave($__internal_ba23bdea186be67bc8590f1a3ff3c1a09ab37caf14031f07f46aa3380582a14f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d49b92eb10e86bff2f502164c813c1b8a5efc491227fbaaf1aab2238d509eccc = $this->env->getExtension("native_profiler");
        $__internal_d49b92eb10e86bff2f502164c813c1b8a5efc491227fbaaf1aab2238d509eccc->enter($__internal_d49b92eb10e86bff2f502164c813c1b8a5efc491227fbaaf1aab2238d509eccc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:new_content.html.twig", "@FOSUser/Group/new.html.twig", 4)->display($context);
        
        $__internal_d49b92eb10e86bff2f502164c813c1b8a5efc491227fbaaf1aab2238d509eccc->leave($__internal_d49b92eb10e86bff2f502164c813c1b8a5efc491227fbaaf1aab2238d509eccc_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:new_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
