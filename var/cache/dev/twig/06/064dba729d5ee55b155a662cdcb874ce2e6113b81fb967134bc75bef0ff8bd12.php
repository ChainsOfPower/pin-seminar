<?php

/* FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig */
class __TwigTemplate_4f4bd4c68f90d4e5234968d4690837caba323893b329771bbab49298edf6ac96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'navBarLinks' => array($this, 'block_navBarLinks'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cdda8d99576260a4e4f1778639808a4186a436d207d4dcae53fbd536c883d4e1 = $this->env->getExtension("native_profiler");
        $__internal_cdda8d99576260a4e4f1778639808a4186a436d207d4dcae53fbd536c883d4e1->enter($__internal_cdda8d99576260a4e4f1778639808a4186a436d207d4dcae53fbd536c883d4e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cdda8d99576260a4e4f1778639808a4186a436d207d4dcae53fbd536c883d4e1->leave($__internal_cdda8d99576260a4e4f1778639808a4186a436d207d4dcae53fbd536c883d4e1_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_823b223b3dc362f69ffa949fd6f30041476a1ddd1f4636ad5282111e925f5874 = $this->env->getExtension("native_profiler");
        $__internal_823b223b3dc362f69ffa949fd6f30041476a1ddd1f4636ad5282111e925f5874->enter($__internal_823b223b3dc362f69ffa949fd6f30041476a1ddd1f4636ad5282111e925f5874_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_823b223b3dc362f69ffa949fd6f30041476a1ddd1f4636ad5282111e925f5874->leave($__internal_823b223b3dc362f69ffa949fd6f30041476a1ddd1f4636ad5282111e925f5874_prof);

    }

    // line 7
    public function block_navBarLinks($context, array $blocks = array())
    {
        $__internal_73f03635a70e71070ef3bfc05a22c9dd15bbacc073bd9fa12c4919e24336cd7d = $this->env->getExtension("native_profiler");
        $__internal_73f03635a70e71070ef3bfc05a22c9dd15bbacc073bd9fa12c4919e24336cd7d->enter($__internal_73f03635a70e71070ef3bfc05a22c9dd15bbacc073bd9fa12c4919e24336cd7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navBarLinks"));

        // line 8
        echo "    <li><a href=\"/login\">Logiraj se</a></li>
    <li><a href=\"/register\">Registriraj se</a></li>
";
        
        $__internal_73f03635a70e71070ef3bfc05a22c9dd15bbacc073bd9fa12c4919e24336cd7d->leave($__internal_73f03635a70e71070ef3bfc05a22c9dd15bbacc073bd9fa12c4919e24336cd7d_prof);

    }

    // line 14
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b3ac541228dbc5c0e025037e14ededc47b622a10ea4fad280531249299865bd9 = $this->env->getExtension("native_profiler");
        $__internal_b3ac541228dbc5c0e025037e14ededc47b622a10ea4fad280531249299865bd9->enter($__internal_b3ac541228dbc5c0e025037e14ededc47b622a10ea4fad280531249299865bd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 15
        echo "    <div class=\"container-fluid\">
        <div class=\"row pagination-centered\">
            <div class=\"col-md-6 col-md-offset-3\" style=\"background-color: rgba(255,255,255,0.8)\">
                <p>";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.password_already_requested", array(), "FOSUserBundle"), "html", null, true);
        echo "</p>
            </div>
        </div>
    </div>
";
        
        $__internal_b3ac541228dbc5c0e025037e14ededc47b622a10ea4fad280531249299865bd9->leave($__internal_b3ac541228dbc5c0e025037e14ededc47b622a10ea4fad280531249299865bd9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 18,  73 => 15,  67 => 14,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block stylesheets %}*/
/*     {{  parent() }}*/
/* {% endblock %}*/
/* */
/* {% block navBarLinks %}*/
/*     <li><a href="/login">Logiraj se</a></li>*/
/*     <li><a href="/register">Registriraj se</a></li>*/
/* {% endblock %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="container-fluid">*/
/*         <div class="row pagination-centered">*/
/*             <div class="col-md-6 col-md-offset-3" style="background-color: rgba(255,255,255,0.8)">*/
/*                 <p>{{ 'resetting.password_already_requested'|trans }}</p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock fos_user_content %}*/
/* */
/* */
/* */
/* */
/* */
/* */
