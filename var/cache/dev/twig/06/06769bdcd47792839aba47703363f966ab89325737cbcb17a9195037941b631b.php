<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_b69c9e7fad16ba788a46c2123b3e558461b4007b2e3d865acdde6a9e1ddaae84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de191de174454d63a00b7f7b913c9b8e0c60185611dfc43c8c20c356833fa2e6 = $this->env->getExtension("native_profiler");
        $__internal_de191de174454d63a00b7f7b913c9b8e0c60185611dfc43c8c20c356833fa2e6->enter($__internal_de191de174454d63a00b7f7b913c9b8e0c60185611dfc43c8c20c356833fa2e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_de191de174454d63a00b7f7b913c9b8e0c60185611dfc43c8c20c356833fa2e6->leave($__internal_de191de174454d63a00b7f7b913c9b8e0c60185611dfc43c8c20c356833fa2e6_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_6abc3fd05a7927e71d908911a032d630dc8198369a4921a10bf897189c5f1f2b = $this->env->getExtension("native_profiler");
        $__internal_6abc3fd05a7927e71d908911a032d630dc8198369a4921a10bf897189c5f1f2b->enter($__internal_6abc3fd05a7927e71d908911a032d630dc8198369a4921a10bf897189c5f1f2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_6abc3fd05a7927e71d908911a032d630dc8198369a4921a10bf897189c5f1f2b->leave($__internal_6abc3fd05a7927e71d908911a032d630dc8198369a4921a10bf897189c5f1f2b_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3d5ce2e77a01fa2b993d3ac50e562cdc2eeced0760a79aee24bac17d678e7bd9 = $this->env->getExtension("native_profiler");
        $__internal_3d5ce2e77a01fa2b993d3ac50e562cdc2eeced0760a79aee24bac17d678e7bd9->enter($__internal_3d5ce2e77a01fa2b993d3ac50e562cdc2eeced0760a79aee24bac17d678e7bd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_3d5ce2e77a01fa2b993d3ac50e562cdc2eeced0760a79aee24bac17d678e7bd9->leave($__internal_3d5ce2e77a01fa2b993d3ac50e562cdc2eeced0760a79aee24bac17d678e7bd9_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
