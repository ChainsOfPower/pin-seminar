<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_dbb59f55ea14ded0cdf374b8b168862b3b61f540fa2ee726df416e30a9036e4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e193d7bd588e5c5da41e65d327932dfca5ea4b450f2a0d601e91e4cc6a62736b = $this->env->getExtension("native_profiler");
        $__internal_e193d7bd588e5c5da41e65d327932dfca5ea4b450f2a0d601e91e4cc6a62736b->enter($__internal_e193d7bd588e5c5da41e65d327932dfca5ea4b450f2a0d601e91e4cc6a62736b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_e193d7bd588e5c5da41e65d327932dfca5ea4b450f2a0d601e91e4cc6a62736b->leave($__internal_e193d7bd588e5c5da41e65d327932dfca5ea4b450f2a0d601e91e4cc6a62736b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
