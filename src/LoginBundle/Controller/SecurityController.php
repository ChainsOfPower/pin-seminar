<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 30.6.2016.
 * Time: 2:45
 */

namespace LoginBundle\Controller;

use FOS\UserBundle\Model\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class SecurityController extends BaseController
{


    public function loginAction(Request $request)
    {


        if ($this->get('security.authorization_checker')->isGranted('ROLE_STUDENT')) {

            return new RedirectResponse($this->get('router')->generate('student_homepage'));
        } else if ($this->get('security.authorization_checker')->isGranted('ROLE_MENTOR')) {
            return new RedirectResponse($this->get('router')->generate('mentor_studenti'));
        }

        return parent::loginAction($request); // TODO: Change the autogenerated stub
    }
}