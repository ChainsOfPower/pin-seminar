<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 3.7.2016.
 * Time: 21:51
 */

namespace LoginBundle\Controller;

use FOS\UserBundle\Model\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ResettingController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class ResettingController extends BaseController
{
    public function requestAction()
    {
        if($this->get('security.authorization_checker')->isGranted('ROLE_STUDENT'))
        {
            return new RedirectResponse($this->get('router')->generate('student_homepage'));
        } else if ($this->get('security.authorization_checker')->isGranted('ROLE_MENTOR')) {
            return new RedirectResponse($this->get('router')->generate('mentor_homepage'));
        }

        return parent::requestAction();
    }
}