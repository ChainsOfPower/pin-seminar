<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 30.6.2016.
 * Time: 2:43
 */

namespace LoginBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class LoginBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}