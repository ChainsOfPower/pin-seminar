<?php

namespace AppBundle\Repository;
use FOS\UserBundle\Model\User;

/**
 * SubjectRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SubjectRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllFreeSubjectsForStudent($studentId)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

       $notin = $qb->select('IDENTITY(pni.predmet)')
            ->from('AppBundle:Upisi', 'pni')
            ->where($qb->expr()->eq('pni.student', $studentId))
            ->getQuery()
            ->getResult();

        $predmeti = null;
        if(count($notin) == 0)
        {
            $predmeti = $this->findAll();
        }
        else
        {
            $str = "";
            foreach($notin as $id)
            {
                $str = $str.$id[1].",";
            }

            $str=$str."0";

            $predmeti = $qb->select('p')
                ->from('AppBundle:Subject', 'p')
                ->where($qb->expr()->notIn('p.id', $str))
                ->getQuery()
                ->getResult();
        }

        return $predmeti;
    }


}
