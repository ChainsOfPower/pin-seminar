<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 26.6.2016.
 * Time: 3:35
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UpisiRepository")
 * @ORM\Table(name="upisi")
 */
class Upisi
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="listovi")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $student;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Subject", inversedBy="listovi")
     * @ORM\JoinColumn(name="predmet_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $predmet;


    /**
     * @ORM\Column(name="status", type="string", columnDefinition="enum('upisano', 'polozeno')")
     * * @Assert\Choice(
     *     choices={ "upisano", "položeno" },
     *     message="Odaber mora biti upisano ili položeno")
     */
    private $status;



    public function getStudent()
    {
        return $this->student;
    }

    public function setStudent($student)
    {
        $this->student = $student;
        return $this;
    }

    public function getPredmet()
    {
        return $this->predmet;
    }

    public function setPredmet($predmet)
    {
        $this->predmet = $predmet;
        return $this;
    }




    /**
     * Set status
     *
     * @param string $status
     *
     * @return Upisi
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

}
