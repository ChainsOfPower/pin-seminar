<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Subject
 *
 * @ORM\Table(name="subject")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubjectRepository")
 * @UniqueEntity("ime", message="Predmet s ovim imenom već postoji")
 * @UniqueEntity("kod", message="Predmet s ovim kodom već postoji")
 */
class Subject
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ime", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Morate upisati ime")
     */
    private $ime;

    /**
     * @var string
     *
     * @ORM\Column(name="kod", type="string", length=16, unique=true)
     * @Assert\NotBlank(message="Morate upisati kod predmeta")
     */
    private $kod;

    /**
     * @var string
     *
     * @ORM\Column(name="program", type="text", nullable=true)
     */
    private $program;

    /**
     * @var int
     *
     * @ORM\Column(name="bodovi", type="integer")
     * @Assert\NotBlank(message="Morate upisati bodove predmeta")
     * @Assert\Type(type="integer",
     *     message="Bodovi moraju biti broj")
     * @Assert\Range(
     *     min=2,
     *     max=12,
     *     minMessage="Najmanji mogući broj ECTS je 2",
     *     maxMessage="Najveći mogući broj ECTS je 12"
     * )
     */
    private $bodovi;

    /**
     * @var int
     *
     * @ORM\Column(name="sem_redovni", type="integer")
     * @Assert\Type(type="integer",
     *     message="Semestar za redovnog studenta mora biti broj u rasponu od 1 do 6")
     * @Assert\Range(
     *     min=1,
     *     max=6,
     *     minMessage="Semestar ne može biti manji od 1",
     *     maxMessage="Semestar ne može biti veći od 6"
     * )
     */
    private $semRedovni;

    /**
     * @var int
     *
     * @ORM\Column(name="sem_izvanredni", type="integer")
     * @Assert\Type(type="integer",
     *     message="Semestar za izvanrednog mora biti broj u rasponu od 1 do 8")
     * @Assert\Range(
     *     min=1,
     *     max=8,
     *     minMessage="Semestar ne može biti manji od 1",
     *     maxMessage="Semestar ne može biti veći od 8"
     * )
     */
    private $semIzvanredni;


    /**
     * @ORM\OneToMany(targetEntity="Upisi", mappedBy="predmet", cascade={"all"})
     */
    private $listovi;

    /**
     * @ORM\Column(name="izborni", type="string", columnDefinition="enum('da', 'ne')")
     * @Assert\Choice(
     *     choices={ "da", "ne" },
     *     message="Odabir mora biti da ili ne"
     * )
     */
    private $izborni;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ime
     *
     * @param string $ime
     *
     * @return Subject
     */
    public function setIme($ime)
    {
        $this->ime = $ime;

        return $this;
    }

    /**
     * Get ime
     *
     * @return string
     */
    public function getIme()
    {
        return $this->ime;
    }

    /**
     * Set kod
     *
     * @param string $kod
     *
     * @return Subject
     */
    public function setKod($kod)
    {
        $this->kod = $kod;

        return $this;
    }

    /**
     * Get kod
     *
     * @return string
     */
    public function getKod()
    {
        return $this->kod;
    }

    /**
     * Set program
     *
     * @param string $program
     *
     * @return Subject
     */
    public function setProgram($program)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return string
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set bodovi
     *
     * @param integer $bodovi
     *
     * @return Subject
     */
    public function setBodovi($bodovi)
    {
        $this->bodovi = $bodovi;

        return $this;
    }

    /**
     * Get bodovi
     *
     * @return int
     */
    public function getBodovi()
    {
        return $this->bodovi;
    }

    /**
     * Set semRedovni
     *
     * @param integer $semRedovni
     *
     * @return Subject
     */
    public function setSemRedovni($semRedovni)
    {
        $this->semRedovni = $semRedovni;

        return $this;
    }

    /**
     * Get semRedovni
     *
     * @return int
     */
    public function getSemRedovni()
    {
        return $this->semRedovni;
    }

    /**
     * Set semIzvanredni
     *
     * @param integer $semIzvanredni
     *
     * @return Subject
     */
    public function setSemIzvanredni($semIzvanredni)
    {
        $this->semIzvanredni = $semIzvanredni;

        return $this;
    }

    /**
     * Get semIzvanredni
     *
     * @return int
     */
    public function getSemIzvanredni()
    {
        return $this->semIzvanredni;
    }

    /**
     * @return ArrayCollection
     */
    public function getListovi()
    {
        return $this->listovi;
    }

    /**
     * @param $listovi
     * @return Subject
     */
    public function setListovi($listovi)
    {
        $this->listovi = $listovi;
        return $this;
    }

    public function __construct()
    {
        $this->listovi = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getIzborni()
    {
        return $this->izborni;
    }

    /**
     * @param $izborni
     * @return Subject
     */
    public function setIzborni($izborni)
    {
        $this->izborni = $izborni;
        return $this;
    }

    /**
     * Add studenti
     *
     * @param \AppBundle\Entity\Upisi $studenti
     *
     * @return Subject
     */
    public function addListovi(\AppBundle\Entity\Upisi $listovi)
    {
        $this->listovi[] = $listovi;

        return $this;

    }

    /**
     * Remove studenti
     *
     * @param \AppBundle\Entity\Upisi $studenti
     */
    public function removeListovi(\AppBundle\Entity\Upisi $listovi)
    {
        $this->listovi->removeElement($listovi);
    }
}
