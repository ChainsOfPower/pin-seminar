<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 26.6.2016.
 * Time: 2:35
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="status", type="string", columnDefinition="enum('none', 'redovni', 'izvanredni')")
     * @Assert\NotBlank(message="Status ne smije biti prazan")
     *@Assert\Choice(
     *     choices={ "none", "redovni", "izvanredni" },
     *     message="Odaber mora biti redovni ili izvanredni",
     *     groups={"Registration", "Profile"})
     */
    private $status;


    /**
     * @ORM\OneToMany(targetEntity="Upisi", mappedBy="student", cascade={"all"})
     */
    private $listovi;

    public function __construct()
    {
        parent::__construct();
        $this->listovi = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getListovi()
    {
        return $this->listovi;
    }

    /**
     * @param $listovi
     * @return User
     */
    public function setListovi($listovi)
    {
        $this->listovi = $listovi;
        return $this;
    }


    /**
     * Add listovi
     *
     * @param \AppBundle\Entity\Upisi $predmet
     *
     * @return User
     */
    public function addListovi(\AppBundle\Entity\Upisi $listovi)
    {
        $this->listovi[] = $listovi;

        return $this;
    }

    /**
     * Remove predmeti
     *
     * @param \AppBundle\Entity\Upisi $predmeti
     */
    public function removeListovi(\AppBundle\Entity\Upisi $listovi)
    {
        $this->predmeti->removeElement($listovi);
    }

    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);

        return $this;
    }


}
