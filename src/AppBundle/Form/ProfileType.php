<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 26.6.2016.
 * Time: 23:53
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('status', ChoiceType::class, array(
            'choices' => array(
                'form.redovni' => 'redovni',
                'form.izvanredni' => 'izvanredni'
            ),
            'required' => true,
            'label' => 'form.status',
            'translation_domain' => 'FOSUserBundle'
        ))
            ->remove('username');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }
}