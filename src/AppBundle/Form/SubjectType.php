<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.7.2016.
 * Time: 18:36
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ime', TextType::class, array('label' => 'app.form.ime'))
            ->add('kod', TextType::class, array('label' => 'app.form.kod'))
            ->add('program', TextareaType::class, array('label' => 'app.form.program'))
            ->add('bodovi', IntegerType::class, array('label' => 'app.form.bodovi'))
            ->add('semRedovni',IntegerType::class, array('label' => 'app.form.sem_redovni'))
            ->add('semIzvanredni', IntegerType::class, array('label' => 'app.form.sem_izvanredni'))
            ->add('izborni',ChoiceType::class, array(
                'choices' => array(
                    'app.form.da' => 'da',
                    'app.form.ne' => 'ne'
                ),'label' => 'app.form.izborni_predmet'))
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
           'data_class' => 'AppBundle\Entity\Subject'
        ));
    }
}