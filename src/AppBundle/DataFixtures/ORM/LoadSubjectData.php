<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 2.7.2016.
 * Time: 0:10
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Subject;

class LoadSubjectData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $linearna = new Subject();
        $linearna->setIme('Linearna algebra');
        $linearna->setKod('SIT001');
        $linearna->setProgram('Program nije unesen');
        $linearna->setBodovi(5);
        $linearna->setSemRedovni(1);
        $linearna->setSemIzvanredni(1);
        $linearna->setIzborni('ne');

        $manager->persist($linearna);
        $this->addReference('linearna-algebra', $linearna);

        $fizika = new Subject();
        $fizika->setIme('Fizika');
        $fizika->setKod('SIT002');
        $fizika->setProgram('Program nije unesen');
        $fizika->setBodovi(6);
        $fizika->setSemRedovni(1);
        $fizika->setSemIzvanredni(3);
        $fizika->setIzborni('ne');

        $manager->persist($fizika);
        $this->addReference('fizika', $fizika);

        $oe = new Subject();
        $oe->setIme('Osnove elektrotehnike');
        $oe->setKod('SIT003');
        $oe->setProgram('Program nije unesen');
        $oe->setBodovi(6);
        $oe->setSemRedovni(1);
        $oe->setSemIzvanredni(1);
        $oe->setIzborni('ne');

        $manager->persist($oe);

        $d = new Subject();
        $d->setIme('Digitalna i mikroprocesorska tehnika');
        $d->setKod('SIT004');
        $d->setProgram('Program nije unesen');
        $d->setBodovi(7);
        $d->setSemRedovni(1);
        $d->setSemIzvanredni(2);
        $d->setIzborni('ne');

        $manager->persist($d);

        $ur = new Subject();
        $ur->setIme('Uporaba računala');
        $ur->setKod('SIT005');
        $ur->setProgram('Program nije unesen');
        $ur->setBodovi(4);
        $ur->setSemRedovni(1);
        $ur->setSemIzvanredni(1);
        $ur->setIzborni('ne');

        $manager->persist($ur);

        $e1 = new Subject();
        $e1->setIme('Engleski jezik 1');
        $e1->setKod('SIT006');
        $e1->setProgram('Program nije unesen');
        $e1->setBodovi(2);
        $e1->setSemRedovni(1);
        $e1->setSemIzvanredni(1);
        $e1->setIzborni('ne');

        $manager->persist($e1);

        $a1 = new Subject();
        $a1->setIme('Analiza 1');
        $a1->setKod('SIT007');
        $a1->setProgram('Program nije unesen');
        $a1->setBodovi(7);
        $a1->setSemRedovni(2);
        $a1->setSemIzvanredni(2);
        $a1->setIzborni('ne');

        $manager->persist($a1);

        $oe = new Subject();
        $oe->setIme('Osnove elektronike');
        $oe->setKod('SIT008');
        $oe->setProgram('Program nije unesen');
        $oe->setBodovi(6);
        $oe->setSemRedovni(2);
        $oe->setSemIzvanredni(2);
        $oe->setIzborni('ne');

        $manager->persist($oe);

        $aodr = new Subject();
        $aodr->setIme('Arhitektura i organizacija digitalnih računala');
        $aodr->setKod('SIT009');
        $aodr->setProgram('Program nije unesen');
        $aodr->setBodovi(7);
        $aodr->setSemRedovni(2);
        $aodr->setSemIzvanredni(3);
        $aodr->setIzborni('ne');

        $manager->persist($aodr);

        $up = new Subject();
        $up->setIme('Uvod u programiranje');
        $up->setKod('SIT010');
        $up->setProgram('Program nije unesen');
        $up->setBodovi(8);
        $up->setSemRedovni(2);
        $up->setSemIzvanredni(3);
        $up->setIzborni('ne');

        $manager->persist($up);

        $e2 = new Subject();
        $e2->setIme('Engleski jezik 2');
        $e2->setKod('SIT011');
        $e2->setProgram('Program nije unesen');
        $e2->setBodovi(2);
        $e2->setSemRedovni(2);
        $e2->setSemIzvanredni(2);
        $e2->setIzborni('ne');

        $manager->persist($e2);

        $pinm = new Subject();
        $pinm->setIme('Primijenjena i numerička matematika');
        $pinm->setKod('SIT012');
        $pinm->setProgram('Program nije unesen');
        $pinm->setBodovi(6);
        $pinm->setSemRedovni(3);
        $pinm->setSemIzvanredni(4);
        $pinm->setIzborni('ne');

        $manager->persist($pinm);

        $pma = new Subject();
        $pma->setIme('Programske metode i apstrakcije');
        $pma->setKod('SIT013');
        $pma->setProgram('Program nije unesen');
        $pma->setBodovi(8);
        $pma->setSemRedovni(3);
        $pma->setSemIzvanredni(4);
        $pma->setIzborni('ne');

        $manager->persist($pma);

        $bp = new Subject();
        $bp->setIme('Baze podataka');
        $bp->setKod('SIT014');
        $bp->setProgram('Program nije unesen');
        $bp->setBodovi(6);
        $bp->setSemRedovni(3);
        $bp->setSemIzvanredni(5);
        $bp->setIzborni('ne');

        $manager->persist($bp);

        $kolegij = new Subject();
        $kolegij->setIme('Informacijski sustavi');
        $kolegij->setKod('SIT015');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(6);
        $kolegij->setSemRedovni(3);
        $kolegij->setSemIzvanredni(4);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Tehnički engleski jezik');
        $kolegij->setKod('SIT016');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(4);
        $kolegij->setSemRedovni(3);
        $kolegij->setSemIzvanredni(5);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Računalne mreže');
        $kolegij->setKod('SIT017');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(5);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Operacijski sustavi');
        $kolegij->setKod('SIT018');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(5);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Strukture podataka i algoritmi');
        $kolegij->setKod('SIT019');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(6);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Objektno programiranje');
        $kolegij->setKod('SIT020');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(6);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Baze podataka 2');
        $kolegij->setKod('SIT021');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(6);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Mrežne usluge i programiranje');
        $kolegij->setKod('SIT022');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(6);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Arhitektura osobnih računala');
        $kolegij->setKod('SIT023');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(6);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Projektiranje i upravljanje računalnim mrežama');
        $kolegij->setKod('SIT024');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(5);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Projektiranje informacijskih sustava');
        $kolegij->setKod('SIT025');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(6);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Informatizacija poslovanja');
        $kolegij->setKod('SIT026');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(4);
        $kolegij->setSemIzvanredni(6);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Ekonomika i organizacija poduzeća');
        $kolegij->setKod('SIT027');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(2);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Analiza 2');
        $kolegij->setKod('SIT028');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(6);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Industrijska praksa');
        $kolegij->setKod('SIT029');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(2);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Arhitektura poslužiteljskih računala');
        $kolegij->setKod('SIT030');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Sigurnost računala i podataka');
        $kolegij->setKod('SIT031');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Programski alati na UNIX računalima');
        $kolegij->setKod('SIT032');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Napredno Windows programiranje');
        $kolegij->setKod('SIT033');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Objektno orijentirano modeliranje');
        $kolegij->setKod('SIT034');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Programiranje u Javi');
        $kolegij->setKod('SIT035');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Programiranje na Internetu');
        $kolegij->setKod('SIT036');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Elektroničko poslovanje');
        $kolegij->setKod('SIT037');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(5);
        $kolegij->setSemIzvanredni(7);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Diskretna matematika');
        $kolegij->setKod('SIT038');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(6);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Upravljanje poslužiteljskim računalima');
        $kolegij->setKod('SIT039');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Programiranje u C#');
        $kolegij->setKod('SIT040');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Društveni informacijski sustavi');
        $kolegij->setKod('SIT041');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Oblikovanje Web stranica');
        $kolegij->setKod('SIT042');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Vođenje projekata i dokumentacija');
        $kolegij->setKod('SIT043');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Informatizacija proizvodnje');
        $kolegij->setKod('SIT044');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Analiza i obrada podataka');
        $kolegij->setKod('SIT045');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(5);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Njemački jezik');
        $kolegij->setKod('SSZP40');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(4);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Talijanski jezik');
        $kolegij->setKod('SSZP50');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(4);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('da');

        $manager->persist($kolegij);

        $kolegij = new Subject();
        $kolegij->setIme('Završni rad');
        $kolegij->setKod('SIT046');
        $kolegij->setProgram('Program nije unesen');
        $kolegij->setBodovi(8);
        $kolegij->setSemRedovni(6);
        $kolegij->setSemIzvanredni(8);
        $kolegij->setIzborni('ne');

        $manager->persist($kolegij);


        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}