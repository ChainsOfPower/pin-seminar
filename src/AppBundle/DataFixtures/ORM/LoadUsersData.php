<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 2.7.2016.
 * Time: 0:33
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;

class LoadUsersData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $redovni = new User();
        $redovni->setEmail('redovni@oss.unist.hr');
        $redovni->setPlainPassword('123');
        $redovni->setRoles(array('ROLE_STUDENT'));
        $redovni->setStatus('redovni');
        $redovni->setEnabled(1);

        $this->container->get('fos_user.user_manager')->updateUser($redovni);
        $this->addReference('redovni-student', $redovni);


        $izvanredni = new User();
        $izvanredni->setEmail('izvanredni@oss.unist.hr');
        $izvanredni->setPlainPassword('123');
        $izvanredni->setRoles(array('ROLE_STUDENT'));
        $izvanredni->setStatus('izvanredni');
        $izvanredni->setEnabled(1);


        $this->container->get('fos_user.user_manager')->updateUser($izvanredni);
        $this->addReference('izvanredni-student', $izvanredni);


        $mentor = new User();
        $mentor->setEmail('mentor@oss.unist.hr');
        $mentor->setPlainPassword('123');
        $mentor->setRoles(array('ROLE_MENTOR'));
        $mentor->setStatus('none');
        $mentor->setEnabled(1);

        $this->container->get('fos_user.user_manager')->updateUser($mentor);
        $this->addReference('mentor', $mentor);


    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}