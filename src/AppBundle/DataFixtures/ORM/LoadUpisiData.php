<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 3.7.2016.
 * Time: 4:04
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Upisi;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Propel\UserGroup;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUpisiData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

       /* $userManager = $manager->getRepository('AppBundle:User');
        $subjectManager = $manager->getRepository('AppBundle:Subject');


        $redovni = $this->getReference('redovni-student');
        $al = $this->getReference('linearna-algebra');


        $redovni->add*/

        $redovni = $this->getReference('redovni-student');
        $al = $this->getReference('linearna-algebra');

        $list = new Upisi();
        $list->setPredmet($al);
        $list->setStudent($redovni);
        $list->setStatus('upisano');


        $redovni->addListovi($list);
        $al->addListovi($list);

        $manager->persist($list);


        $f = $this->getReference('fizika');
        $list = new Upisi();
        $list->setPredmet($f);
        $list->setStudent($redovni);
        $list->setStatus('upisano');

        $redovni->addListovi($list);
        $f->addListovi($list);

        $manager->persist($list);



        $manager->flush();


    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }

}