<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 29.6.2016.
 * Time: 21:39
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Subject;
use AppBundle\Entity\Upisi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Util\Debug;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @Route("/student")
 * @Security("has_role('ROLE_STUDENT')")
 */
class StudentController extends Controller
{
    /**
     * @Route("/", name="student_homepage")
     */
    public function indexAction()
    {
        $csrf = $this->get('security.csrf.token_manager');
        $token = $csrf->getToken("ajax");

        $em = $this->getDoctrine()->getManager();
        $predmetiRepo = $em->getRepository('AppBundle:Subject');
        $upisiRepo = $em->getRepository('AppBundle:Upisi');

        $neupisaniPredmeti = $predmetiRepo->findAllFreeSubjectsForStudent($this->getUser()->getId());
        /*dump($neupisaniPredmeti);
        die();*/

        $upisaniPredmeti = $upisiRepo->findAllSubjectsWithStatusForStudent($this->getUser()->getId());


        if($this->getUser()->getStatus() == 'redovni')
        {
            $prviSemestar = [];
            $drugiSemestar = [];
            $treciSemestar = [];
            $cetvrtiSemestar = [];
            $petiSemestar = [];
            $sestiSemestar = [];

            foreach($upisaniPredmeti as $predmet)
            {
                if($predmet['semRedovni'] == 1)
                {
                    $prviSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 2)
                {
                    $drugiSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 3)
                {
                    $treciSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 4)
                {
                    $cetvrtiSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 5)
                {
                    $petiSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 6)
                {
                    $sestiSemestar[] = $predmet;
                }
            }

            return $this->render('Student/redovni_home.html.twig', array('user' => $this->getUser(), 'neupisani' => $neupisaniPredmeti, 'prviSemestar' => $prviSemestar, 'drugiSemestar' => $drugiSemestar,
                'treciSemestar' => $treciSemestar, 'cetvrtiSemestar' => $cetvrtiSemestar, 'petiSemestar' => $petiSemestar, 'sestiSemestar' => $sestiSemestar, 'student' => $this->getUser(), 'token' => $token));
        }
        else
        {
            $prviSemestar = [];
            $drugiSemestar = [];
            $treciSemestar = [];
            $cetvrtiSemestar = [];
            $petiSemestar = [];
            $sestiSemestar = [];
            $sedmiSemestar = [];
            $osmiSemestar = [];

            foreach($upisaniPredmeti as $predmet)
            {
                if($predmet['semIzvanredni'] == 1)
                {
                    $prviSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 2)
                {
                    $drugiSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 3)
                {
                    $treciSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 4)
                {
                    $cetvrtiSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 5)
                {
                    $petiSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 6)
                {
                    $sestiSemestar[] = $predmet;
                }else if($predmet['semIzvanredni'] == 7)
                {
                    $sedmiSemestar[] = $predmet;
                }else if($predmet['semIzvanredni'] == 8)
                {
                    $osmiSemestar[] = $predmet;
                }
            }

            return $this->render('Student/izvanredni_home.html.twig', array('user' => $this->getUser(), 'neupisani' => $neupisaniPredmeti, 'prviSemestar' => $prviSemestar, 'drugiSemestar' => $drugiSemestar,
                'treciSemestar' => $treciSemestar, 'cetvrtiSemestar' => $cetvrtiSemestar, 'petiSemestar' => $petiSemestar, 'sestiSemestar' => $sestiSemestar,
                'sedmiSemestar' => $sedmiSemestar, 'osmiSemestar' => $osmiSemestar, 'student' => $this->getUser(), 'token' => $token));
        }

    }

    /**
     * @Route("/refreshUpisaniINeupisani", name="student_refreshUpisaniINeupisaniPredmeti")
     * @Method({"GET"})
     */
    public function loadUpisaniINeupisaniAction()
    {
        $em = $this->getDoctrine()->getManager();
        $predmetiRepo = $em->getRepository('AppBundle:Subject');
        $upisiRepo = $em->getRepository('AppBundle:Upisi');

        $sviPredmeti = [];

        $neupisaniPredmeti = $predmetiRepo->findAllFreeSubjectsForStudent($this->getUser()->getId());
        $upisaniPredmeti = $upisiRepo->findAllSubjectsWithStatusForStudent($this->getUser()->getId());

        $sviPredmeti[] = $neupisaniPredmeti;
        $sviPredmeti[] = $upisaniPredmeti;

        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object;
        });


        $serializer = new Serializer(array($normalizer), array($encoder));


        $sviPredmeti = $serializer->serialize($sviPredmeti, 'json');


        $response = new Response($sviPredmeti);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    /**
     * @Route("/upisiPredmet/{id}", name="student_upisi_predmet")
     * @Method({"POST"})
     */
    public function upisiPredmet(Request $request, Subject $subject)
    {
        $sessionToken = $request->request->get('token');
        $csrf = $this->get('security.csrf.token_manager');
        $checkToken = $csrf->getToken("ajax");

        if($checkToken->getValue() === $sessionToken) {
            if ($subject == null) {
                $content = json_encode(array('message' => 'Predmet ne postoji'));
                return new Response($content, 404);
            }

            $em = $this->getDoctrine()->getManager();

            $student = $this->getUser();


            $list = new Upisi();
            $list->setStatus('upisano');
            $list->setPredmet($subject);
            $list->setStudent($student);

            $subject->addListovi($list);
            $student->addListovi($list);

            try {
                $em->persist($list);
                $em->flush();
                return new Response();
            } catch (EntityNotFoundException $e) {
                $content = json_encode(array('message' => 'Upis neuspješan'));
                return new Response($content, 419);
            }
        }

        $content = json_encode(array('message' => 'Token error'));
        return new Response($content, 419);
    }

    /**
     * @param Subject $subject
     * @Route("/poloziPredmet/{id}", name="student_polozi_predmet")
     * @Method({"POST"})
     */
    public function poloziPredmet(Request $request, Subject $subject)
    {
        $sessionToken = $request->request->get('token');
        $csrf = $this->get('security.csrf.token_manager');
        $checkToken = $csrf->getToken("ajax");

        if($checkToken->getValue() === $sessionToken) {
            $em = $this->getDoctrine()->getManager();
            $upisiRepo = $em->getRepository('AppBundle:Upisi');

            $list = $upisiRepo->findOneBy(array('student' => $this->getUser(), 'predmet' => $subject));

            if ($list == null) {
                $content = json_encode(array('message' => 'Upisni list ne postoji'));
                return new Response($content, 404);
            }

            try {
                $list->setStatus('polozeno');
                $em->flush();
                return new Response();
            } catch (EntityNotFoundException $e) {
                $content = json_encode(array('message' => 'Polaganje neuspješno'));
                return new Response($content, 419);
            }
        }
        $content = json_encode(array('message' => 'Token error'));
        return new Response($content, 419);
    }

    /**
     * @Route("/ispisiPredmet/{id}", name="student_ispisi_predmet")
     * @Method({"POST"})
     */
    public function ispisiPredmet(Request $request, Subject $subject)
    {
        $sessionToken = $request->request->get('token');
        $csrf = $this->get('security.csrf.token_manager');
        $checkToken = $csrf->getToken("ajax");

        if($checkToken->getValue() === $sessionToken) {
            $em = $this->getDoctrine()->getManager();
            $upisiRepo = $em->getRepository('AppBundle:Upisi');

            $list = $upisiRepo->findOneBy(array('student' => $this->getUser(), 'predmet' => $subject));

            if ($list == null) {
                $content = json_encode(array('message' => 'Upisni list ne postoji'));
                return new Response($content, 404);
            }

            try {
                $em->remove($list);
                $em->flush();
                return new Response();
            } catch (EntityNotFoundException $e) {
                $content = json_encode(array('message' => 'Polaganje neuspješno'));
                return new Response($content, 419);
            }
        }
        $content = json_encode(array('message' => 'Token error'));
        return new Response($content, 419);
    }
}