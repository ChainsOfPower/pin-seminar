<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 29.6.2016.
 * Time: 22:06
 */

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Subject;
use AppBundle\Entity\Upisi;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Tests\Fixtures\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
 * @Route("/mentor")
 * @Security("has_role('ROLE_MENTOR')")
 */
class MentorController extends Controller
{
    /**
     * @Route("/studenti", name="mentor_studenti")
     */
    public function studentsAction()
    {

        $em = $this->getDoctrine();

        $userRepo = $em->getRepository('AppBundle:User');

        $studenti = $userRepo->findAllStudents();

        return $this->render(':Mentor:mentor_studenti.html.twig', array('studenti' => $studenti));
    }

    /**
     * @Route("/studenti/refreshStudenti", name="mentor_refreshStudenti")
     * @Method({"GET"})
     */
    public function loadStudentsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository('AppBundle:User');

        $studenti = $userRepo->findAllStudents();

        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object;
        });

        $serializer = new Serializer(array($normalizer), array($encoder));

        $studenti = $serializer->serialize($studenti, 'json');

        $response = new Response($studenti);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/student/{id}", name="mentor_studentList")
     */
    public function studentUpisniListAction(\AppBundle\Entity\User $user)
    {
        $csrf = $this->get('security.csrf.token_manager');
        $token = $csrf->getToken("ajax");


        if($user == null || $user->getStatus() == 'none')
        {
            $response = new Response();
            $response->setStatusCode(404, 'Student nije pronađen');

            return $response;
        }

        $em = $this->getDoctrine();
        $predmetiRepo = $em->getRepository('AppBundle:Subject');
        $upisiRepo = $em->getRepository('AppBundle:Upisi');

        $neupisaniPredmeti = $predmetiRepo->findAllFreeSubjectsForStudent($user->getId());
        $upisaniPredmeti = $upisiRepo->findAllSubjectsWithStatusForStudent($user->getId());

        if($user->getStatus() == 'redovni')
        {
            $prviSemestar = [];
            $drugiSemestar = [];
            $treciSemestar = [];
            $cetvrtiSemestar = [];
            $petiSemestar = [];
            $sestiSemestar = [];

            foreach($upisaniPredmeti as $predmet)
            {
                if($predmet['semRedovni'] == 1)
                {
                    $prviSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 2)
                {
                    $drugiSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 3)
                {
                    $treciSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 4)
                {
                    $cetvrtiSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 5)
                {
                    $petiSemestar[] = $predmet;

                }else if($predmet['semRedovni'] == 6)
                {
                    $sestiSemestar[] = $predmet;
                }
            }

            return $this->render('Student/redovni_home.html.twig', array('user' => $this->getUser(), 'neupisani' => $neupisaniPredmeti, 'prviSemestar' => $prviSemestar, 'drugiSemestar' => $drugiSemestar,
                'treciSemestar' => $treciSemestar, 'cetvrtiSemestar' => $cetvrtiSemestar, 'petiSemestar' => $petiSemestar, 'sestiSemestar' => $sestiSemestar, 'student' => $user, 'token' => $token));
        }
        else
        {
            $prviSemestar = [];
            $drugiSemestar = [];
            $treciSemestar = [];
            $cetvrtiSemestar = [];
            $petiSemestar = [];
            $sestiSemestar = [];
            $sedmiSemestar = [];
            $osmiSemestar = [];

            foreach($upisaniPredmeti as $predmet)
            {
                if($predmet['semIzvanredni'] == 1)
                {
                    $prviSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 2)
                {
                    $drugiSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 3)
                {
                    $treciSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 4)
                {
                    $cetvrtiSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 5)
                {
                    $petiSemestar[] = $predmet;

                }else if($predmet['semIzvanredni'] == 6)
                {
                    $sestiSemestar[] = $predmet;
                }else if($predmet['semIzvanredni'] == 7)
                {
                    $sedmiSemestar[] = $predmet;
                }else if($predmet['semIzvanredni'] == 8)
                {
                    $osmiSemestar[] = $predmet;
                }
            }

            return $this->render('Student/izvanredni_home.html.twig', array('user' => $this->getUser(), 'neupisani' => $neupisaniPredmeti, 'prviSemestar' => $prviSemestar, 'drugiSemestar' => $drugiSemestar,
                'treciSemestar' => $treciSemestar, 'cetvrtiSemestar' => $cetvrtiSemestar, 'petiSemestar' => $petiSemestar, 'sestiSemestar' => $sestiSemestar,
                'sedmiSemestar' => $sedmiSemestar, 'osmiSemestar' => $osmiSemestar, 'student' => $user, 'token' => $token));
        }


    }

    /**
     * @Route("/student/{id}/refreshUpisaniINeupisani", name="mentor_refreshUpisaniINeupisani")
     * @Method({"GET"})
     */
    public function loadUpisaniINeupisaniAction(\AppBundle\Entity\User $user)
    {
        $em = $this->getDoctrine();
        $predmetiRepo = $em->getRepository('AppBundle:Subject');
        $upisiRepo = $em->getRepository('AppBundle:Upisi');

        $sviPredmeti = [];

        $neupisaniPredmeti = $predmetiRepo->findAllFreeSubjectsForStudent($user->getId());
        $upisaniPredmeti = $upisiRepo->findAllSubjectsWithStatusForStudent($user->getId());

        $sviPredmeti[] = $neupisaniPredmeti;
        $sviPredmeti[] = $upisaniPredmeti;

        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object;
        });


        $serializer = new Serializer(array($normalizer), array($encoder));


        $sviPredmeti = $serializer->serialize($sviPredmeti, 'json');


        $response = new Response($sviPredmeti);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/student/{user}/upisiPredmet/{subject}", name="mentor_upisi_predmet")
     * @Method({"POST"})
     */
    public function upisiPredmet(Request $request ,\AppBundle\Entity\User $user, Subject $subject)
    {

        $sessionToken = $request->request->get('token');
        $csrf = $this->get('security.csrf.token_manager');
        $checkToken = $csrf->getToken("ajax");

        if($checkToken->getValue() === $sessionToken)
        {
            if($user == null || $subject == null || $user->getStatus() == 'none')
            {
                $content = json_encode(array('message' => 'Student ili predmet ne postoje'));
                return new Response($content, 404);
            }

            $em = $this->getDoctrine()->getManager();

            $list = new Upisi();
            $list->setStatus('upisano');
            $list->setPredmet($subject);
            $list->setStudent($user);

            $subject->addListovi($list);
            $user->addListovi($list);

            try{
                $em->persist($list);
                $em->flush();
                return new Response();
            }
            catch(EntityNotFoundException $e)
            {
                $content = json_encode(array('message' => 'Upis neuspješan'));
                return new Response($content, 419);
            }
        }

        $content = json_encode(array('message' => 'Token error'));
        return new Response($content, 419);
    }

    /**
     * @Route("/student/{user}/poloziPredmet/{subject}", name="mentor_polozi_predmet")
     * @Method({"POST"})
     */
    public function poloziPredmet(Request $request,\AppBundle\Entity\User $user, Subject $subject)
    {
        $sessionToken = $request->request->get('token');
        $csrf = $this->get('security.csrf.token_manager');
        $checkToken = $csrf->getToken("ajax");

        if($checkToken->getValue() === $sessionToken) {
            if ($user == null || $subject == null || $user->getStatus() == 'none') {
                $content = json_encode(array('message' => 'Student ili predmet ne postoje'));
                return new Response($content, 404);
            }
            $em = $this->getDoctrine()->getManager();
            $upisiRepo = $em->getRepository('AppBundle:Upisi');

            $list = $upisiRepo->findOneBy(array('student' => $user, 'predmet' => $subject));

            if ($list == null) {
                $content = json_encode(array('message' => 'Upisni list ne postoji'));
                return new Response($content, 404);
            }

            try {
                $list->setStatus('polozeno');
                $em->flush();
                return new Response();
            } catch (EntityNotFoundException $e) {
                $content = json_encode(array('message' => 'Polaganje neuspješno'));
                return new Response($content, 419);
            }
        }

        $content = json_encode(array('message' => 'Token error'));
        return new Response($content, 419);
    }

    /**
     * @Route("/student/{user}/ispisiPredmet/{subject}", name="mentor_ispisi_predmet")
     * @Method({"POST"})
     */
    public function ispisiPredmet(Request $request, \AppBundle\Entity\User $user, Subject $subject)
    {
        $sessionToken = $request->request->get('token');
        $csrf = $this->get('security.csrf.token_manager');
        $checkToken = $csrf->getToken("ajax");

        if($checkToken->getValue() === $sessionToken) {
            if ($user == null || $subject == null || $user->getStatus() == 'none') {
                $content = json_encode(array('message' => 'Student ili predmet ne postoje'));
                return new Response($content, 404);
            }

            $em = $this->getDoctrine()->getManager();
            $upisiRepo = $em->getRepository('AppBundle:Upisi');

            $list = $upisiRepo->findOneBy(array('student' => $user, 'predmet' => $subject));

            if ($list == null) {
                $content = json_encode(array('message' => 'Upisni list ne postoji'));
                return new Response($content, 404);
            }

            try {
                $em->remove($list);
                $em->flush();
                return new Response();
            } catch (EntityNotFoundException $e) {
                $content = json_encode(array('message' => 'Polaganje neuspješno'));
                return new Response($content, 419);
            }
        }
        $content = json_encode(array('message' => 'Token error'));
        return new Response($content, 419);
    }
}