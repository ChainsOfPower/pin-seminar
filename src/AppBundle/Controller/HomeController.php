<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 26.6.2016.
 * Time: 16:41
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class HomeController extends Controller
{
    /**
     * @Route("", name="homepage")
     */
    public function indexAction()
    {
        if($this->get('security.authorization_checker')->isGranted('ROLE_STUDENT'))
        {
            return new RedirectResponse($this->get('router')->generate('student_homepage'));
        }
        else if ($this->get('security.authorization_checker')->isGranted('ROLE_MENTOR'))
        {
            return new RedirectResponse($this->get('router')->generate('mentor_studenti'));
        }

        return $this->render('Home/index.html.twig', array());
    }

}