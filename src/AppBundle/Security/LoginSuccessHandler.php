<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 30.6.2016.
 * Time: 1:02
 */

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{

    protected $router;
    protected $authorizationChecker;

    public function __construct(Router $router, AuthorizationChecker $authorizationChecker)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request $request
     * @param TokenInterface $token
     *
     * @return Response never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $response = null;

        if($this->authorizationChecker->isGranted('ROLE_MENTOR'))
        {
            $response = new RedirectResponse($this->router->generate('mentor_studenti'));

        } else if ($this->authorizationChecker->isGranted('ROLE_STUDENT')) {
            $response = new RedirectResponse($this->router->generate('student_homepage'));
        }

        return $response;
    }
}