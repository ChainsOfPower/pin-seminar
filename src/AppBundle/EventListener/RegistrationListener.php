<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 28.6.2016.
 * Time: 1:47
 */

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class RegistrationListener implements EventSubscriberInterface
{
    private $router;
    private $authorizationChecker;

    public function __construct(AuthorizationChecker $authorizationChecker, UrlGeneratorInterface $router)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
        );
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        $roleArr = array('ROLE_STUDENT');

        $user = $event->getForm()->getData();
        $user->setRoles($roleArr);
        $user->setUsername($user->getEmail());

    }

    public function onRegistrationInitialize(GetResponseUserEvent $event)
    {

        if ($this->authorizationChecker->isGranted('ROLE_STUDENT')) {
            $url = $this->router->generate('student_homepage');
            $event->setResponse(new RedirectResponse($url));
        } else if ($this->authorizationChecker->isGranted('ROLE_MENTOR')) {
            $url = $this->router->generate('mentor_homepage');
            $event->setResponse(new RedirectResponse($url));
        }

    }
}