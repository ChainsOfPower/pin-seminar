<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 3.7.2016.
 * Time: 19:09
 */

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;


class ResettingListener implements EventSubscriberInterface
{
    private $router;
    private $authorizationChecker;

    public function __construct(UrlGeneratorInterface $router, AuthorizationChecker $authorizationChecker)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    public static function getSubscribedEvents()
    {
        return array(
          FOSUserEvents::RESETTING_RESET_INITIALIZE => 'onResettingInitialize',
            FOSUserEvents::RESETTING_RESET_SUCCESS => 'onResettingSuccess',
        );
    }

    public function onResettingInitialize(GetResponseUserEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_STUDENT')) {
            $url = $this->router->generate('student_homepage');
            $event->setResponse(new RedirectResponse($url));
        } else if ($this->authorizationChecker->isGranted('ROLE_MENTOR')) {
            $url = $this->router->generate('mentor_homepage');
            $event->setResponse(new RedirectResponse($url));
        }
    }

    public function onResettingSuccess(FormEvent $event)
    {
        $url = $this->router->generate('homepage');
        $event->setResponse(new RedirectResponse($url));
    }

}