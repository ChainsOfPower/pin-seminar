<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 29.6.2016.
 * Time: 20:11
 */

namespace AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class PasswordChangingListener implements EventSubscriberInterface
{
    private $router;
    protected $authorizationChecker;


    public function __construct(UrlGeneratorInterface $router, AuthorizationChecker $authorizationChecker)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::CHANGE_PASSWORD_SUCCESS => 'onPasswordChangingSuccess',
        );
    }

    public function onPasswordChangingSuccess(FormEvent $event)
    {

        if ($this->authorizationChecker->isGranted('ROLE_STUDENT')) {
            $url = $this->router->generate('student_homepage');
            $event->setResponse(new RedirectResponse($url));
        } else if ($this->authorizationChecker->isGranted('ROLE_MENTOR')) {
            $url = $this->router->generate('mentor_studenti');
            $event->setResponse(new RedirectResponse($url));
        }

    }
}