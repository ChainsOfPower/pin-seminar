/**
 * Created by Ivan on 12.7.2016..
 */
var rutaUpdatePredmeti = Routing.generate('subject_refreshSubjects');

window.onload = function()
{
    setTimeout(refreshEvent,
    10000);
};


function refreshEvent()
{
    refreshPredmetiLoop();
}

function updateView(data)
{
    var predmeti = data;

    $("#lista_predmeta").empty();


    $.each(predmeti, function(k, v){
        var rutaDetails = Routing.generate('subject_show', {id: v.id});
        var rutaEdit = Routing.generate('subject_edit', {id: v.id});

       $("#lista_predmeta").append('<tr><td>' + v.ime + ' (' + v.kod + ')<a href="' + rutaDetails + '"> Detalji </a><a href="' + rutaEdit + '">Uredi</a></td></tr>');
    });
}

function refreshPredmetiLoop(){
    $.ajax({
        url: rutaUpdatePredmeti,
        success:    function(data)
        {
            updateView(data);
        },
        complete:   function() {
            setTimeout(refreshPredmetiLoop, 10000);
        }
    });
}