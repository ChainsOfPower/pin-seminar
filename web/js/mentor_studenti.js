
var rutaUpdateStudenti = Routing.generate('mentor_refreshStudenti')
window.onload = function() {
    setTimeout(refreshEvent,
        10000);
};

function refreshEvent()
{
    refreshStudentiLoop();
}/**
 * Created by Ivan on 10.7.2016..
 */

function updateView(data)
{
    var studenti = data;

    //osvježi upisane predmete
    $("#lista_studenata").empty();



    $.each(studenti, function(k, v){
        var link = Routing.generate('mentor_studentList', {id: v.id});
        $("#lista_studenata").append('<tr><td><a href="' + link + '">' + v.username + '</a></td></tr>');
    });
}

function refreshStudentiLoop(){
    $.ajax({
        url: rutaUpdateStudenti,
        success:    function(data)
        {
            updateView(data);
        },
        complete:   function() {
            setTimeout(refreshStudentiLoop, 10000);
        }
    });
}