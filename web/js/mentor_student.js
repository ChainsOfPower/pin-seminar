/**
 * Created by Ivan on 10.7.2016..
 */

var csrf_token = $("#token").html();
console.log(csrf_token);



window.onload = function() {
    setTimeout(refreshEvent,
        10000);
};

var studentId = $("#korisnik").text();

var rutaUpdateUpisaniINeupisani = Routing.generate('mentor_refreshUpisaniINeupisani', {id: studentId });

$("#neupisani_predmeti_studenta").on("click", "a", function (event) {
    var rutaUpisiPredmet = Routing.generate('mentor_upisi_predmet', {user: studentId, subject: this.id});
    $.ajax({
        url: rutaUpisiPredmet,
        method: "POST",
        data:   {token: csrf_token},
        complete:   function() {
            $.ajax({
                url: rutaUpdateUpisaniINeupisani,
                success:    function(data)
                {
                    updateView(data);
                }
            });
        }
    });
});

$("#upisani_predmeti_studenta").on("click", ".fa-check-square-o", function(event){
    var rutaPoloziPredmet = Routing.generate('mentor_polozi_predmet', {user: studentId, subject: this.id});

    $.ajax({
        url:    rutaPoloziPredmet,
        method: "POST",
        data:   {token: csrf_token},
        complete:   function() {
            $.ajax({
                url: rutaUpdateUpisaniINeupisani,
                success:    function(data)
                {
                    updateView(data);
                }
            });
        }
    });
});

$("#upisani_predmeti_studenta").on("click", ".fa-times", function(event){
    var rutaIspisiPredmet = Routing.generate('mentor_ispisi_predmet', {user: studentId, subject: this.id});

    $.ajax({
        url:    rutaIspisiPredmet,
        method: "POST",
        data:   {token: csrf_token},
        complete:   function() {
            $.ajax({
                url: rutaUpdateUpisaniINeupisani,
                success:    function(data)
                {
                    updateView(data);
                }
            });
        }
    });
});


function refreshEvent()
{
    refreshUpisaniINeupisaniPredmetiLoop();
}

function updateView(data)
{
    var neupisani = data[0];
    var upisani = data[1];
    //osvježi neupisane predmete
    $("#neupisani_predmeti_studenta").empty();
    $.each(neupisani, function(k, v){
        $("#neupisani_predmeti_studenta").append('<li class="list-group-item"><a href="javascript:void(0)" id="' + v.id+ '"><i class="fa fa-plus-square fa-lg" aria-hidden="true"></i></a> ' + v.ime + '</li>');
    });

    //osvježi upisane predmete
    $("#prviSemestar,#drugiSemestar, #treciSemestar, #cetvrtiSemestar, #petiSemestar, #sestiSemestar").each(function(){
        $(this).empty();
    });


    $.each(upisani, function(k, v){
        if(v.status == 'upisano')
        {
            var icon = '<a href="javascript:void(0)"><i id="' + v.id + '" class="fa fa-check-square-o fa-lg" aria-hidden="true"></i></a> ';
        }else
        {
            var icon = '<i class="fa fa-check fa-lg" aria-hidden="true"></i> ';
        }

        var del = '<a href="javascript:void(0)"><i id="' + v.id + '" class="fa fa-times fa-lg"></i></a> ';
        if(v.semRedovni == 1)
        {
            $("#prviSemestar").append(' <tr> <td>' + icon + del + v.ime + '</td> </tr>');
        }else if(v.semRedovni == 2)
        {
            $("#drugiSemestar").append(' <tr> <td>' + icon + del + v.ime + '</td> </tr>');
        }else if(v.semRedovni == 3)
        {
            $("#treciSemestar").append(' <tr> <td>' + icon + del + v.ime + '</td> </tr>');
        }else if(v.semRedovni == 4)
        {
            $("#cetvrtiSemestar").append(' <tr> <td>' + icon + del + v.ime + '</td> </tr>');
        }else if(v.semRedovni == 5)
        {
            $("#petiSemestar").append(' <tr> <td>' + icon + del + v.ime + '</td> </tr>');
        }else if(v.semRedovni == 6)
        {
            $("#sestiSemestar").append(' <tr> <td>' + icon + del + v.ime + '</td> </tr>');
        }

    });
}

function refreshUpisaniINeupisaniPredmetiLoop(){
    $.ajax({
        url: rutaUpdateUpisaniINeupisani,
        success:    function(data)
        {
            updateView(data);
        },
        complete:   function() {
            setTimeout(refreshUpisaniINeupisaniPredmetiLoop, 10000);
        }
    });
}